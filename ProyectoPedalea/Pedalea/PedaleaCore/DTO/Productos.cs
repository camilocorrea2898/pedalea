﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PedaleaCore.DTO
{
    public class Productos
    {
        public long ProductoId { get; set; }
        public string Descripcion { get; set; }
        public float Valor { get; set; }
        public long DepartamentoVentaId { get; set; }
        public DateTime FechaCreacion { get; set; }
        public DateTime FechaModificacion { get; set; }
    }

    public class ProductosDisponiblesDepartamento
    {
        public string PuntoVenta { get; set; }
        public string DireccionVenta { get; set; }
        public string CiudadPuntoVenta { get; set; }
        public List<ProductosDisponibles> ListaProductos { get; set; }
    }
    public class ProductosDisponibles
    {
        public long ProductoId { get; set; }
        public int StockTotal { get; set; }
        public string DescripcionProducto { get; set; }
        public float ValorProducto { get; set; }
    }
}

