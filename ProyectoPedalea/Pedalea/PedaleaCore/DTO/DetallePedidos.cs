﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PedaleaCore.DTO
{
    public class DetallePedidos
    {
        public long DetallePedidoId { get; set; }
        public long PedidoId { get; set; }
        public long ProductoId { get; set; }
        public float ValorProducto { get; set; }
        public int Cantidad { get; set; }
        public float SubTotal { get; set; }
        public DateTime FechaCreacion { get; set; }
        public DateTime FechaModificacion { get; set; }
    }
}