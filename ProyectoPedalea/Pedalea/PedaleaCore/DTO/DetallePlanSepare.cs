﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PedaleaCore.DTO
{
    public class DetallePlanSepare
    {
        public long DetallePlanSepareId { get; set; }
        public long PlanSepareId { get; set; }
        public DateTime FechaPago { get; set; }
        public float ValorPagado { get; set; }
        public string MedioPago { get; set; }
        public float ValorRestante { get; set; }
        public DateTime FechaCreacion { get; set; }
        public DateTime FechaModificacion { get; set; }
    }
}