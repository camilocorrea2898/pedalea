﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PedaleaCore.DTO
{
    public class Pedidos
    {
        public long PedidoId { get; set; }
        public DateTime FechaPedido { get; set; }
        public int EstadoPedidoId { get; set; }
        public string DireccionEnvio { get; set; }
        public DateTime FechaEnvio { get; set; }
        public string IdentificacionCliente { get; set; }
        public float SubTotal { get; set; }
        public float ValorTotal { get; set; }
        public long PromocionId { get; set; }
        public DateTime FechaCreacion { get; set; }
        public DateTime FechaModificacion { get; set; }
    }
}