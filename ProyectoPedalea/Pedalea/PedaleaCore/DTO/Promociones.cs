﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PedaleaCore.DTO
{
    public class Promociones
    {
        public long PromocionId { get; set; }
        public string Descripcion { get; set; }
        public float MontoMinimo { get; set; }
        public float MontoMaximo { get; set; }
        public int CantidadProductosMinimo { get; set; }
        public float PorcentajePromocion { get; set; }
        public DateTime FechaCreacion { get; set; }
        public DateTime FechaModificacion { get; set; }
    }
    public class ValidarPromocionesDepartamento
    {
        public long DepartamentoVentaId { get; set; }
        public string IdentificacionCliente { get; set; }
        public float ValorCompra { get; set; }
    }

    public class ValidarPromocionesDepartamentoResponse
    {
        public bool AplicaSepare { get; set; }
        public float ValorFinal { get; set; }
    }
}