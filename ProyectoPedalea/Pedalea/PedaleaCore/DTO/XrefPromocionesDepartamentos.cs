﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PedaleaCore.DTO
{
    public class XrefPromocionesDepartamentos
    {
        public long PromocionId { get; set; }
        public long DepartamentoVentaId { get; set; }
    }
    public class XrefPromocionesDepartamentosUpdate
    {
        public XrefPromocionesDepartamentos XrefPromocionesDepartamentosLast { get; set; }
        public XrefPromocionesDepartamentos XrefPromocionesDepartamentosNew { get; set; }
    }
}