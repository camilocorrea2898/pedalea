﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PedaleaCore.DTO
{
    public class PlanSepare
    {
        public long PlanSepareId { get; set; }
        public long PedidoId { get; set; }
        public float ValorTotal { get; set; }
        public float ValorPagado { get; set; }
        public int CantidadCuotas { get; set; }
        public DateTime FechaCreacion { get; set; }
        public DateTime FechaModificacion { get; set; }
    }
}