﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PedaleaCore.DTO
{
    public class DetalleProductos
    {
        public long DetalleProductoId { get; set; }
        public long ProductoId { get; set; }
        public int Stock { get; set; }
        public string Talla { get; set; }
        public string Color { get; set; }
        public DateTime FechaCreacion { get; set; }
        public DateTime FechaModificacion { get; set; }
    }
}