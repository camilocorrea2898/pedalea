﻿using Microsoft.Extensions.Configuration;
using NLog;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace PedaleaCore.DAO
{
    public class XrefPromocionesDepartamentos : ConexionBD
    {
        SqlDataReader LeerFilas;
        SqlCommand Comando = new SqlCommand();
        public ILogger _Logger = LogManager.GetCurrentClassLogger();

        public XrefPromocionesDepartamentos()
        {
            var builder = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json", optional: false);
            IConfiguration config = builder.Build();
            Conexion.ConnectionString = config.GetValue<string>("ConnectionStringPedalea");
        }
        public List<DTO.XrefPromocionesDepartamentos> TraerXrefPromocionesDepartamentos()
        {
            _Logger.Debug($"Se ingresa al método TraerXrefPromocionesDepartamentos");
            List<DTO.XrefPromocionesDepartamentos> ObjListXrefPromocionesDepartamentos = new List<DTO.XrefPromocionesDepartamentos>();
            try
            {

                Comando.Connection = Conexion;
                Comando.CommandText = "SP_XrefPromocionesDepartamentosTraerTodo";
                Comando.CommandType = CommandType.StoredProcedure;
                Conexion.Open();
                _Logger.Debug($"Se va a ejecutar el SP {Comando.CommandText}");
                LeerFilas = Comando.ExecuteReader();

                while (LeerFilas.Read())
                {
                    ObjListXrefPromocionesDepartamentos.Add(new DTO.XrefPromocionesDepartamentos
                    {
                        PromocionId = LeerFilas.GetInt64(0),
                        DepartamentoVentaId = LeerFilas.GetInt64(1)
                    });
                }
                Comando.Parameters.Clear();
                LeerFilas.Close();
                Conexion.Close();
                _Logger.Debug($"Se va a consulto correctamente el SP {Comando.CommandText}");

            }
            catch (Exception ex)
            {
                _Logger.Error($"Se genero una excepción: {ex.Message}");
            }
            _Logger.Debug($"Se finaliza método TraerXrefPromocionesDepartamentos");
            return ObjListXrefPromocionesDepartamentos;
        }

        public List<DTO.XrefPromocionesDepartamentos> TraerXrefPromocionesDepartamentosPorPromocionId(long PromocionId)
        {
            _Logger.Debug($"Se ingresa al método TraerXrefPromocionesDepartamentosPorPromocionId {PromocionId}");
            List<DTO.XrefPromocionesDepartamentos> ObjListXrefPromocionesDepartamentos = new List<DTO.XrefPromocionesDepartamentos>();
            try
            {

                Comando.Connection = Conexion;
                Comando.CommandText = "SP_XrefPromocionesDepartamentosTraerPorPromocionId";
                Comando.CommandType = CommandType.StoredProcedure;

                SqlParameter parametro = new SqlParameter();
                parametro.ParameterName = "@PromocionId";
                parametro.Value = PromocionId;

                Comando.Parameters.Add(parametro);

                Conexion.Open();
                _Logger.Debug($"Se va a ejecutar el SP {Comando.CommandText}");
                LeerFilas = Comando.ExecuteReader();

                while (LeerFilas.Read())
                {
                    ObjListXrefPromocionesDepartamentos.Add(new DTO.XrefPromocionesDepartamentos
                    {
                        PromocionId = LeerFilas.GetInt64(0),
                        DepartamentoVentaId = LeerFilas.GetInt64(1)
                    });
                }
                Comando.Parameters.Clear();
                LeerFilas.Close();
                Conexion.Close();
                _Logger.Debug($"Se va a consulto correctamente el SP {Comando.CommandText}");

            }
            catch (Exception ex)
            {
                _Logger.Error($"Se genero una excepción: {ex.Message}");
            }
            _Logger.Debug($"Se finaliza método TraerXrefPromocionesDepartamentosPorPromocionId");
            return ObjListXrefPromocionesDepartamentos;
        }

        public List<DTO.XrefPromocionesDepartamentos> TraerXrefPromocionesDepartamentosPorDepartamentoId(long DepartamentoVentaId)
        {
            _Logger.Debug($"Se ingresa al método TraerXrefPromocionesDepartamentosPorDepartamentoId {DepartamentoVentaId}");
            List<DTO.XrefPromocionesDepartamentos> ObjListXrefPromocionesDepartamentos = new List<DTO.XrefPromocionesDepartamentos>();
            try
            {

                Comando.Connection = Conexion;
                Comando.CommandText = "SP_XrefPromocionesDepartamentosTraerPorPromocionId";
                Comando.CommandType = CommandType.StoredProcedure;

                SqlParameter parametro = new SqlParameter();
                parametro.ParameterName = "@DepartamentoVentaId";
                parametro.Value = DepartamentoVentaId;

                Comando.Parameters.Add(parametro);

                Conexion.Open();
                _Logger.Debug($"Se va a ejecutar el SP {Comando.CommandText}");
                LeerFilas = Comando.ExecuteReader();

                while (LeerFilas.Read())
                {
                    ObjListXrefPromocionesDepartamentos.Add(new DTO.XrefPromocionesDepartamentos
                    {
                        PromocionId = LeerFilas.GetInt64(0),
                        DepartamentoVentaId = LeerFilas.GetInt64(1)
                    });
                }
                Comando.Parameters.Clear();
                LeerFilas.Close();
                Conexion.Close();
                _Logger.Debug($"Se va a consulto correctamente el SP {Comando.CommandText}");

            }
            catch (Exception ex)
            {
                _Logger.Error($"Se genero una excepción: {ex.Message}");
            }
            _Logger.Debug($"Se finaliza método TraerXrefPromocionesDepartamentosPorDepartamentoId");
            return ObjListXrefPromocionesDepartamentos;
        }

        public DTO.XrefPromocionesDepartamentos InsertarXrefPromocionesDepartamentos(DTO.XrefPromocionesDepartamentos ObjInsertar)
        {
            _Logger.Debug($"Se ingresa al método InsertarXrefPromocionesDepartamentos con lo siguientes datos:");
            _Logger.Debug($"\n{Newtonsoft.Json.JsonConvert.SerializeObject(ObjInsertar, Newtonsoft.Json.Formatting.Indented)}");
            DTO.XrefPromocionesDepartamentos ObjXrefPromocionesDepartamentos = new DTO.XrefPromocionesDepartamentos();
            try
            {

                Comando.Connection = Conexion;
                Comando.CommandText = "SP_XrefPromocionesDepartamentosInsertar";
                Comando.CommandType = CommandType.StoredProcedure;

                Comando.Parameters.Add(new SqlParameter("@PromocionId", ObjInsertar.PromocionId));
                Comando.Parameters.Add(new SqlParameter("@DepartamentoVentaId", ObjInsertar.DepartamentoVentaId));


                Conexion.Open();
                _Logger.Debug($"Se va a ejecutar el SP {Comando.CommandText}");
                LeerFilas = Comando.ExecuteReader();

                while (LeerFilas.Read())
                {
                    ObjXrefPromocionesDepartamentos = (new DTO.XrefPromocionesDepartamentos
                    {
                        PromocionId = LeerFilas.GetInt64(0),
                        DepartamentoVentaId = LeerFilas.GetInt64(1)
                    });
                    break;
                }
                Comando.Parameters.Clear();
                LeerFilas.Close();
                Conexion.Close();

                if (ObjXrefPromocionesDepartamentos.PromocionId > 0 && ObjXrefPromocionesDepartamentos.DepartamentoVentaId > 0)
                {
                    _Logger.Debug($"Se insertaron los siguientes datos:");
                    _Logger.Debug($"\n{Newtonsoft.Json.JsonConvert.SerializeObject(ObjXrefPromocionesDepartamentos, Newtonsoft.Json.Formatting.Indented)}");
                    _Logger.Debug($"Se ejecuto correctamente el SP {Comando.CommandText}");
                }
                else
                {
                    _Logger.Error($"Ocurrio algo inesperado al ejecutar el SP {Comando.CommandText}");
                }
            }
            catch (Exception ex)
            {
                _Logger.Error($"Se genero una excepción: {ex.Message}");
            }
            _Logger.Debug($"Se finaliza método InsertarXrefPromocionesDepartamentos");
            return ObjXrefPromocionesDepartamentos;
        }

        public DTO.XrefPromocionesDepartamentos ActualizarXrefPromocionesDepartamentos(DTO.XrefPromocionesDepartamentosUpdate ObjActualizar)
        {
            _Logger.Debug($"Se ingresa al método ActualizarXrefPromocionesDepartamentos con lo siguientes datos:");
            _Logger.Debug($"\nLast\n{Newtonsoft.Json.JsonConvert.SerializeObject(ObjActualizar, Newtonsoft.Json.Formatting.Indented)}");
            DTO.XrefPromocionesDepartamentos ObjXrefPromocionesDepartamentos = new DTO.XrefPromocionesDepartamentos();
            try
            {

                Comando.Connection = Conexion;
                Comando.CommandText = "SP_XrefPromocionesDepartamentosActualizar";
                Comando.CommandType = CommandType.StoredProcedure;

                Comando.Parameters.Add(new SqlParameter("@PromocionIdLast", ObjActualizar.XrefPromocionesDepartamentosLast.PromocionId));
                Comando.Parameters.Add(new SqlParameter("@PromocionIdNew", ObjActualizar.XrefPromocionesDepartamentosNew.DepartamentoVentaId));
                Comando.Parameters.Add(new SqlParameter("@DepartamentoVentaIdLast", ObjActualizar.XrefPromocionesDepartamentosLast.DepartamentoVentaId));
                Comando.Parameters.Add(new SqlParameter("@DepartamentoVentaIdNew", ObjActualizar.XrefPromocionesDepartamentosNew.DepartamentoVentaId));

                Conexion.Open();
                _Logger.Debug($"Se va a ejecutar el SP {Comando.CommandText}");
                LeerFilas = Comando.ExecuteReader();

                while (LeerFilas.Read())
                {
                    ObjXrefPromocionesDepartamentos = (new DTO.XrefPromocionesDepartamentos
                    {
                        PromocionId = LeerFilas.GetInt64(0),
                        DepartamentoVentaId = LeerFilas.GetInt64(1)
                    });
                    break;
                }
                Comando.Parameters.Clear();
                LeerFilas.Close();
                Conexion.Close();

                if (ObjXrefPromocionesDepartamentos.PromocionId > 0 && ObjXrefPromocionesDepartamentos.DepartamentoVentaId > 0)
                {
                    _Logger.Debug($"Se actualizaron los siguientes datos:");
                    _Logger.Debug($"\n{Newtonsoft.Json.JsonConvert.SerializeObject(ObjXrefPromocionesDepartamentos, Newtonsoft.Json.Formatting.Indented)}");
                    _Logger.Debug($"Se ejecuto correctamente el SP {Comando.CommandText}");
                }
                else
                {
                    _Logger.Error($"Ocurrio algo inesperado al ejecutar el SP {Comando.CommandText}");
                }
            }
            catch (Exception ex)
            {
                _Logger.Error($"Se genero una excepción: {ex.Message}");
            }
            _Logger.Debug($"Se finaliza método ActualizarXrefPromocionesDepartamentos");
            return ObjXrefPromocionesDepartamentos;
        }

        public bool EliminarXrefPromocionesDepartamentos(long XrefPromocionesDepartamentosId)
        {
            _Logger.Debug($"Se ingresa al método EliminarXrefPromocionesDepartamentos con lo siguientes datos:");
            _Logger.Debug($"\n{Newtonsoft.Json.JsonConvert.SerializeObject(XrefPromocionesDepartamentosId, Newtonsoft.Json.Formatting.Indented)}");
            DTO.XrefPromocionesDepartamentos ObjXrefPromocionesDepartamentos = new DTO.XrefPromocionesDepartamentos();
            try
            {

                Comando.Connection = Conexion;
                Comando.CommandText = "SP_XrefPromocionesDepartamentosEliminar";
                Comando.CommandType = CommandType.StoredProcedure;

                Comando.Parameters.Add(new SqlParameter("@XrefPromocionesDepartamentosId", XrefPromocionesDepartamentosId));

                SqlParameter StatusParam = new SqlParameter("@Status", SqlDbType.Int)
                {
                    Direction = ParameterDirection.Output
                };
                Comando.Parameters.Add(StatusParam);

                Conexion.Open();
                _Logger.Debug($"Se va a ejecutar el SP {Comando.CommandText}");
                Comando.ExecuteNonQuery();

                int Status = int.Parse(StatusParam.Value.ToString());

                Comando.Parameters.Clear();
                Conexion.Close();
                if (Status == 1)
                {
                    return true;
                }
                _Logger.Error($"Ocurrio algo inesperado al ejecutar el SP {Comando.CommandText}");
            }
            catch (Exception ex)
            {
                _Logger.Error($"Se genero una excepción: {ex.Message}");
            }
            _Logger.Debug($"Se finaliza método EliminarXrefPromocionesDepartamentos");
            return false;
        }
    }
}