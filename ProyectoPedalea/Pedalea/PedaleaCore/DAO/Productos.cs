﻿using Microsoft.Extensions.Configuration;
using NLog;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace PedaleaCore.DAO
{
    public class Productos : ConexionBD
    {
        SqlDataReader LeerFilas;
        SqlCommand Comando = new SqlCommand();
        public ILogger _Logger = LogManager.GetCurrentClassLogger();

        public Productos()
        {
            var builder = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json", optional: false);
            IConfiguration config = builder.Build();
            Conexion.ConnectionString = config.GetValue<string>("ConnectionStringPedalea");
        }
        public List<DTO.Productos> TraerProductos()
        {
            _Logger.Debug($"Se ingresa al método TraerProductos");
            List<DTO.Productos> ObjListProductos = new List<DTO.Productos>();
            try
            {

                Comando.Connection = Conexion;
                Comando.CommandText = "SP_ProductosTraerTodo";
                Comando.CommandType = CommandType.StoredProcedure;
                Conexion.Open();
                _Logger.Debug($"Se va a ejecutar el SP {Comando.CommandText}");
                LeerFilas = Comando.ExecuteReader();

                while (LeerFilas.Read())
                {
                    ObjListProductos.Add(new DTO.Productos
                    {
                        ProductoId = LeerFilas.GetInt64(0),
                        Descripcion = LeerFilas.GetString(1),
                        Valor = (float)LeerFilas.GetSqlDecimal(2).Value,
                        DepartamentoVentaId = LeerFilas.GetInt64(3),
                        FechaCreacion = LeerFilas.GetDateTime(4),
                        FechaModificacion = LeerFilas.GetDateTime(5)
                    });
                }
                Comando.Parameters.Clear();
                LeerFilas.Close();
                Conexion.Close();
                _Logger.Debug($"Se va a consulto correctamente el SP {Comando.CommandText}");

            }
            catch (Exception ex)
            {
                _Logger.Error($"Se genero una excepción: {ex.Message}");
            }
            _Logger.Debug($"Se finaliza método TraerProductos");
            return ObjListProductos;
        }

        public DTO.Productos TraerProductosPorId(long ProductoId)
        {
            _Logger.Debug($"Se ingresa al método TraerProductosPorId {ProductoId}");
            DTO.Productos ObjProductos = new DTO.Productos();
            try
            {

                Comando.Connection = Conexion;
                Comando.CommandText = "SP_ProductosTraerPorDetalleId";
                Comando.CommandType = CommandType.StoredProcedure;

                SqlParameter parametro = new SqlParameter();
                parametro.ParameterName = "@ProductoId";
                parametro.Value = ProductoId;

                Comando.Parameters.Add(parametro);

                Conexion.Open();
                _Logger.Debug($"Se va a ejecutar el SP {Comando.CommandText}");
                LeerFilas = Comando.ExecuteReader();

                while (LeerFilas.Read())
                {
                    ObjProductos = (new DTO.Productos
                    {
                        ProductoId = LeerFilas.GetInt64(0),
                        Descripcion = LeerFilas.GetString(1),
                        Valor = (float)LeerFilas.GetSqlDecimal(2).Value,
                        DepartamentoVentaId = LeerFilas.GetInt64(3),
                        FechaCreacion = LeerFilas.GetDateTime(4),
                        FechaModificacion = LeerFilas.GetDateTime(5)
                    });
                    break;
                }
                Comando.Parameters.Clear();
                LeerFilas.Close();
                Conexion.Close();
                _Logger.Debug($"Se consulto correctamente el SP {Comando.CommandText}");

            }
            catch (Exception ex)
            {
                _Logger.Error($"Se genero una excepción: {ex.Message}");
            }
            _Logger.Debug($"Se finaliza método TraerProductosPorId");
            return ObjProductos;
        }

        public DTO.ProductosDisponiblesDepartamento TraerProductosDisponibles()
        {
            _Logger.Debug($"Se ingresa al método TraerProductosDisponibles");
            DTO.ProductosDisponiblesDepartamento ObjDepartamentoVentas = new DTO.ProductosDisponiblesDepartamento();
            try
            {

                Comando.Connection = Conexion;
                Comando.CommandText = "SP_TraerProductosDisponibles";
                Comando.CommandType = CommandType.StoredProcedure;


                Conexion.Open();
                _Logger.Debug($"Se va a ejecutar el SP {Comando.CommandText}");
                LeerFilas = Comando.ExecuteReader();

                List<DTO.ProductosDisponibles> ObjListaProductos = new List<DTO.ProductosDisponibles>();

                while (LeerFilas.Read())
                {
                    ObjDepartamentoVentas.PuntoVenta = LeerFilas.GetString(4);
                    ObjDepartamentoVentas.DireccionVenta = LeerFilas.GetString(5);
                    ObjDepartamentoVentas.CiudadPuntoVenta = LeerFilas.GetString(6);

                    DTO.ProductosDisponibles ObjProductos = (new DTO.ProductosDisponibles
                    {
                        ProductoId = LeerFilas.GetInt64(0),
                        StockTotal = LeerFilas.GetInt32(1),
                        DescripcionProducto = LeerFilas.GetString(2),
                        ValorProducto = (float)LeerFilas.GetSqlDecimal(3).Value
                    });
                    ObjListaProductos.Add(ObjProductos);
                }
                ObjDepartamentoVentas.ListaProductos = ObjListaProductos;

                Comando.Parameters.Clear();
                LeerFilas.Close();
                Conexion.Close();
                _Logger.Debug($"Se consulto correctamente el SP {Comando.CommandText}");

            }
            catch (Exception ex)
            {
                _Logger.Error($"Se genero una excepción: {ex.Message}");
            }
            _Logger.Debug($"Se finaliza método TraerProductosDisponibles");
            return ObjDepartamentoVentas;
        }

        public DTO.ProductosDisponiblesDepartamento TraerProductosDisponiblesPorDepartamento(long DepartamentoVentaId)
        {
            _Logger.Debug($"Se ingresa al método TraerProductosDisponiblesPorDepartamento DepartamentoVentaId {DepartamentoVentaId} ");
            DTO.ProductosDisponiblesDepartamento ObjDepartamentoVentas = new DTO.ProductosDisponiblesDepartamento();
            try
            {

                Comando.Connection = Conexion;
                Comando.CommandText = "SP_TraerProductosDisponiblesPorDepartamento";
                Comando.CommandType = CommandType.StoredProcedure;

                SqlParameter parametro = new SqlParameter();
                parametro.ParameterName = "@DepartamentoVentaId";
                parametro.Value = DepartamentoVentaId;

                Comando.Parameters.Add(parametro);

                Conexion.Open();
                _Logger.Debug($"Se va a ejecutar el SP {Comando.CommandText}");
                LeerFilas = Comando.ExecuteReader();               

                List<DTO.ProductosDisponibles> ObjListaProductos = new List<DTO.ProductosDisponibles>();

                while (LeerFilas.Read())
                {
                    ObjDepartamentoVentas.PuntoVenta = LeerFilas.GetString(4);
                    ObjDepartamentoVentas.DireccionVenta = LeerFilas.GetString(5);
                    ObjDepartamentoVentas.CiudadPuntoVenta = LeerFilas.GetString(6);

                    DTO.ProductosDisponibles ObjProductos = (new DTO.ProductosDisponibles {
                        ProductoId = LeerFilas.GetInt64(0),
                        StockTotal = LeerFilas.GetInt32(1),
                        DescripcionProducto = LeerFilas.GetString(2),
                        ValorProducto = (float)LeerFilas.GetSqlDecimal(3).Value 
                    });
                    ObjListaProductos.Add(ObjProductos);
                }
                ObjDepartamentoVentas.ListaProductos = ObjListaProductos;

                Comando.Parameters.Clear();
                LeerFilas.Close();
                Conexion.Close();
                _Logger.Debug($"Se consulto correctamente el SP {Comando.CommandText}");

            }
            catch (Exception ex)
            {
                _Logger.Error($"Se genero una excepción: {ex.Message}");
            }
            _Logger.Debug($"Se finaliza método TraerProductosDisponiblesPorDepartamento");
            return ObjDepartamentoVentas;
        }

        public DTO.Productos InsertarProductos(DTO.Productos ObjInsertar)
        {
            _Logger.Debug($"Se ingresa al método InsertarProductos con lo siguientes datos:");
            _Logger.Debug($"\n{Newtonsoft.Json.JsonConvert.SerializeObject(ObjInsertar, Newtonsoft.Json.Formatting.Indented)}");
            DTO.Productos ObjProductos = new DTO.Productos();
            try
            {

                Comando.Connection = Conexion;
                Comando.CommandText = "SP_ProductosInsertar";
                Comando.CommandType = CommandType.StoredProcedure;

                Comando.Parameters.Add(new SqlParameter("@Descripcion", ObjInsertar.Descripcion));
                Comando.Parameters.Add(new SqlParameter("@Valor", ObjInsertar.Valor));
                Comando.Parameters.Add(new SqlParameter("@DepartamentoVentaId", ObjInsertar.DepartamentoVentaId));

                Conexion.Open();
                _Logger.Debug($"Se va a ejecutar el SP {Comando.CommandText}");
                LeerFilas = Comando.ExecuteReader();

                while (LeerFilas.Read())
                {
                    ObjProductos = (new DTO.Productos
                    {
                        ProductoId = LeerFilas.GetInt64(0),
                        Descripcion = LeerFilas.GetString(1),
                        Valor = (float)LeerFilas.GetSqlDecimal(2).Value,
                        DepartamentoVentaId = LeerFilas.GetInt64(3),
                        FechaCreacion = LeerFilas.GetDateTime(4),
                        FechaModificacion = LeerFilas.GetDateTime(5)
                    });
                    break;
                }
                Comando.Parameters.Clear();
                LeerFilas.Close();
                Conexion.Close();

                if (ObjProductos.ProductoId > 0)
                {
                    _Logger.Debug($"Se insertaron los siguientes datos:");
                    _Logger.Debug($"\n{Newtonsoft.Json.JsonConvert.SerializeObject(ObjProductos, Newtonsoft.Json.Formatting.Indented)}");
                    _Logger.Debug($"Se ejecuto correctamente el SP {Comando.CommandText}");
                }
                else
                {
                    _Logger.Error($"Ocurrio algo inesperado al ejecutar el SP {Comando.CommandText}");
                }
            }
            catch (Exception ex)
            {
                _Logger.Error($"Se genero una excepción: {ex.Message}");
            }
            _Logger.Debug($"Se finaliza método InsertarProductos");
            return ObjProductos;
        }

        public DTO.Productos ActualizarProductos(DTO.Productos ObjActualizar)
        {
            _Logger.Debug($"Se ingresa al método ActualizarProductos con lo siguientes datos:");
            _Logger.Debug($"\n{Newtonsoft.Json.JsonConvert.SerializeObject(ObjActualizar, Newtonsoft.Json.Formatting.Indented)}");
            DTO.Productos ObjProductos = new DTO.Productos();
            try
            {

                Comando.Connection = Conexion;
                Comando.CommandText = "SP_ProductosActualizar";
                Comando.CommandType = CommandType.StoredProcedure;

                Comando.Parameters.Add(new SqlParameter("@ProductoId", ObjActualizar.ProductoId));
                Comando.Parameters.Add(new SqlParameter("@Descripcion", ObjActualizar.Descripcion));
                Comando.Parameters.Add(new SqlParameter("@Valor", ObjActualizar.Valor));
                Comando.Parameters.Add(new SqlParameter("@DepartamentoVentaId", ObjActualizar.DepartamentoVentaId));

                Conexion.Open();
                _Logger.Debug($"Se va a ejecutar el SP {Comando.CommandText}");
                LeerFilas = Comando.ExecuteReader();

                while (LeerFilas.Read())
                {
                    ObjProductos = (new DTO.Productos
                    {
                        ProductoId = LeerFilas.GetInt64(0),
                        Descripcion = LeerFilas.GetString(1),
                        Valor = (float)LeerFilas.GetSqlDecimal(2).Value,
                        DepartamentoVentaId = LeerFilas.GetInt64(3),
                        FechaCreacion = LeerFilas.GetDateTime(4),
                        FechaModificacion = LeerFilas.GetDateTime(5)
                    });
                    break;
                }
                Comando.Parameters.Clear();
                LeerFilas.Close();
                Conexion.Close();

                if (ObjProductos.ProductoId > 0)
                {
                    _Logger.Debug($"Se actualizaron los siguientes datos:");
                    _Logger.Debug($"\n{Newtonsoft.Json.JsonConvert.SerializeObject(ObjProductos, Newtonsoft.Json.Formatting.Indented)}");
                    _Logger.Debug($"Se ejecuto correctamente el SP {Comando.CommandText}");
                }
                else
                {
                    _Logger.Error($"Ocurrio algo inesperado al ejecutar el SP {Comando.CommandText}");
                }
            }
            catch (Exception ex)
            {
                _Logger.Error($"Se genero una excepción: {ex.Message}");
            }
            _Logger.Debug($"Se finaliza método ActualizarProductos");
            return ObjProductos;
        }

        public bool EliminarProductos(long ProductoId)
        {
            _Logger.Debug($"Se ingresa al método EliminarProductos con lo siguientes datos:");
            _Logger.Debug($"\n{Newtonsoft.Json.JsonConvert.SerializeObject(ProductoId, Newtonsoft.Json.Formatting.Indented)}");
            DTO.Productos ObjProductos = new DTO.Productos();
            try
            {

                Comando.Connection = Conexion;
                Comando.CommandText = "SP_ProductosEliminar";
                Comando.CommandType = CommandType.StoredProcedure;

                Comando.Parameters.Add(new SqlParameter("@ProductoId", ProductoId));

                SqlParameter StatusParam = new SqlParameter("@Status", SqlDbType.Int)
                {
                    Direction = ParameterDirection.Output
                };
                Comando.Parameters.Add(StatusParam);

                Conexion.Open();
                _Logger.Debug($"Se va a ejecutar el SP {Comando.CommandText}");
                Comando.ExecuteNonQuery();

                int Status = int.Parse(StatusParam.Value.ToString());

                Comando.Parameters.Clear();
                Conexion.Close();
                if (Status == 1)
                {
                    return true;
                }
                _Logger.Error($"Ocurrio algo inesperado al ejecutar el SP {Comando.CommandText}");
            }
            catch (Exception ex)
            {
                _Logger.Error($"Se genero una excepción: {ex.Message}");
            }
            _Logger.Debug($"Se finaliza método EliminarProductos");
            return false;
        }
    }
}