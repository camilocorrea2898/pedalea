﻿using Microsoft.Extensions.Configuration;
using NLog;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;


namespace PedaleaCore.DAO
{
    public class Pedidos : ConexionBD
    {
        SqlDataReader LeerFilas;
        SqlCommand Comando = new SqlCommand();
        public ILogger _Logger = LogManager.GetCurrentClassLogger();

        public Pedidos()
        {
            var builder = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json", optional: false);
            IConfiguration config = builder.Build();
            Conexion.ConnectionString = config.GetValue<string>("ConnectionStringPedalea");
        }
        public List<DTO.Pedidos> TraerPedidos()
        {
            _Logger.Debug($"Se ingresa al método TraerPedidos");
            List<DTO.Pedidos> ObjListPedidos = new List<DTO.Pedidos>();
            try
            {

                Comando.Connection = Conexion;
                Comando.CommandText = "SP_PedidosTraerTodo";
                Comando.CommandType = CommandType.StoredProcedure;
                Conexion.Open();
                _Logger.Debug($"Se va a ejecutar el SP {Comando.CommandText}");
                LeerFilas = Comando.ExecuteReader();

                while (LeerFilas.Read())
                {
                    ObjListPedidos.Add(new DTO.Pedidos
                    {
                        PedidoId = LeerFilas.GetInt64(0),
                        FechaPedido = LeerFilas.GetDateTime(1),
                        EstadoPedidoId = LeerFilas.GetInt32(2),
                        DireccionEnvio = LeerFilas.GetString(3),
                        FechaEnvio = LeerFilas.GetDateTime(4),
                        IdentificacionCliente = LeerFilas.GetString(5),
                        SubTotal = (float)LeerFilas.GetSqlDecimal(6).Value,
                        ValorTotal = (float)LeerFilas.GetSqlDecimal(7).Value,
                        PromocionId = LeerFilas.GetInt64(8),
                        FechaCreacion = LeerFilas.GetDateTime(9),
                        FechaModificacion = LeerFilas.GetDateTime(10)
                    });
                }
                Comando.Parameters.Clear();
                LeerFilas.Close();
                Conexion.Close();
                _Logger.Debug($"Se va a consulto correctamente el SP {Comando.CommandText}");

            }
            catch (Exception ex)
            {
                _Logger.Error($"Se genero una excepción: {ex.Message}");
            }
            _Logger.Debug($"Se finaliza método TraerPedidos");
            return ObjListPedidos;
        }

        public DTO.Pedidos TraerPedidosPorId(long PedidoId)
        {
            _Logger.Debug($"Se ingresa al método TraerPedidosPorId {PedidoId}");
            DTO.Pedidos ObjPedidos = new DTO.Pedidos();
            try
            {

                Comando.Connection = Conexion;
                Comando.CommandText = "SP_PedidosTraerPorDetalleId";
                Comando.CommandType = CommandType.StoredProcedure;

                SqlParameter parametro = new SqlParameter();
                parametro.ParameterName = "@PedidoId";
                parametro.Value = PedidoId;

                Comando.Parameters.Add(parametro);

                Conexion.Open();
                _Logger.Debug($"Se va a ejecutar el SP {Comando.CommandText}");
                LeerFilas = Comando.ExecuteReader();

                while (LeerFilas.Read())
                {
                    ObjPedidos = (new DTO.Pedidos
                    {
                        PedidoId = LeerFilas.GetInt64(0),
                        FechaPedido = LeerFilas.GetDateTime(1),
                        EstadoPedidoId = LeerFilas.GetInt32(2),
                        DireccionEnvio = LeerFilas.GetString(3),
                        FechaEnvio = LeerFilas.GetDateTime(4),
                        IdentificacionCliente = LeerFilas.GetString(5),
                        SubTotal = (float)LeerFilas.GetSqlDecimal(6).Value,
                        ValorTotal = (float)LeerFilas.GetSqlDecimal(7).Value,
                        PromocionId = LeerFilas.GetInt64(8),
                        FechaCreacion = LeerFilas.GetDateTime(9),
                        FechaModificacion = LeerFilas.GetDateTime(10)
                    });
                    break;
                }
                Comando.Parameters.Clear();
                LeerFilas.Close();
                Conexion.Close();
                _Logger.Debug($"Se consulto correctamente el SP {Comando.CommandText}");

            }
            catch (Exception ex)
            {
                _Logger.Error($"Se genero una excepción: {ex.Message}");
            }
            _Logger.Debug($"Se finaliza método TraerPedidosPorId");
            return ObjPedidos;
        }

        public DTO.Pedidos TraerPedidosPorIdentificacionCliente(string IdentificacionCliente)
        {
            _Logger.Debug($"Se ingresa al método TraerPedidosPorIdentificacionCliente{IdentificacionCliente}");
            DTO.Pedidos ObjPedidos = new DTO.Pedidos();
            try
            {

                Comando.Connection = Conexion;
                Comando.CommandText = "SP_PedidosTraerPorIdentificacionCliente";
                Comando.CommandType = CommandType.StoredProcedure;

                SqlParameter parametro = new SqlParameter();
                parametro.ParameterName = "@IdentificacionCliente";
                parametro.Value = IdentificacionCliente;

                Comando.Parameters.Add(parametro);

                Conexion.Open();
                _Logger.Debug($"Se va a ejecutar el SP {Comando.CommandText}");
                LeerFilas = Comando.ExecuteReader();

                while (LeerFilas.Read())
                {
                    ObjPedidos = (new DTO.Pedidos
                    {
                        PedidoId = LeerFilas.GetInt64(0),
                        FechaPedido = LeerFilas.GetDateTime(1),
                        EstadoPedidoId = LeerFilas.GetInt32(2),
                        DireccionEnvio = LeerFilas.GetString(3),
                        FechaEnvio = LeerFilas.GetDateTime(4),
                        IdentificacionCliente = LeerFilas.GetString(5),
                        SubTotal = (float)LeerFilas.GetSqlDecimal(6).Value,
                        ValorTotal = (float)LeerFilas.GetSqlDecimal(7).Value,
                        PromocionId = LeerFilas.GetInt64(8),
                        FechaCreacion = LeerFilas.GetDateTime(9),
                        FechaModificacion = LeerFilas.GetDateTime(10)
                    });
                    break;
                }
                Comando.Parameters.Clear();
                LeerFilas.Close();
                Conexion.Close();
                _Logger.Debug($"Se consulto correctamente el SP {Comando.CommandText}");

            }
            catch (Exception ex)
            {
                _Logger.Error($"Se genero una excepción: {ex.Message}");
            }
            _Logger.Debug($"Se finaliza método TraerPedidosPorIdentificacionCliente");
            return ObjPedidos;
        }

        public DTO.Pedidos InsertarPedidos(DTO.Pedidos ObjInsertar)
        {
            _Logger.Debug($"Se ingresa al método InsertarPedidos con lo siguientes datos:");
            _Logger.Debug($"\n{Newtonsoft.Json.JsonConvert.SerializeObject(ObjInsertar, Newtonsoft.Json.Formatting.Indented)}");
            DTO.Pedidos ObjPedidos = new DTO.Pedidos();
            try
            {

                Comando.Connection = Conexion;
                Comando.CommandText = "SP_PedidosInsertar";
                Comando.CommandType = CommandType.StoredProcedure;
                Comando.Parameters.Add(new SqlParameter("@FechaPedido", ObjInsertar.FechaPedido));
                Comando.Parameters.Add(new SqlParameter("@EstadoPedidoId", ObjInsertar.EstadoPedidoId));
                Comando.Parameters.Add(new SqlParameter("@DireccionEnvio", ObjInsertar.DireccionEnvio));
                Comando.Parameters.Add(new SqlParameter("@FechaEnvio",  ObjInsertar.FechaEnvio));
                Comando.Parameters.Add(new SqlParameter("@IdentificacionCliente", ObjInsertar.IdentificacionCliente));
                Comando.Parameters.Add(new SqlParameter("@SubTotal", ObjInsertar.SubTotal));
                Comando.Parameters.Add(new SqlParameter("@ValorTotal", ObjInsertar.ValorTotal));
                Comando.Parameters.Add(new SqlParameter("@PromocionId", ObjInsertar.PromocionId));

                Conexion.Open();
                _Logger.Debug($"Se va a ejecutar el SP {Comando.CommandText}");
                LeerFilas = Comando.ExecuteReader();

                while (LeerFilas.Read())
                {
                    ObjPedidos = (new DTO.Pedidos
                    {
                        PedidoId = LeerFilas.GetInt64(0),
                        FechaPedido = LeerFilas.GetDateTime(1),
                        EstadoPedidoId = LeerFilas.GetInt32(2),
                        DireccionEnvio = LeerFilas.GetString(3),
                        FechaEnvio = LeerFilas.GetDateTime(4),
                        IdentificacionCliente = LeerFilas.GetString(5),
                        SubTotal = (float)LeerFilas.GetSqlDecimal(6).Value,
                        ValorTotal = (float)LeerFilas.GetSqlDecimal(7).Value,
                        PromocionId = LeerFilas.GetInt64(8),
                        FechaCreacion = LeerFilas.GetDateTime(9),
                        FechaModificacion = LeerFilas.GetDateTime(10)
                    });
                    break;
                }
                Comando.Parameters.Clear();
                LeerFilas.Close();
                Conexion.Close();

                if (ObjPedidos.EstadoPedidoId > 0)
                {
                    _Logger.Debug($"Se insertaron los siguientes datos:");
                    _Logger.Debug($"\n{Newtonsoft.Json.JsonConvert.SerializeObject(ObjPedidos, Newtonsoft.Json.Formatting.Indented)}");
                    _Logger.Debug($"Se ejecuto correctamente el SP {Comando.CommandText}");
                }
                else
                {
                    _Logger.Error($"Ocurrio algo inesperado al ejecutar el SP {Comando.CommandText}");
                }
            }
            catch (Exception ex)
            {
                _Logger.Error($"Se genero una excepción: {ex.Message}");
            }
            _Logger.Debug($"Se finaliza método InsertarPedidos");
            return ObjPedidos;
        }

        public DTO.Pedidos ActualizarPedidos(DTO.Pedidos ObjActualizar)
        {
            _Logger.Debug($"Se ingresa al método ActualizarPedidos con lo siguientes datos:");
            _Logger.Debug($"\n{Newtonsoft.Json.JsonConvert.SerializeObject(ObjActualizar, Newtonsoft.Json.Formatting.Indented)}");
            DTO.Pedidos ObjPedidos = new DTO.Pedidos();
            try
            {

                Comando.Connection = Conexion;
                Comando.CommandText = "SP_PedidosActualizar";
                Comando.CommandType = CommandType.StoredProcedure;

                Comando.Parameters.Add(new SqlParameter("@PedidoId", ObjActualizar.PedidoId));
                Comando.Parameters.Add(new SqlParameter("@FechaPedido", ObjActualizar.FechaPedido));
                Comando.Parameters.Add(new SqlParameter("@EstadoPedidoId", ObjActualizar.EstadoPedidoId));
                Comando.Parameters.Add(new SqlParameter("@DireccionEnvio", ObjActualizar.DireccionEnvio));
                Comando.Parameters.Add(new SqlParameter("@FechaEnvio", ObjActualizar.FechaEnvio));
                Comando.Parameters.Add(new SqlParameter("@IdentificacionCliente", ObjActualizar.IdentificacionCliente));
                Comando.Parameters.Add(new SqlParameter("@SubTotal", ObjActualizar.SubTotal));
                Comando.Parameters.Add(new SqlParameter("@ValorTotal", ObjActualizar.ValorTotal));
                Comando.Parameters.Add(new SqlParameter("@PromocionId", ObjActualizar.PromocionId));

                Conexion.Open();
                _Logger.Debug($"Se va a ejecutar el SP {Comando.CommandText}");
                LeerFilas = Comando.ExecuteReader();

                while (LeerFilas.Read())
                {
                    ObjPedidos = (new DTO.Pedidos
                    {
                        PedidoId = LeerFilas.GetInt64(0),
                        FechaPedido = LeerFilas.GetDateTime(1),
                        EstadoPedidoId = LeerFilas.GetInt32(2),
                        DireccionEnvio = LeerFilas.GetString(3),
                        FechaEnvio = LeerFilas.GetDateTime(4),
                        IdentificacionCliente = LeerFilas.GetString(5),
                        SubTotal = (float)LeerFilas.GetSqlDecimal(6).Value,
                        ValorTotal = (float)LeerFilas.GetSqlDecimal(7).Value,
                        PromocionId = LeerFilas.GetInt64(8),
                        FechaCreacion = LeerFilas.GetDateTime(9),
                        FechaModificacion = LeerFilas.GetDateTime(10)
                    });
                    break;
                }
                Comando.Parameters.Clear();
                LeerFilas.Close();
                Conexion.Close();

                if (ObjPedidos.PedidoId > 0)
                {
                    _Logger.Debug($"Se actualizaron los siguientes datos:");
                    _Logger.Debug($"\n{Newtonsoft.Json.JsonConvert.SerializeObject(ObjPedidos, Newtonsoft.Json.Formatting.Indented)}");
                    _Logger.Debug($"Se ejecuto correctamente el SP {Comando.CommandText}");
                }
                else
                {
                    _Logger.Error($"Ocurrio algo inesperado al ejecutar el SP {Comando.CommandText}");
                }
            }
            catch (Exception ex)
            {
                _Logger.Error($"Se genero una excepción: {ex.Message}");
            }
            _Logger.Debug($"Se finaliza método ActualizarPedidos");
            return ObjPedidos;
        }

        public bool EliminarPedidos(long PedidoId)
        {
            _Logger.Debug($"Se ingresa al método EliminarPedidos con lo siguientes datos:");
            _Logger.Debug($"\n{Newtonsoft.Json.JsonConvert.SerializeObject(PedidoId, Newtonsoft.Json.Formatting.Indented)}");
            DTO.Pedidos ObjPedidos = new DTO.Pedidos();
            try
            {

                Comando.Connection = Conexion;
                Comando.CommandText = "SP_PedidosEliminar";
                Comando.CommandType = CommandType.StoredProcedure;

                Comando.Parameters.Add(new SqlParameter("@PedidoId", PedidoId));

                SqlParameter StatusParam = new SqlParameter("@Status", SqlDbType.Int)
                {
                    Direction = ParameterDirection.Output
                };
                Comando.Parameters.Add(StatusParam);

                Conexion.Open();
                _Logger.Debug($"Se va a ejecutar el SP {Comando.CommandText}");
                Comando.ExecuteNonQuery();

                int Status = int.Parse(StatusParam.Value.ToString());

                Comando.Parameters.Clear();
                Conexion.Close();
                if (Status == 1)
                {
                    return true;
                }
                _Logger.Error($"Ocurrio algo inesperado al ejecutar el SP {Comando.CommandText}");
            }
            catch (Exception ex)
            {
                _Logger.Error($"Se genero una excepción: {ex.Message}");
            }
            _Logger.Debug($"Se finaliza método EliminarPedidos");
            return false;
        }
    }
}