﻿using Microsoft.Extensions.Configuration;
using NLog;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace PedaleaCore.DAO
{
    public class DepartamentosVenta: ConexionBD
    {
        SqlDataReader LeerFilas;
        SqlCommand Comando = new SqlCommand();
        public ILogger _Logger = LogManager.GetCurrentClassLogger();

        public DepartamentosVenta()
        {
            var builder = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json", optional: false);
            IConfiguration config = builder.Build();
            Conexion.ConnectionString = config.GetValue<string>("ConnectionStringPedalea");
        }
        

        public List<DTO.DepartamentosVenta> TraerDepartamentosVenta()
        {
            _Logger.Debug($"Se ingresa al método TraerDepartamentosVenta");
            List<DTO.DepartamentosVenta> ObjListDepartamentoVentas = new List<DTO.DepartamentosVenta>();
            try
            {
                
                Comando.Connection = Conexion;
                Comando.CommandText = "SP_DepartamentosVentaTraerTodo";
                Comando.CommandType = CommandType.StoredProcedure;
                Conexion.Open();
                _Logger.Debug($"Se va a ejecutar el SP {Comando.CommandText}");
                LeerFilas = Comando.ExecuteReader();

                while (LeerFilas.Read())
                {
                    ObjListDepartamentoVentas.Add(new DTO.DepartamentosVenta
                    {

                        DepartamentoVentaId = LeerFilas.GetInt64(0),
                        Descripcion = LeerFilas.GetString(1),
                        Direccion = LeerFilas.GetString(2),
                        Ciudad = LeerFilas.GetString(3),
                        Activo = LeerFilas.GetBoolean(4),
                        FechaCreacion = LeerFilas.GetDateTime(5),
                        FechaModificacion = LeerFilas.GetDateTime(6)
                    });
                }
                Comando.Parameters.Clear();
                LeerFilas.Close();
                Conexion.Close();
                _Logger.Debug($"Se va a consulto correctamente el SP {Comando.CommandText}");

            }
            catch(Exception ex)
            {
                _Logger.Error($"Se genero una excepción: {ex.Message}");
            }
            _Logger.Debug($"Se finaliza método TraerDepartamentosVenta");
            return ObjListDepartamentoVentas;
        }

        public DTO.DepartamentosVenta TraerDepartamentoVentaPorId(long DepartamentoVentaId)
        {
            _Logger.Debug($"Se ingresa al método TraerDepartamentoVentaPorId {DepartamentoVentaId}");
            DTO.DepartamentosVenta ObjDepartamentoVentas = new DTO.DepartamentosVenta();
            try
            {

                Comando.Connection = Conexion;
                Comando.CommandText = "SP_DepartamentosVentaTraerPorDetalleId";
                Comando.CommandType = CommandType.StoredProcedure;

                SqlParameter parametro = new SqlParameter();
                parametro.ParameterName = "@DepartamentoVentaId";
                parametro.Value = DepartamentoVentaId;

                Comando.Parameters.Add(parametro);

                Conexion.Open();
                _Logger.Debug($"Se va a ejecutar el SP {Comando.CommandText}");
                LeerFilas = Comando.ExecuteReader();

                while (LeerFilas.Read())
                {
                    ObjDepartamentoVentas = (new DTO.DepartamentosVenta
                    {

                        DepartamentoVentaId = LeerFilas.GetInt64(0),
                        Descripcion = LeerFilas.GetString(1),
                        Direccion = LeerFilas.GetString(2),
                        Ciudad = LeerFilas.GetString(3),
                        Activo = LeerFilas.GetBoolean(4),
                        FechaCreacion = LeerFilas.GetDateTime(5),
                        FechaModificacion = LeerFilas.GetDateTime(6)
                    });
                    break;
                }
                Comando.Parameters.Clear();
                LeerFilas.Close();
                Conexion.Close();
                _Logger.Debug($"Se consulto correctamente el SP {Comando.CommandText}");

            }
            catch (Exception ex)
            {
                _Logger.Error($"Se genero una excepción: {ex.Message}");
            }
            _Logger.Debug($"Se finaliza método TraerDepartamentoVentaPorId");
            return ObjDepartamentoVentas;
        }

        public DTO.DepartamentosVenta InsertarDepartamentoVenta(DTO.DepartamentosVenta ObjInsertar)
        {
            _Logger.Debug($"Se ingresa al método InsertarDepartamentoVenta con lo siguientes datos:");
            _Logger.Debug($"\n{Newtonsoft.Json.JsonConvert.SerializeObject(ObjInsertar, Newtonsoft.Json.Formatting.Indented)}");
            DTO.DepartamentosVenta ObjDepartamentoVentas = new DTO.DepartamentosVenta();
            try
            {

                Comando.Connection = Conexion;
                Comando.CommandText = "SP_DepartamentosVentaInsertar";
                Comando.CommandType = CommandType.StoredProcedure;

                Comando.Parameters.Add(new SqlParameter("@Descripcion", ObjInsertar.Descripcion));
                Comando.Parameters.Add(new SqlParameter("@Direccion", ObjInsertar.Direccion));
                Comando.Parameters.Add(new SqlParameter("@Ciudad", ObjInsertar.Ciudad));
                Comando.Parameters.Add(new SqlParameter("@Activo", ObjInsertar.Activo));

                Conexion.Open();
                _Logger.Debug($"Se va a ejecutar el SP {Comando.CommandText}");
                LeerFilas = Comando.ExecuteReader();

                while (LeerFilas.Read())
                {
                    ObjDepartamentoVentas = (new DTO.DepartamentosVenta
                    {

                        DepartamentoVentaId = LeerFilas.GetInt64(0),
                        Descripcion = LeerFilas.GetString(1),
                        Direccion = LeerFilas.GetString(2),
                        Ciudad = LeerFilas.GetString(3),
                        Activo = LeerFilas.GetBoolean(4),
                        FechaCreacion = LeerFilas.GetDateTime(5),
                        FechaModificacion = LeerFilas.GetDateTime(6)
                    });
                    break;
                }
                Comando.Parameters.Clear();
                LeerFilas.Close();
                Conexion.Close();

                if (ObjDepartamentoVentas.DepartamentoVentaId > 0)
                {
                    _Logger.Debug($"Se insertaron los siguientes datos:");
                    _Logger.Debug($"\n{Newtonsoft.Json.JsonConvert.SerializeObject(ObjDepartamentoVentas, Newtonsoft.Json.Formatting.Indented)}");
                    _Logger.Debug($"Se ejecuto correctamente el SP {Comando.CommandText}");
                }
                else
                {
                    _Logger.Error($"Ocurrio algo inesperado al ejecutar el SP {Comando.CommandText}");
                }
            }
            catch (Exception ex)
            {
                _Logger.Error($"Se genero una excepción: {ex.Message}");
            }
            _Logger.Debug($"Se finaliza método InsertarDepartamentoVenta");
            return ObjDepartamentoVentas;
        }

        public DTO.DepartamentosVenta ActualizarDepartamentoVenta(DTO.DepartamentosVenta ObjActualizar)
        {
            _Logger.Debug($"Se ingresa al método ActualizarDepartamentoVenta con lo siguientes datos:");
            _Logger.Debug($"\n{Newtonsoft.Json.JsonConvert.SerializeObject(ObjActualizar, Newtonsoft.Json.Formatting.Indented)}");
            DTO.DepartamentosVenta ObjDepartamentoVentas = new DTO.DepartamentosVenta();
            try
            {

                Comando.Connection = Conexion;
                Comando.CommandText = "SP_DepartamentosVentaActualizar";
                Comando.CommandType = CommandType.StoredProcedure;

                Comando.Parameters.Add(new SqlParameter("@DepartamentoVentaId", ObjActualizar.DepartamentoVentaId));
                Comando.Parameters.Add(new SqlParameter("@Descripcion", ObjActualizar.Descripcion));
                Comando.Parameters.Add(new SqlParameter("@Direccion", ObjActualizar.Direccion));
                Comando.Parameters.Add(new SqlParameter("@Ciudad", ObjActualizar.Ciudad));
                Comando.Parameters.Add(new SqlParameter("@Activo", ObjActualizar.Activo));

                Conexion.Open();
                _Logger.Debug($"Se va a ejecutar el SP {Comando.CommandText}");
                LeerFilas = Comando.ExecuteReader();

                while (LeerFilas.Read())
                {
                    ObjDepartamentoVentas = (new DTO.DepartamentosVenta
                    {

                        DepartamentoVentaId = LeerFilas.GetInt64(0),
                        Descripcion = LeerFilas.GetString(1),
                        Direccion = LeerFilas.GetString(2),
                        Ciudad = LeerFilas.GetString(3),
                        Activo = LeerFilas.GetBoolean(4),
                        FechaCreacion = LeerFilas.GetDateTime(5),
                        FechaModificacion = LeerFilas.GetDateTime(6)
                    });
                    break;
                }
                Comando.Parameters.Clear();
                LeerFilas.Close();
                Conexion.Close();

                if (ObjDepartamentoVentas.DepartamentoVentaId > 0)
                {
                    _Logger.Debug($"Se actualizaron los siguientes datos:");
                    _Logger.Debug($"\n{Newtonsoft.Json.JsonConvert.SerializeObject(ObjDepartamentoVentas, Newtonsoft.Json.Formatting.Indented)}");
                    _Logger.Debug($"Se ejecuto correctamente el SP {Comando.CommandText}");
                }
                else
                {
                    _Logger.Error($"Ocurrio algo inesperado al ejecutar el SP {Comando.CommandText}");
                }
            }
            catch (Exception ex)
            {
                _Logger.Error($"Se genero una excepción: {ex.Message}");
            }
            _Logger.Debug($"Se finaliza método ActualizarDepartamentoVenta");
            return ObjDepartamentoVentas;
        }

        public bool EliminarDepartamentoVenta(long DepartamentoVentaId)
        {
            _Logger.Debug($"Se ingresa al método EliminarDepartamentoVenta: {DepartamentoVentaId}");
            try
            {

                Comando.Connection = Conexion;
                Comando.CommandText = "SP_DepartamentosVentaEliminar";
                Comando.CommandType = CommandType.StoredProcedure;

                Comando.Parameters.Add(new SqlParameter("@DepartamentoVentaId", DepartamentoVentaId));

                SqlParameter StatusParam = new SqlParameter("@Status", SqlDbType.Int)
                {
                    Direction = ParameterDirection.Output
                };
                Comando.Parameters.Add(StatusParam);

                Conexion.Open();
                _Logger.Debug($"Se va a ejecutar el SP {Comando.CommandText}");
                Comando.ExecuteNonQuery();

                int Status = int.Parse(StatusParam.Value.ToString());

                Comando.Parameters.Clear();
                Conexion.Close();
                if (Status == 1)
                {
                    return true;
                }
                _Logger.Error($"Ocurrio algo inesperado al ejecutar el SP {Comando.CommandText}");
            }
            catch (Exception ex)
            {
                _Logger.Error($"Se genero una excepción: {ex.Message}");
            }
            _Logger.Debug($"Se finaliza método EliminarDepartamentoVenta");
            return false;
        }
    }
}