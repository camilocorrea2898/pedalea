﻿using Microsoft.Extensions.Configuration;
using NLog;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace PedaleaCore.DAO
{
    public class DetalleProductos : ConexionBD
    {
        SqlDataReader LeerFilas;
        SqlCommand Comando = new SqlCommand();
        public ILogger _Logger = LogManager.GetCurrentClassLogger();

        public DetalleProductos()
        {
            var builder = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json", optional: false);
            IConfiguration config = builder.Build();
            Conexion.ConnectionString = config.GetValue<string>("ConnectionStringPedalea");
        }
        public List<DTO.DetalleProductos> TraerDetalleProductos()
        {
            _Logger.Debug($"Se ingresa al método TraerDetalleProductos");
            List<DTO.DetalleProductos> ObjListDetalleProducto = new List<DTO.DetalleProductos>();
            try
            {

                Comando.Connection = Conexion;
                Comando.CommandText = "SP_DetalleProductosTraerTodo";
                Comando.CommandType = CommandType.StoredProcedure;
                Conexion.Open();
                _Logger.Debug($"Se va a ejecutar el SP {Comando.CommandText}");
                LeerFilas = Comando.ExecuteReader();

                while (LeerFilas.Read())
                {
                    ObjListDetalleProducto.Add(new DTO.DetalleProductos
                    {
                        DetalleProductoId = LeerFilas.GetInt64(0),
                        ProductoId = LeerFilas.GetInt64(1),
                        Stock = LeerFilas.GetInt32(2),
                        Talla = LeerFilas.GetString(3),
                        Color = LeerFilas.GetString(4),
                        FechaCreacion = LeerFilas.GetDateTime(5),
                        FechaModificacion = LeerFilas.GetDateTime(6)
                    });
                }
                Comando.Parameters.Clear();
                LeerFilas.Close();
                Conexion.Close();
                _Logger.Debug($"Se va a consulto correctamente el SP {Comando.CommandText}");

            }
            catch (Exception ex)
            {
                _Logger.Error($"Se genero una excepción: {ex.Message}");
            }
            _Logger.Debug($"Se finaliza método TraerDetalleProductos");
            return ObjListDetalleProducto;
        }

        public DTO.DetalleProductos TraerDetalleProductosPorId(long DetalleProductoId)
        {
            _Logger.Debug($"Se ingresa al método TraerDetalleProductosPorId {DetalleProductoId}");
            DTO.DetalleProductos ObjDetalleProducto = new DTO.DetalleProductos();
            try
            {

                Comando.Connection = Conexion;
                Comando.CommandText = "SP_DetalleProductosTraerPorDetalleId";
                Comando.CommandType = CommandType.StoredProcedure;

                SqlParameter parametro = new SqlParameter();
                parametro.ParameterName = "@DetalleProductoId";
                parametro.Value = DetalleProductoId;

                Comando.Parameters.Add(parametro);

                Conexion.Open();
                _Logger.Debug($"Se va a ejecutar el SP {Comando.CommandText}");
                LeerFilas = Comando.ExecuteReader();

                while (LeerFilas.Read())
                {
                    ObjDetalleProducto = (new DTO.DetalleProductos
                    {
                        DetalleProductoId = LeerFilas.GetInt64(0),
                        ProductoId = LeerFilas.GetInt64(1),
                        Stock = LeerFilas.GetInt32(2),
                        Talla = LeerFilas.GetString(3),
                        Color = LeerFilas.GetString(4),
                        FechaCreacion = LeerFilas.GetDateTime(5),
                        FechaModificacion = LeerFilas.GetDateTime(6)
                    });
                    break;
                }
                Comando.Parameters.Clear();
                LeerFilas.Close();
                Conexion.Close();
                _Logger.Debug($"Se consulto correctamente el SP {Comando.CommandText}");

            }
            catch (Exception ex)
            {
                _Logger.Error($"Se genero una excepción: {ex.Message}");
            }
            _Logger.Debug($"Se finaliza método TraerDetalleProductosPorId");
            return ObjDetalleProducto;
        }

        public List<DTO.DetalleProductos> TraerDetalleProductosPorProductoId(long ProductoId)
        {
            _Logger.Debug($"Se ingresa al método TraerDetalleProductosPorProductoId {ProductoId}");
            List<DTO.DetalleProductos> ObjListDetalleProducto = new List<DTO.DetalleProductos>();
            try
            {

                Comando.Connection = Conexion;
                Comando.CommandText = "SP_DetalleProductosTraerPorProductoId";
                Comando.CommandType = CommandType.StoredProcedure;

                SqlParameter parametro = new SqlParameter();
                parametro.ParameterName = "@ProductoId";
                parametro.Value = ProductoId;

                Comando.Parameters.Add(parametro);

                Conexion.Open();
                _Logger.Debug($"Se va a ejecutar el SP {Comando.CommandText}");
                LeerFilas = Comando.ExecuteReader();

                while (LeerFilas.Read())
                {
                    ObjListDetalleProducto.Add(new DTO.DetalleProductos
                    {
                        DetalleProductoId = LeerFilas.GetInt64(0),
                        ProductoId = LeerFilas.GetInt64(1),
                        Stock = LeerFilas.GetInt32(2),
                        Talla = LeerFilas.GetString(3),
                        Color = LeerFilas.GetString(4),
                        FechaCreacion = LeerFilas.GetDateTime(5),
                        FechaModificacion = LeerFilas.GetDateTime(6)
                    });
                }
                Comando.Parameters.Clear();
                LeerFilas.Close();
                Conexion.Close();
                _Logger.Debug($"Se va a consulto correctamente el SP {Comando.CommandText}");

            }
            catch (Exception ex)
            {
                _Logger.Error($"Se genero una excepción: {ex.Message}");
            }
            _Logger.Debug($"Se finaliza método TraerDetalleProductosPorProductoId");
            return ObjListDetalleProducto;
        }

        public DTO.DetalleProductos InsertarDetalleProducto(DTO.DetalleProductos ObjInsertar)
        {
            _Logger.Debug($"Se ingresa al método InsertarDetalleProducto con lo siguientes datos:");
            _Logger.Debug($"\n{Newtonsoft.Json.JsonConvert.SerializeObject(ObjInsertar, Newtonsoft.Json.Formatting.Indented)}");
            DTO.DetalleProductos ObjDetalleProducto = new DTO.DetalleProductos();
            try
            {

                Comando.Connection = Conexion;
                Comando.CommandText = "SP_DetalleProductosInsertar";
                Comando.CommandType = CommandType.StoredProcedure;

                Comando.Parameters.Add(new SqlParameter("@ProductoId", ObjInsertar.ProductoId));
                Comando.Parameters.Add(new SqlParameter("@Stock", ObjInsertar.Stock));
                Comando.Parameters.Add(new SqlParameter("@Talla", ObjInsertar.Talla));
                Comando.Parameters.Add(new SqlParameter("@Color", ObjInsertar.Color));

                Conexion.Open();
                _Logger.Debug($"Se va a ejecutar el SP {Comando.CommandText}");
                LeerFilas = Comando.ExecuteReader();

                while (LeerFilas.Read())
                {
                    ObjDetalleProducto = (new DTO.DetalleProductos
                    {
                        DetalleProductoId = LeerFilas.GetInt64(0),
                        ProductoId = LeerFilas.GetInt64(1),
                        Stock = LeerFilas.GetInt32(2),
                        Talla = LeerFilas.GetString(3),
                        Color = LeerFilas.GetString(4),
                        FechaCreacion = LeerFilas.GetDateTime(5),
                        FechaModificacion = LeerFilas.GetDateTime(6)
                    });
                    break;
                }
                Comando.Parameters.Clear();
                LeerFilas.Close();
                Conexion.Close();

                if (ObjDetalleProducto.DetalleProductoId > 0)
                {
                    _Logger.Debug($"Se insertaron los siguientes datos:");
                    _Logger.Debug($"\n{Newtonsoft.Json.JsonConvert.SerializeObject(ObjDetalleProducto, Newtonsoft.Json.Formatting.Indented)}");
                    _Logger.Debug($"Se ejecuto correctamente el SP {Comando.CommandText}");
                }
                else
                {
                    _Logger.Error($"Ocurrio algo inesperado al ejecutar el SP {Comando.CommandText}");
                }
            }
            catch (Exception ex)
            {
                _Logger.Error($"Se genero una excepción: {ex.Message}");
            }
            _Logger.Debug($"Se finaliza método InsertarDetalleProducto");
            return ObjDetalleProducto;
        }

        public DTO.DetalleProductos ActualizarDetalleProductos(DTO.DetalleProductos ObjActualizar)
        {
            _Logger.Debug($"Se ingresa al método ActualizarDetalleProductos con lo siguientes datos:");
            _Logger.Debug($"\n{Newtonsoft.Json.JsonConvert.SerializeObject(ObjActualizar, Newtonsoft.Json.Formatting.Indented)}");
            DTO.DetalleProductos ObjDetalleProducto = new DTO.DetalleProductos();
            try
            {

                Comando.Connection = Conexion;
                Comando.CommandText = "SP_DetalleProductosActualizar";
                Comando.CommandType = CommandType.StoredProcedure;

                Comando.Parameters.Add(new SqlParameter("@DetalleProductoId", ObjActualizar.DetalleProductoId));
                Comando.Parameters.Add(new SqlParameter("@ProductoId", ObjActualizar.ProductoId));
                Comando.Parameters.Add(new SqlParameter("@Stock", ObjActualizar.Stock));
                Comando.Parameters.Add(new SqlParameter("@Talla", ObjActualizar.Talla));
                Comando.Parameters.Add(new SqlParameter("@Color", ObjActualizar.Color));

                Conexion.Open();
                _Logger.Debug($"Se va a ejecutar el SP {Comando.CommandText}");
                LeerFilas = Comando.ExecuteReader();

                while (LeerFilas.Read())
                {
                    ObjDetalleProducto = (new DTO.DetalleProductos
                    {
                        DetalleProductoId = LeerFilas.GetInt64(0),
                        ProductoId = LeerFilas.GetInt64(1),
                        Stock = LeerFilas.GetInt32(2),
                        Talla = LeerFilas.GetString(3),
                        Color = LeerFilas.GetString(4),
                        FechaCreacion = LeerFilas.GetDateTime(5),
                        FechaModificacion = LeerFilas.GetDateTime(6)
                    });
                    break;
                }
                Comando.Parameters.Clear();
                LeerFilas.Close();
                Conexion.Close();

                if (ObjDetalleProducto.DetalleProductoId > 0)
                {
                    _Logger.Debug($"Se actualizaron los siguientes datos:");
                    _Logger.Debug($"\n{Newtonsoft.Json.JsonConvert.SerializeObject(ObjDetalleProducto, Newtonsoft.Json.Formatting.Indented)}");
                    _Logger.Debug($"Se ejecuto correctamente el SP {Comando.CommandText}");
                }
                else
                {
                    _Logger.Error($"Ocurrio algo inesperado al ejecutar el SP {Comando.CommandText}");
                }
            }
            catch (Exception ex)
            {
                _Logger.Error($"Se genero una excepción: {ex.Message}");
            }
            _Logger.Debug($"Se finaliza método ActualizarDetalleProductos");
            return ObjDetalleProducto;
        }

        public bool EliminarDetalleProductos(long DetalleProductoId)
        {
            _Logger.Debug($"Se ingresa al método EliminarDetalleProductos con lo siguientes datos:");
            _Logger.Debug($"\n{Newtonsoft.Json.JsonConvert.SerializeObject(DetalleProductoId, Newtonsoft.Json.Formatting.Indented)}");
            DTO.DetalleProductos ObjDetalleProducto = new DTO.DetalleProductos();
            try
            {

                Comando.Connection = Conexion;
                Comando.CommandText = "SP_DetalleProductosEliminar";
                Comando.CommandType = CommandType.StoredProcedure;

                Comando.Parameters.Add(new SqlParameter("@DetalleProductoId", DetalleProductoId));

                SqlParameter StatusParam = new SqlParameter("@Status", SqlDbType.Int)
                {
                    Direction = ParameterDirection.Output
                };
                Comando.Parameters.Add(StatusParam);

                Conexion.Open();
                _Logger.Debug($"Se va a ejecutar el SP {Comando.CommandText}");
                Comando.ExecuteNonQuery();

                int Status = int.Parse(StatusParam.Value.ToString());

                Comando.Parameters.Clear();
                Conexion.Close();
                if (Status == 1)
                {
                    return true;
                }
                _Logger.Error($"Ocurrio algo inesperado al ejecutar el SP {Comando.CommandText}");
            }
            catch (Exception ex)
            {
                _Logger.Error($"Se genero una excepción: {ex.Message}");
            }
            _Logger.Debug($"Se finaliza método EliminarDetalleProductos");
            return false;
        }
    }
}