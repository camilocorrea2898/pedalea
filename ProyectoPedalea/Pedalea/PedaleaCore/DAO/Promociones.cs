﻿using Microsoft.Extensions.Configuration;
using NLog;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace PedaleaCore.DAO
{
    public class Promociones : ConexionBD
    {
        SqlDataReader LeerFilas;
        SqlCommand Comando = new SqlCommand();
        public ILogger _Logger = LogManager.GetCurrentClassLogger();

        public Promociones()
        {
            var builder = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json", optional: false);
            IConfiguration config = builder.Build();
            Conexion.ConnectionString = config.GetValue<string>("ConnectionStringPedalea");
        }
        public List<DTO.Promociones> TraerPromociones()
        {
            _Logger.Debug($"Se ingresa al método TraerPromociones");
            List<DTO.Promociones> ObjListPromociones = new List<DTO.Promociones>();
            try
            {

                Comando.Connection = Conexion;
                Comando.CommandText = "SP_PromocionesTraerTodo";
                Comando.CommandType = CommandType.StoredProcedure;
                Conexion.Open();
                _Logger.Debug($"Se va a ejecutar el SP {Comando.CommandText}");
                LeerFilas = Comando.ExecuteReader();

                while (LeerFilas.Read())
                {
                    ObjListPromociones.Add(new DTO.Promociones
                    {
                        PromocionId = LeerFilas.GetInt64(0),
                        Descripcion = LeerFilas.GetString(1),
                        MontoMinimo = (float)LeerFilas.GetSqlDecimal(2).Value,
                        MontoMaximo = (float)LeerFilas.GetSqlDecimal(3).Value,
                        CantidadProductosMinimo = LeerFilas.GetInt32(4),
                        PorcentajePromocion = (float)LeerFilas.GetSqlDecimal(5).Value,
                        FechaCreacion = LeerFilas.GetDateTime(6),
                        FechaModificacion = LeerFilas.GetDateTime(7)
                    });
                }
                Comando.Parameters.Clear();
                LeerFilas.Close();
                Conexion.Close();
                _Logger.Debug($"Se va a consulto correctamente el SP {Comando.CommandText}");

            }
            catch (Exception ex)
            {
                _Logger.Error($"Se genero una excepción: {ex.Message}");
            }
            _Logger.Debug($"Se finaliza método TraerPromociones");
            return ObjListPromociones;
        }

        public DTO.Promociones TraerPromocionesPorId(long PromocionId)
        {
            _Logger.Debug($"Se ingresa al método TraerPromocionesPorId {PromocionId}");
            DTO.Promociones ObjPromociones = new DTO.Promociones();
            try
            {

                Comando.Connection = Conexion;
                Comando.CommandText = "SP_PromocionesTraerPorDetalleId";
                Comando.CommandType = CommandType.StoredProcedure;

                SqlParameter parametro = new SqlParameter();
                parametro.ParameterName = "@PromocionId";
                parametro.Value = PromocionId;

                Comando.Parameters.Add(parametro);

                Conexion.Open();
                _Logger.Debug($"Se va a ejecutar el SP {Comando.CommandText}");
                LeerFilas = Comando.ExecuteReader();

                while (LeerFilas.Read())
                {
                    ObjPromociones = (new DTO.Promociones
                    {
                        PromocionId = LeerFilas.GetInt64(0),
                        Descripcion = LeerFilas.GetString(1),
                        MontoMinimo = (float)LeerFilas.GetSqlDecimal(2).Value,
                        MontoMaximo = (float)LeerFilas.GetSqlDecimal(3).Value,
                        CantidadProductosMinimo = LeerFilas.GetInt32(4),
                        PorcentajePromocion = (float)LeerFilas.GetSqlDecimal(5).Value,
                        FechaCreacion = LeerFilas.GetDateTime(6),
                        FechaModificacion = LeerFilas.GetDateTime(7)
                    });
                    break;
                }
                Comando.Parameters.Clear();
                LeerFilas.Close();
                Conexion.Close();
                _Logger.Debug($"Se consulto correctamente el SP {Comando.CommandText}");

            }
            catch (Exception ex)
            {
                _Logger.Error($"Se genero una excepción: {ex.Message}");
            }
            _Logger.Debug($"Se finaliza método TraerPromocionesPorId");
            return ObjPromociones;
        }

        public DTO.Promociones InsertarPromociones(DTO.Promociones ObjInsertar)
        {
            _Logger.Debug($"Se ingresa al método InsertarPromociones con lo siguientes datos:");
            _Logger.Debug($"\n{Newtonsoft.Json.JsonConvert.SerializeObject(ObjInsertar, Newtonsoft.Json.Formatting.Indented)}");
            DTO.Promociones ObjPromociones = new DTO.Promociones();
            try
            {

                Comando.Connection = Conexion;
                Comando.CommandText = "SP_PromocionesInsertar";
                Comando.CommandType = CommandType.StoredProcedure;

                Comando.Parameters.Add(new SqlParameter("@Descripcion", ObjInsertar.Descripcion));
                Comando.Parameters.Add(new SqlParameter("@MontoMinimo", ObjInsertar.MontoMinimo));
                Comando.Parameters.Add(new SqlParameter("@MontoMaximo", ObjInsertar.MontoMaximo));
                Comando.Parameters.Add(new SqlParameter("@CantidadProductosMinimo", ObjInsertar.CantidadProductosMinimo));
                Comando.Parameters.Add(new SqlParameter("@PorcentajePromocion", ObjInsertar.PorcentajePromocion));

                Conexion.Open();
                _Logger.Debug($"Se va a ejecutar el SP {Comando.CommandText}");
                LeerFilas = Comando.ExecuteReader();

                while (LeerFilas.Read())
                {
                    ObjPromociones = (new DTO.Promociones
                    {
                        PromocionId = LeerFilas.GetInt64(0),
                        Descripcion = LeerFilas.GetString(1),
                        MontoMinimo = (float)LeerFilas.GetSqlDecimal(2).Value,
                        MontoMaximo = (float)LeerFilas.GetSqlDecimal(3).Value,
                        CantidadProductosMinimo = LeerFilas.GetInt32(4),
                        PorcentajePromocion = (float)LeerFilas.GetSqlDecimal(5).Value,
                        FechaCreacion = LeerFilas.GetDateTime(6),
                        FechaModificacion = LeerFilas.GetDateTime(7)
                    });
                    break;
                }
                Comando.Parameters.Clear();
                LeerFilas.Close();
                Conexion.Close();

                if (ObjPromociones.PromocionId > 0)
                {
                    _Logger.Debug($"Se insertaron los siguientes datos:");
                    _Logger.Debug($"\n{Newtonsoft.Json.JsonConvert.SerializeObject(ObjPromociones, Newtonsoft.Json.Formatting.Indented)}");
                    _Logger.Debug($"Se ejecuto correctamente el SP {Comando.CommandText}");
                }
                else
                {
                    _Logger.Error($"Ocurrio algo inesperado al ejecutar el SP {Comando.CommandText}");
                }
            }
            catch (Exception ex)
            {
                _Logger.Error($"Se genero una excepción: {ex.Message}");
            }
            _Logger.Debug($"Se finaliza método InsertarPromociones");
            return ObjPromociones;
        }

        public DTO.Promociones ActualizarPromociones(DTO.Promociones ObjActualizar)
        {
            _Logger.Debug($"Se ingresa al método ActualizarPromociones con lo siguientes datos:");
            _Logger.Debug($"\n{Newtonsoft.Json.JsonConvert.SerializeObject(ObjActualizar, Newtonsoft.Json.Formatting.Indented)}");
            DTO.Promociones ObjPromociones = new DTO.Promociones();
            try
            {

                Comando.Connection = Conexion;
                Comando.CommandText = "SP_PromocionesActualizar";
                Comando.CommandType = CommandType.StoredProcedure;

                Comando.Parameters.Add(new SqlParameter("@PromocionId", ObjActualizar.PromocionId));
                Comando.Parameters.Add(new SqlParameter("@Descripcion", ObjActualizar.Descripcion));
                Comando.Parameters.Add(new SqlParameter("@MontoMinimo", ObjActualizar.MontoMinimo));
                Comando.Parameters.Add(new SqlParameter("@MontoMaximo", ObjActualizar.MontoMaximo));
                Comando.Parameters.Add(new SqlParameter("@CantidadProductosMinimo", ObjActualizar.CantidadProductosMinimo));
                Comando.Parameters.Add(new SqlParameter("@PorcentajePromocion", ObjActualizar.PorcentajePromocion));

                Conexion.Open();
                _Logger.Debug($"Se va a ejecutar el SP {Comando.CommandText}");
                LeerFilas = Comando.ExecuteReader();

                while (LeerFilas.Read())
                {
                    ObjPromociones = (new DTO.Promociones
                    {
                        PromocionId = LeerFilas.GetInt64(0),
                        Descripcion = LeerFilas.GetString(1),
                        MontoMinimo = (float)LeerFilas.GetSqlDecimal(2).Value,
                        MontoMaximo = (float)LeerFilas.GetSqlDecimal(3).Value,
                        CantidadProductosMinimo = LeerFilas.GetInt32(4),
                        PorcentajePromocion = (float)LeerFilas.GetSqlDecimal(5).Value,
                        FechaCreacion = LeerFilas.GetDateTime(6),
                        FechaModificacion = LeerFilas.GetDateTime(7)
                    });
                    break;
                }
                Comando.Parameters.Clear();
                LeerFilas.Close();
                Conexion.Close();

                if (ObjPromociones.PromocionId > 0)
                {
                    _Logger.Debug($"Se actualizaron los siguientes datos:");
                    _Logger.Debug($"\n{Newtonsoft.Json.JsonConvert.SerializeObject(ObjPromociones, Newtonsoft.Json.Formatting.Indented)}");
                    _Logger.Debug($"Se ejecuto correctamente el SP {Comando.CommandText}");
                }
                else
                {
                    _Logger.Error($"Ocurrio algo inesperado al ejecutar el SP {Comando.CommandText}");
                }
            }
            catch (Exception ex)
            {
                _Logger.Error($"Se genero una excepción: {ex.Message}");
            }
            _Logger.Debug($"Se finaliza método ActualizarPromociones");
            return ObjPromociones;
        }

        public bool EliminarPromociones(long PromocionId)
        {
            _Logger.Debug($"Se ingresa al método EliminarPromociones con lo siguientes datos:");
            _Logger.Debug($"\n{Newtonsoft.Json.JsonConvert.SerializeObject(PromocionId, Newtonsoft.Json.Formatting.Indented)}");
            DTO.Promociones ObjPromociones = new DTO.Promociones();
            try
            {

                Comando.Connection = Conexion;
                Comando.CommandText = "SP_PromocionesEliminar";
                Comando.CommandType = CommandType.StoredProcedure;

                Comando.Parameters.Add(new SqlParameter("@PromocionId", PromocionId));

                SqlParameter StatusParam = new SqlParameter("@Status", SqlDbType.Int)
                {
                    Direction = ParameterDirection.Output
                };
                Comando.Parameters.Add(StatusParam);

                Conexion.Open();
                _Logger.Debug($"Se va a ejecutar el SP {Comando.CommandText}");
                Comando.ExecuteNonQuery();

                int Status = int.Parse(StatusParam.Value.ToString());

                Comando.Parameters.Clear();
                LeerFilas.Close();
                Conexion.Close();
                if (Status == 1)
                {
                    return true;
                }
                _Logger.Error($"Ocurrio algo inesperado al ejecutar el SP {Comando.CommandText}");
            }
            catch (Exception ex)
            {
                _Logger.Error($"Se genero una excepción: {ex.Message}");
            }
            _Logger.Debug($"Se finaliza método EliminarPromociones");
            return false;
        }

        public DTO.ValidarPromocionesDepartamentoResponse ValidarPromocionesPorDepartamentoYCompra(DTO.ValidarPromocionesDepartamento ObjValidate)
        {
            _Logger.Debug($"Se ingresa al método ValidarPromocionesPorDepartamentoYCompra con lo siguientes datos:");
            _Logger.Debug($"\n{Newtonsoft.Json.JsonConvert.SerializeObject(ObjValidate, Newtonsoft.Json.Formatting.Indented)}");
            DTO.ValidarPromocionesDepartamentoResponse ObjValidateResponse = new DTO.ValidarPromocionesDepartamentoResponse();
            try
            {

                Comando.Connection = Conexion;
                Comando.CommandText = "SP_PromocionesEliminar";
                Comando.CommandType = CommandType.StoredProcedure;

                Comando.Parameters.Add(new SqlParameter("@DepartamentoVentaId", ObjValidate.DepartamentoVentaId));
                Comando.Parameters.Add(new SqlParameter("@IdentificacionCliente", ObjValidate.IdentificacionCliente));
                Comando.Parameters.Add(new SqlParameter("@ValorCompra", ObjValidate.ValorCompra));
                Comando.Parameters.Add(new SqlParameter("@ValorFinal", ObjValidate.ValorCompra));

                SqlParameter AplicaSepareParam = new SqlParameter("@AplicaSepare", SqlDbType.Bit)
                {
                    Direction = ParameterDirection.Output
                };
                Comando.Parameters.Add(AplicaSepareParam);

                SqlParameter ValorFinalParam = new SqlParameter("@ValorFinal", SqlDbType.Float)
                {
                    Direction = ParameterDirection.Output
                };
                Comando.Parameters.Add(ValorFinalParam);


                Conexion.Open();
                _Logger.Debug($"Se va a ejecutar el SP {Comando.CommandText}");
                Comando.ExecuteNonQuery();

                ObjValidateResponse.AplicaSepare = bool.Parse(AplicaSepareParam.Value.ToString());
                ObjValidateResponse.ValorFinal = float.Parse(ValorFinalParam.Value.ToString());

                Comando.Parameters.Clear();
                Conexion.Close();
                
                _Logger.Error($"Ocurrio algo inesperado al ejecutar el SP {Comando.CommandText}");
            }
            catch (Exception ex)
            {
                _Logger.Error($"Se genero una excepción: {ex.Message}");
            }
            _Logger.Debug($"Se finaliza método EliminarPromociones");
            _Logger.Debug($"Se responde el método ValidarPromocionesPorDepartamentoYCompra con lo siguientes datos:");
            _Logger.Debug($"\n{Newtonsoft.Json.JsonConvert.SerializeObject(ObjValidateResponse, Newtonsoft.Json.Formatting.Indented)}");
            return ObjValidateResponse;
        }
    }
}