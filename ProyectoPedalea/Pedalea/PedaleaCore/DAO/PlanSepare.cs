﻿using Microsoft.Extensions.Configuration;
using NLog;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace PedaleaCore.DAO
{
    public class PlanSepare : ConexionBD
    {
        SqlDataReader LeerFilas;
        SqlCommand Comando = new SqlCommand();
        public ILogger _Logger = LogManager.GetCurrentClassLogger();

        public PlanSepare()
        {
            var builder = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json", optional: false);
            IConfiguration config = builder.Build();
            Conexion.ConnectionString = config.GetValue<string>("ConnectionStringPedalea");
        }
        public List<DTO.PlanSepare> TraerPlanSepare()
        {
            _Logger.Debug($"Se ingresa al método TraerPlanSepare");
            List<DTO.PlanSepare> ObjListPlanSepare = new List<DTO.PlanSepare>();
            try
            {

                Comando.Connection = Conexion;
                Comando.CommandText = "SP_PlanSepareTraerTodo";
                Comando.CommandType = CommandType.StoredProcedure;
                Conexion.Open();
                _Logger.Debug($"Se va a ejecutar el SP {Comando.CommandText}");
                LeerFilas = Comando.ExecuteReader();

                while (LeerFilas.Read())
                {
                    ObjListPlanSepare.Add(new DTO.PlanSepare
                    {
                        PlanSepareId = LeerFilas.GetInt64(0),
                        PedidoId = LeerFilas.GetInt64(1),
                        ValorTotal = (float)LeerFilas.GetSqlDecimal(2).Value,
                        ValorPagado = (float)LeerFilas.GetSqlDecimal(3).Value,
                        CantidadCuotas = LeerFilas.GetInt32(4),
                        FechaCreacion = LeerFilas.GetDateTime(5),
                        FechaModificacion = LeerFilas.GetDateTime(6)
                    });
                }
                Comando.Parameters.Clear();
                LeerFilas.Close();
                Conexion.Close();
                _Logger.Debug($"Se va a consulto correctamente el SP {Comando.CommandText}");

            }
            catch (Exception ex)
            {
                _Logger.Error($"Se genero una excepción: {ex.Message}");
            }
            _Logger.Debug($"Se finaliza método TraerPlanSepare");
            return ObjListPlanSepare;
        }

        public DTO.PlanSepare TraerPlanSeparePorId(long PlanSepareId)
        {
            _Logger.Debug($"Se ingresa al método TraerPlanSeparePorId {PlanSepareId}");
            DTO.PlanSepare ObjPlanSepare = new DTO.PlanSepare();
            try
            {

                Comando.Connection = Conexion;
                Comando.CommandText = "SP_PlanSepareTraerPorDetalleId";
                Comando.CommandType = CommandType.StoredProcedure;

                SqlParameter parametro = new SqlParameter();
                parametro.ParameterName = "@PlanSepareId";
                parametro.Value = PlanSepareId;

                Comando.Parameters.Add(parametro);

                Conexion.Open();
                _Logger.Debug($"Se va a ejecutar el SP {Comando.CommandText}");
                LeerFilas = Comando.ExecuteReader();

                while (LeerFilas.Read())
                {
                    ObjPlanSepare = (new DTO.PlanSepare
                    {
                        PlanSepareId = LeerFilas.GetInt64(0),
                        PedidoId = LeerFilas.GetInt64(1),
                        ValorTotal = (float)LeerFilas.GetSqlDecimal(2).Value,
                        ValorPagado = (float)LeerFilas.GetSqlDecimal(3).Value,
                        CantidadCuotas = LeerFilas.GetInt32(4),
                        FechaCreacion = LeerFilas.GetDateTime(5),
                        FechaModificacion = LeerFilas.GetDateTime(6)
                    });
                    break;
                }
                Comando.Parameters.Clear();
                LeerFilas.Close();
                Conexion.Close();
                _Logger.Debug($"Se consulto correctamente el SP {Comando.CommandText}");

            }
            catch (Exception ex)
            {
                _Logger.Error($"Se genero una excepción: {ex.Message}");
            }
            _Logger.Debug($"Se finaliza método TraerPlanSeparePorId");
            return ObjPlanSepare;
        }

        public DTO.PlanSepare TraerPlanSeparePorPedidoId(long PedidoId)
        {
            _Logger.Debug($"Se ingresa al método TraerPlanSeparePorPedidoId {PedidoId}");
            DTO.PlanSepare ObjPlanSepare = new DTO.PlanSepare();
            try
            {

                Comando.Connection = Conexion;
                Comando.CommandText = "SP_PlanSepareTraerPorPedidoId";
                Comando.CommandType = CommandType.StoredProcedure;

                SqlParameter parametro = new SqlParameter();
                parametro.ParameterName = "@PedidoId";
                parametro.Value = PedidoId;

                Comando.Parameters.Add(parametro);

                Conexion.Open();
                _Logger.Debug($"Se va a ejecutar el SP {Comando.CommandText}");
                LeerFilas = Comando.ExecuteReader();

                while (LeerFilas.Read())
                {
                    ObjPlanSepare = (new DTO.PlanSepare
                    {
                        PlanSepareId = LeerFilas.GetInt64(0),
                        PedidoId = LeerFilas.GetInt64(1),
                        ValorTotal = (float)LeerFilas.GetSqlDecimal(2).Value,
                        ValorPagado = (float)LeerFilas.GetSqlDecimal(3).Value,
                        CantidadCuotas = LeerFilas.GetInt32(4),
                        FechaCreacion = LeerFilas.GetDateTime(5),
                        FechaModificacion = LeerFilas.GetDateTime(6)
                    });
                    break;
                }
                Comando.Parameters.Clear();
                LeerFilas.Close();
                Conexion.Close();
                _Logger.Debug($"Se consulto correctamente el SP {Comando.CommandText}");

            }
            catch (Exception ex)
            {
                _Logger.Error($"Se genero una excepción: {ex.Message}");
            }
            _Logger.Debug($"Se finaliza método TraerPlanSeparePorId");
            return ObjPlanSepare;
        }

        public DTO.PlanSepare InsertarPlanSepare(DTO.PlanSepare ObjInsertar)
        {
            _Logger.Debug($"Se ingresa al método InsertarPlanSepare con lo siguientes datos:");
            _Logger.Debug($"\n{Newtonsoft.Json.JsonConvert.SerializeObject(ObjInsertar, Newtonsoft.Json.Formatting.Indented)}");
            DTO.PlanSepare ObjPlanSepare = new DTO.PlanSepare();
            try
            {

                Comando.Connection = Conexion;
                Comando.CommandText = "SP_PlanSepareInsertar";
                Comando.CommandType = CommandType.StoredProcedure;

                Comando.Parameters.Add(new SqlParameter("@PedidoId", ObjInsertar.PedidoId));
                Comando.Parameters.Add(new SqlParameter("@ValorTotal", ObjInsertar.ValorTotal));
                Comando.Parameters.Add(new SqlParameter("@ValorPagado", ObjInsertar.ValorPagado));
                Comando.Parameters.Add(new SqlParameter("@CantidadCuotas", ObjInsertar.CantidadCuotas));

                Conexion.Open();
                _Logger.Debug($"Se va a ejecutar el SP {Comando.CommandText}");
                LeerFilas = Comando.ExecuteReader();

                while (LeerFilas.Read())
                {
                    ObjPlanSepare = (new DTO.PlanSepare
                    {
                        PlanSepareId = LeerFilas.GetInt64(0),
                        PedidoId = LeerFilas.GetInt64(1),
                        ValorTotal = (float)LeerFilas.GetSqlDecimal(2).Value,
                        ValorPagado = (float)LeerFilas.GetSqlDecimal(3).Value,
                        CantidadCuotas = LeerFilas.GetInt32(4),
                        FechaCreacion = LeerFilas.GetDateTime(5),
                        FechaModificacion = LeerFilas.GetDateTime(6)
                    });
                    break;
                }
                Comando.Parameters.Clear();
                LeerFilas.Close();
                Conexion.Close();

                if (ObjPlanSepare.PlanSepareId > 0)
                {
                    _Logger.Debug($"Se insertaron los siguientes datos:");
                    _Logger.Debug($"\n{Newtonsoft.Json.JsonConvert.SerializeObject(ObjPlanSepare, Newtonsoft.Json.Formatting.Indented)}");
                    _Logger.Debug($"Se ejecuto correctamente el SP {Comando.CommandText}");
                }
                else
                {
                    _Logger.Error($"Ocurrio algo inesperado al ejecutar el SP {Comando.CommandText}");
                }
            }
            catch (Exception ex)
            {
                _Logger.Error($"Se genero una excepción: {ex.Message}");
            }
            _Logger.Debug($"Se finaliza método InsertarPlanSepare");
            return ObjPlanSepare;
        }

        public DTO.PlanSepare ActualizarPlanSepare(DTO.PlanSepare ObjActualizar)
        {
            _Logger.Debug($"Se ingresa al método ActualizarPlanSepare con lo siguientes datos:");
            _Logger.Debug($"\n{Newtonsoft.Json.JsonConvert.SerializeObject(ObjActualizar, Newtonsoft.Json.Formatting.Indented)}");
            DTO.PlanSepare ObjPlanSepare = new DTO.PlanSepare();
            try
            {

                Comando.Connection = Conexion;
                Comando.CommandText = "SP_PlanSepareActualizar";
                Comando.CommandType = CommandType.StoredProcedure;

                Comando.Parameters.Add(new SqlParameter("@PlanSepareId", ObjActualizar.PlanSepareId));
                Comando.Parameters.Add(new SqlParameter("@PedidoId", ObjActualizar.PedidoId));
                Comando.Parameters.Add(new SqlParameter("@ValorTotal", ObjActualizar.ValorTotal));
                Comando.Parameters.Add(new SqlParameter("@ValorPagado", ObjActualizar.ValorPagado));
                Comando.Parameters.Add(new SqlParameter("@CantidadCuotas", ObjActualizar.CantidadCuotas));

                Conexion.Open();
                _Logger.Debug($"Se va a ejecutar el SP {Comando.CommandText}");
                LeerFilas = Comando.ExecuteReader();

                while (LeerFilas.Read())
                {
                    ObjPlanSepare = (new DTO.PlanSepare
                    {
                        PlanSepareId = LeerFilas.GetInt64(0),
                        PedidoId = LeerFilas.GetInt64(1),
                        ValorTotal = (float)LeerFilas.GetSqlDecimal(2).Value,
                        ValorPagado = (float)LeerFilas.GetSqlDecimal(3).Value,
                        CantidadCuotas = LeerFilas.GetInt32(4),
                        FechaCreacion = LeerFilas.GetDateTime(5),
                        FechaModificacion = LeerFilas.GetDateTime(6)
                    });
                    break;
                }
                Comando.Parameters.Clear();
                LeerFilas.Close();
                Conexion.Close();

                if (ObjPlanSepare.PlanSepareId > 0)
                {
                    _Logger.Debug($"Se actualizaron los siguientes datos:");
                    _Logger.Debug($"\n{Newtonsoft.Json.JsonConvert.SerializeObject(ObjPlanSepare, Newtonsoft.Json.Formatting.Indented)}");
                    _Logger.Debug($"Se ejecuto correctamente el SP {Comando.CommandText}");
                }
                else
                {
                    _Logger.Error($"Ocurrio algo inesperado al ejecutar el SP {Comando.CommandText}");
                }
            }
            catch (Exception ex)
            {
                _Logger.Error($"Se genero una excepción: {ex.Message}");
            }
            _Logger.Debug($"Se finaliza método ActualizarPlanSepare");
            return ObjPlanSepare;
        }

        public bool EliminarPlanSepare(long PlanSepareId)
        {
            _Logger.Debug($"Se ingresa al método EliminarPlanSepare con lo siguientes datos:");
            _Logger.Debug($"\n{Newtonsoft.Json.JsonConvert.SerializeObject(PlanSepareId, Newtonsoft.Json.Formatting.Indented)}");
            DTO.PlanSepare ObjPlanSepare = new DTO.PlanSepare();
            try
            {

                Comando.Connection = Conexion;
                Comando.CommandText = "SP_PlanSepareEliminar";
                Comando.CommandType = CommandType.StoredProcedure;

                Comando.Parameters.Add(new SqlParameter("@PlanSepareId", PlanSepareId));

                SqlParameter StatusParam = new SqlParameter("@Status", SqlDbType.Int)
                {
                    Direction = ParameterDirection.Output
                };
                Comando.Parameters.Add(StatusParam);

                Conexion.Open();
                _Logger.Debug($"Se va a ejecutar el SP {Comando.CommandText}");
                Comando.ExecuteNonQuery();

                int Status = int.Parse(StatusParam.Value.ToString());

                Comando.Parameters.Clear();
                Conexion.Close();
                if (Status == 1)
                {
                    return true;
                }
                _Logger.Error($"Ocurrio algo inesperado al ejecutar el SP {Comando.CommandText}");
            }
            catch (Exception ex)
            {
                _Logger.Error($"Se genero una excepción: {ex.Message}");
            }
            _Logger.Debug($"Se finaliza método EliminarPlanSepare");
            return false;
        }
    }
}