﻿using Microsoft.Extensions.Configuration;
using NLog;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace PedaleaCore.DAO
{
    public class DetallePedidos : ConexionBD
    {
        SqlDataReader LeerFilas;
        SqlCommand Comando = new SqlCommand();
        public ILogger _Logger = LogManager.GetCurrentClassLogger();

        public DetallePedidos()
        {
            var builder = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json", optional: false);
            IConfiguration config = builder.Build();
            Conexion.ConnectionString = config.GetValue<string>("ConnectionStringPedalea");
        }

        public List<DTO.DetallePedidos> TraerDetallePedidos()
        {
            _Logger.Debug($"Se ingresa al método TraerDetallePedidos");
            List<DTO.DetallePedidos> ObjListDetallePedido = new List<DTO.DetallePedidos>();
            try
            {

                Comando.Connection = Conexion;
                Comando.CommandText = "SP_DetallePedidosTraerTodo";
                Comando.CommandType = CommandType.StoredProcedure;
                Conexion.Open();
                _Logger.Debug($"Se va a ejecutar el SP {Comando.CommandText}");
                LeerFilas = Comando.ExecuteReader();

                while (LeerFilas.Read())
                {
                    ObjListDetallePedido.Add(new DTO.DetallePedidos
                    {
                        DetallePedidoId = LeerFilas.GetInt64(0),
                        PedidoId = LeerFilas.GetInt64(1),
                        ProductoId = LeerFilas.GetInt64(2),
                        ValorProducto = (float)LeerFilas.GetSqlDecimal(3).Value,
                        Cantidad = LeerFilas.GetInt32(4),
                        SubTotal = (float)LeerFilas.GetSqlDecimal(5).Value,
                        FechaCreacion = LeerFilas.GetDateTime(6),
                        FechaModificacion = LeerFilas.GetDateTime(7)
                    });
                }
                Comando.Parameters.Clear();
                LeerFilas.Close();
                Conexion.Close();
                _Logger.Debug($"Se va a consulto correctamente el SP {Comando.CommandText}");

            }
            catch (Exception ex)
            {
                _Logger.Error($"Se genero una excepción: {ex.Message}");
            }
            _Logger.Debug($"Se finaliza método TraerDetallePedidos");
            return ObjListDetallePedido;
        }

        public DTO.DetallePedidos TraerDetallePedidoPorId(long DetallePedidoId)
        {
            _Logger.Debug($"Se ingresa al método TraerDetallePedidoPorId {DetallePedidoId}");
            DTO.DetallePedidos ObjDetallePedido = new DTO.DetallePedidos();
            try
            {

                Comando.Connection = Conexion;
                Comando.CommandText = "SP_DetallePedidosTraerPorDetalleId";
                Comando.CommandType = CommandType.StoredProcedure;

                SqlParameter parametro = new SqlParameter();
                parametro.ParameterName = "@DetallePedidoId";
                parametro.Value = DetallePedidoId;

                Comando.Parameters.Add(parametro);

                Conexion.Open();
                _Logger.Debug($"Se va a ejecutar el SP {Comando.CommandText}");
                LeerFilas = Comando.ExecuteReader();

                while (LeerFilas.Read())
                {
                    ObjDetallePedido = (new DTO.DetallePedidos
                    {
                        DetallePedidoId = LeerFilas.GetInt64(0),
                        PedidoId = LeerFilas.GetInt64(1),
                        ProductoId = LeerFilas.GetInt64(2),
                        ValorProducto = (float)LeerFilas.GetSqlDecimal(3).Value,
                        Cantidad = LeerFilas.GetInt32(4),
                        SubTotal = (float)LeerFilas.GetSqlDecimal(5).Value,
                        FechaCreacion = LeerFilas.GetDateTime(6),
                        FechaModificacion = LeerFilas.GetDateTime(7)
                    });
                    break;
                }
                Comando.Parameters.Clear();
                LeerFilas.Close();
                Conexion.Close();
                _Logger.Debug($"Se consulto correctamente el SP {Comando.CommandText}");

            }
            catch (Exception ex)
            {
                _Logger.Error($"Se genero una excepción: {ex.Message}");
            }
            _Logger.Debug($"Se finaliza método TraerDetallePedidoPorId");
            return ObjDetallePedido;
        }

        public List<DTO.DetallePedidos> TraerDetallePedidoPorPedidoId(long PedidoId)
        {
            _Logger.Debug($"Se ingresa al método TraerDetallePedidoPorPedidoId {PedidoId}");
            List<DTO.DetallePedidos> ObjListDetallePedido = new List<DTO.DetallePedidos>();
            try
            {

                Comando.Connection = Conexion;
                Comando.CommandText = "SP_DetallePedidosTraerPorPedidoId";
                Comando.CommandType = CommandType.StoredProcedure;

                SqlParameter parametro = new SqlParameter();
                parametro.ParameterName = "@PedidoId";
                parametro.Value = PedidoId;

                Comando.Parameters.Add(parametro);

                Conexion.Open();
                _Logger.Debug($"Se va a ejecutar el SP {Comando.CommandText}");
                LeerFilas = Comando.ExecuteReader();

                while (LeerFilas.Read())
                {
                    ObjListDetallePedido.Add(new DTO.DetallePedidos
                    {
                        DetallePedidoId = LeerFilas.GetInt64(0),
                        PedidoId = LeerFilas.GetInt64(1),
                        ProductoId = LeerFilas.GetInt64(2),
                        ValorProducto = (float)LeerFilas.GetSqlDecimal(3).Value,
                        Cantidad = LeerFilas.GetInt32(4),
                        SubTotal = (float)LeerFilas.GetSqlDecimal(5).Value,
                        FechaCreacion = LeerFilas.GetDateTime(6),
                        FechaModificacion = LeerFilas.GetDateTime(7)
                    });
                }
                Comando.Parameters.Clear();
                LeerFilas.Close();
                Conexion.Close();
                _Logger.Debug($"Se va a consulto correctamente el SP {Comando.CommandText}");

            }
            catch (Exception ex)
            {
                _Logger.Error($"Se genero una excepción: {ex.Message}");
            }
            _Logger.Debug($"Se finaliza método TraerDetallePedidoPorPedidoId");
            return ObjListDetallePedido;
        }

        public DTO.DetallePedidos InsertarDetallePedido(DTO.DetallePedidos ObjInsertar)
        {
            _Logger.Debug($"Se ingresa al método InsertarDetallePedido con lo siguientes datos:");
            _Logger.Debug($"\n{Newtonsoft.Json.JsonConvert.SerializeObject(ObjInsertar, Newtonsoft.Json.Formatting.Indented)}");
            DTO.DetallePedidos ObjDetallePedido = new DTO.DetallePedidos();
            try
            {

                Comando.Connection = Conexion;
                Comando.CommandText = "SP_DetallePedidosInsertar";
                Comando.CommandType = CommandType.StoredProcedure;

                Comando.Parameters.Add(new SqlParameter("@PedidoId", ObjInsertar.PedidoId));
                Comando.Parameters.Add(new SqlParameter("@ProductoId", ObjInsertar.ProductoId));
                Comando.Parameters.Add(new SqlParameter("@ValorProducto", ObjInsertar.ValorProducto));
                Comando.Parameters.Add(new SqlParameter("@Cantidad", ObjInsertar.Cantidad));
                Comando.Parameters.Add(new SqlParameter("@SubTotal", ObjInsertar.SubTotal));

                Conexion.Open();
                _Logger.Debug($"Se va a ejecutar el SP {Comando.CommandText}");
                LeerFilas = Comando.ExecuteReader();

                while (LeerFilas.Read())
                {
                    ObjDetallePedido = (new DTO.DetallePedidos
                    {
                        DetallePedidoId = LeerFilas.GetInt64(0),
                        PedidoId = LeerFilas.GetInt64(1),
                        ProductoId = LeerFilas.GetInt64(2),
                        ValorProducto = (float)LeerFilas.GetSqlDecimal(3).Value,
                        Cantidad = LeerFilas.GetInt32(4),
                        SubTotal = (float)LeerFilas.GetSqlDecimal(5).Value,
                        FechaCreacion = LeerFilas.GetDateTime(6),
                        FechaModificacion = LeerFilas.GetDateTime(7)
                    });
                    break;
                }
                Comando.Parameters.Clear();
                LeerFilas.Close();
                Conexion.Close();

                if (ObjDetallePedido.DetallePedidoId > 0)
                {
                    _Logger.Debug($"Se insertaron los siguientes datos:");
                    _Logger.Debug($"\n{Newtonsoft.Json.JsonConvert.SerializeObject(ObjDetallePedido, Newtonsoft.Json.Formatting.Indented)}");
                    _Logger.Debug($"Se ejecuto correctamente el SP {Comando.CommandText}");
                }
                else
                {
                    _Logger.Error($"Ocurrio algo inesperado al ejecutar el SP {Comando.CommandText}");
                }
            }
            catch (Exception ex)
            {
                _Logger.Error($"Se genero una excepción: {ex.Message}");
            }
            _Logger.Debug($"Se finaliza método InsertarDetallePedido");
            return ObjDetallePedido;
        }

        public DTO.DetallePedidos ActualizarDetallePedido(DTO.DetallePedidos ObjActualizar)
        {
            _Logger.Debug($"Se ingresa al método ActualizarDetallePedido con lo siguientes datos:");
            _Logger.Debug($"\n{Newtonsoft.Json.JsonConvert.SerializeObject(ObjActualizar, Newtonsoft.Json.Formatting.Indented)}");
            DTO.DetallePedidos ObjDetallePedido = new DTO.DetallePedidos();
            try
            {

                Comando.Connection = Conexion;
                Comando.CommandText = "SP_DetallePedidosActualizar";
                Comando.CommandType = CommandType.StoredProcedure;

                Comando.Parameters.Add(new SqlParameter("@DetallePedidoId", ObjActualizar.DetallePedidoId));
                Comando.Parameters.Add(new SqlParameter("@PedidoId", ObjActualizar.PedidoId));
                Comando.Parameters.Add(new SqlParameter("@ProductoId", ObjActualizar.ProductoId));
                Comando.Parameters.Add(new SqlParameter("@ValorProducto", ObjActualizar.ValorProducto));
                Comando.Parameters.Add(new SqlParameter("@Cantidad", ObjActualizar.Cantidad));
                Comando.Parameters.Add(new SqlParameter("@SubTotal", ObjActualizar.SubTotal));

                Conexion.Open();
                _Logger.Debug($"Se va a ejecutar el SP {Comando.CommandText}");
                LeerFilas = Comando.ExecuteReader();

                while (LeerFilas.Read())
                {
                    ObjDetallePedido = (new DTO.DetallePedidos
                    {
                        DetallePedidoId = LeerFilas.GetInt64(0),
                        PedidoId = LeerFilas.GetInt64(1),
                        ProductoId = LeerFilas.GetInt64(2),
                        ValorProducto = (float)LeerFilas.GetSqlDecimal(3).Value,
                        Cantidad = LeerFilas.GetInt32(4),
                        SubTotal = (float)LeerFilas.GetSqlDecimal(5).Value,
                        FechaCreacion = LeerFilas.GetDateTime(6),
                        FechaModificacion = LeerFilas.GetDateTime(7)
                    });
                    break;
                }
                Comando.Parameters.Clear();
                LeerFilas.Close();
                Conexion.Close();

                if (ObjDetallePedido.DetallePedidoId > 0)
                {
                    _Logger.Debug($"Se actualizaron los siguientes datos:");
                    _Logger.Debug($"\n{Newtonsoft.Json.JsonConvert.SerializeObject(ObjDetallePedido, Newtonsoft.Json.Formatting.Indented)}");
                    _Logger.Debug($"Se ejecuto correctamente el SP {Comando.CommandText}");
                }
                else
                {
                    _Logger.Error($"Ocurrio algo inesperado al ejecutar el SP {Comando.CommandText}");
                }
            }
            catch (Exception ex)
            {
                _Logger.Error($"Se genero una excepción: {ex.Message}");
            }
            _Logger.Debug($"Se finaliza método ActualizarDetallePedido");
            return ObjDetallePedido;
        }

        public bool EliminarDetallePedido(long DetallePedidoId)
        {
            _Logger.Debug($"Se ingresa al método EliminarDetallePedido con lo siguientes datos:");
            _Logger.Debug($"\n{Newtonsoft.Json.JsonConvert.SerializeObject(DetallePedidoId, Newtonsoft.Json.Formatting.Indented)}");
            DTO.DetallePedidos ObjDetallePedido = new DTO.DetallePedidos();
            try
            {

                Comando.Connection = Conexion;
                Comando.CommandText = "SP_DetallePedidosEliminar";
                Comando.CommandType = CommandType.StoredProcedure;

                Comando.Parameters.Add(new SqlParameter("@DetallePedidoId", DetallePedidoId));

                SqlParameter StatusParam = new SqlParameter("@Status", SqlDbType.Int)
                {
                    Direction = ParameterDirection.Output
                };
                Comando.Parameters.Add(StatusParam);

                Conexion.Open();
                _Logger.Debug($"Se va a ejecutar el SP {Comando.CommandText}");
                Comando.ExecuteNonQuery();

                int Status = int.Parse(StatusParam.Value.ToString());

                Comando.Parameters.Clear();
                Conexion.Close();
                if (Status == 1)
                {
                    return true;
                }
                _Logger.Error($"Ocurrio algo inesperado al ejecutar el SP {Comando.CommandText}");
            }
            catch (Exception ex)
            {
                _Logger.Error($"Se genero una excepción: {ex.Message}");
            }
            _Logger.Debug($"Se finaliza método EliminarDetallePedido");
            return false;
        }
    }
}