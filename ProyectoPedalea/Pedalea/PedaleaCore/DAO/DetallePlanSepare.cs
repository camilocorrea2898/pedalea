﻿using Microsoft.Extensions.Configuration;
using NLog;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace PedaleaCore.DAO
{
    public class DetallePlanSepare : ConexionBD
    {
        SqlDataReader LeerFilas;
        SqlCommand Comando = new SqlCommand();
        public ILogger _Logger = LogManager.GetCurrentClassLogger();

        public DetallePlanSepare()
        {
            var builder = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json", optional: false);
            IConfiguration config = builder.Build();
            Conexion.ConnectionString = config.GetValue<string>("ConnectionStringPedalea");
        }

        public List<DTO.DetallePlanSepare> TraerDetallePlanSepare()
        {
            _Logger.Debug($"Se ingresa al método TraerDetallePlanSepare");
            List<DTO.DetallePlanSepare> ObjListDetallePedido = new List<DTO.DetallePlanSepare>();
            try
            {

                Comando.Connection = Conexion;
                Comando.CommandText = "SP_DetallePlanSepareTraerTodo";
                Comando.CommandType = CommandType.StoredProcedure;
                Conexion.Open();
                _Logger.Debug($"Se va a ejecutar el SP {Comando.CommandText}");
                LeerFilas = Comando.ExecuteReader();

                while (LeerFilas.Read())
                {
                    ObjListDetallePedido.Add(new DTO.DetallePlanSepare
                    {
                        DetallePlanSepareId = LeerFilas.GetInt64(0),
                        PlanSepareId = LeerFilas.GetInt64(1),
                        FechaPago = LeerFilas.GetDateTime(2),
                        ValorPagado = (float)LeerFilas.GetSqlDecimal(3).Value,
                        MedioPago = LeerFilas.GetString(4),
                        ValorRestante = (float)LeerFilas.GetSqlDecimal(5).Value,
                        FechaCreacion = LeerFilas.GetDateTime(6),
                        FechaModificacion = LeerFilas.GetDateTime(7)
                    });
                }
                Comando.Parameters.Clear();
                LeerFilas.Close();
                Conexion.Close();
                _Logger.Debug($"Se va a consulto correctamente el SP {Comando.CommandText}");

            }
            catch (Exception ex)
            {
                _Logger.Error($"Se genero una excepción: {ex.Message}");
            }
            _Logger.Debug($"Se finaliza método TraerDetallePlanSepare");
            return ObjListDetallePedido;
        }

        public DTO.DetallePlanSepare TraerDetallePlanSeparePorId(long DetallePlanSepareId)
        {
            _Logger.Debug($"Se ingresa al método TraerDetallePlanSeparePorId {DetallePlanSepareId}");
            DTO.DetallePlanSepare ObjDetallePedido = new DTO.DetallePlanSepare();
            try
            {

                Comando.Connection = Conexion;
                Comando.CommandText = "SP_DetallePlanSepareTraerPorDetalleId";
                Comando.CommandType = CommandType.StoredProcedure;

                SqlParameter parametro = new SqlParameter();
                parametro.ParameterName = "@DetallePlanSepareId";
                parametro.Value = DetallePlanSepareId;

                Comando.Parameters.Add(parametro);

                Conexion.Open();
                _Logger.Debug($"Se va a ejecutar el SP {Comando.CommandText}");
                LeerFilas = Comando.ExecuteReader();

                while (LeerFilas.Read())
                {
                    ObjDetallePedido = (new DTO.DetallePlanSepare
                    {
                        DetallePlanSepareId = LeerFilas.GetInt64(0),
                        PlanSepareId = LeerFilas.GetInt64(1),
                        FechaPago = LeerFilas.GetDateTime(2),
                        ValorPagado = (float)LeerFilas.GetSqlDecimal(3).Value,
                        MedioPago = LeerFilas.GetString(4),
                        ValorRestante = (float)LeerFilas.GetSqlDecimal(5).Value,
                        FechaCreacion = LeerFilas.GetDateTime(6),
                        FechaModificacion = LeerFilas.GetDateTime(7)
                    });
                    break;
                }
                Comando.Parameters.Clear();
                LeerFilas.Close();
                Conexion.Close();
                _Logger.Debug($"Se consulto correctamente el SP {Comando.CommandText}");

            }
            catch (Exception ex)
            {
                _Logger.Error($"Se genero una excepción: {ex.Message}");
            }
            _Logger.Debug($"Se finaliza método TraerDetallePlanSeparePorId");
            return ObjDetallePedido;
        }

        public List<DTO.DetallePlanSepare> TraerDetallePlanSeparePorPlanSepareId(long PlanSepareId)
        {
            _Logger.Debug($"Se ingresa al método TraerDetallePlanSeparePorPlanSepareId {PlanSepareId}");
            List<DTO.DetallePlanSepare> ObjListDetallePedido = new List<DTO.DetallePlanSepare>();
            try
            {

                Comando.Connection = Conexion;
                Comando.CommandText = "SP_DetallePlanSepareTraerPorPlanSepareId";
                Comando.CommandType = CommandType.StoredProcedure;

                SqlParameter parametro = new SqlParameter();
                parametro.ParameterName = "@PlanSepareId";
                parametro.Value = PlanSepareId;

                Comando.Parameters.Add(parametro);

                Conexion.Open();
                _Logger.Debug($"Se va a ejecutar el SP {Comando.CommandText}");
                LeerFilas = Comando.ExecuteReader();

                while (LeerFilas.Read())
                {
                    ObjListDetallePedido.Add(new DTO.DetallePlanSepare
                    {
                        DetallePlanSepareId = LeerFilas.GetInt64(0),
                        PlanSepareId = LeerFilas.GetInt64(1),
                        FechaPago = LeerFilas.GetDateTime(2),
                        ValorPagado = (float)LeerFilas.GetSqlDecimal(3).Value,
                        MedioPago = LeerFilas.GetString(4),
                        ValorRestante = (float)LeerFilas.GetSqlDecimal(5).Value,
                        FechaCreacion = LeerFilas.GetDateTime(6),
                        FechaModificacion = LeerFilas.GetDateTime(7)
                    });
                }
                Comando.Parameters.Clear();
                LeerFilas.Close();
                Conexion.Close();
                _Logger.Debug($"Se va a consulto correctamente el SP {Comando.CommandText}");

            }
            catch (Exception ex)
            {
                _Logger.Error($"Se genero una excepción: {ex.Message}");
            }
            _Logger.Debug($"Se finaliza método TraerDetallePlanSeparePorPlanSepareId");
            return ObjListDetallePedido;
        }

        public DTO.DetallePlanSepare InsertarDetallePlanSepare(DTO.DetallePlanSepare ObjInsertar)
        {
            _Logger.Debug($"Se ingresa al método InsertarDetallePlanSepare con lo siguientes datos:");
            _Logger.Debug($"\n{Newtonsoft.Json.JsonConvert.SerializeObject(ObjInsertar, Newtonsoft.Json.Formatting.Indented)}");
            DTO.DetallePlanSepare ObjDetallePedido = new DTO.DetallePlanSepare();
            try
            {

                Comando.Connection = Conexion;
                Comando.CommandText = "SP_DetallePlanSepareInsertar";
                Comando.CommandType = CommandType.StoredProcedure;

                Comando.Parameters.Add(new SqlParameter("@PlanSepareId", ObjInsertar.PlanSepareId));
                Comando.Parameters.Add(new SqlParameter("@FechaPago", ObjInsertar.FechaPago));
                Comando.Parameters.Add(new SqlParameter("@ValorPagado", ObjInsertar.ValorPagado));
                Comando.Parameters.Add(new SqlParameter("@MedioPago", ObjInsertar.MedioPago));
                Comando.Parameters.Add(new SqlParameter("@ValorRestante", ObjInsertar.ValorRestante));

                Conexion.Open();
                _Logger.Debug($"Se va a ejecutar el SP {Comando.CommandText}");
                LeerFilas = Comando.ExecuteReader();

                while (LeerFilas.Read())
                {
                    ObjDetallePedido = (new DTO.DetallePlanSepare
                    {
                        DetallePlanSepareId = LeerFilas.GetInt64(0),
                        PlanSepareId = LeerFilas.GetInt64(1),
                        FechaPago = LeerFilas.GetDateTime(2),
                        ValorPagado = (float)LeerFilas.GetSqlDecimal(3).Value,
                        MedioPago = LeerFilas.GetString(4),
                        ValorRestante = (float)LeerFilas.GetSqlDecimal(5).Value,
                        FechaCreacion = LeerFilas.GetDateTime(6),
                        FechaModificacion = LeerFilas.GetDateTime(7)
                    });
                    break;
                }
                Comando.Parameters.Clear();
                LeerFilas.Close();
                Conexion.Close();

                if (ObjDetallePedido.DetallePlanSepareId > 0)
                {
                    _Logger.Debug($"Se insertaron los siguientes datos:");
                    _Logger.Debug($"\n{Newtonsoft.Json.JsonConvert.SerializeObject(ObjDetallePedido, Newtonsoft.Json.Formatting.Indented)}");
                    _Logger.Debug($"Se ejecuto correctamente el SP {Comando.CommandText}");
                }
                else
                {
                    _Logger.Error($"Ocurrio algo inesperado al ejecutar el SP {Comando.CommandText}");
                }
            }
            catch (Exception ex)
            {
                _Logger.Error($"Se genero una excepción: {ex.Message}");
            }
            _Logger.Debug($"Se finaliza método InsertarDetallePedido");
            return ObjDetallePedido;
        }

        public DTO.DetallePlanSepare ActualizarDetallePlanSepare(DTO.DetallePlanSepare ObjActualizar)
        {
            _Logger.Debug($"Se ingresa al método ActualizarDetallePlanSepare con lo siguientes datos:");
            _Logger.Debug($"\n{Newtonsoft.Json.JsonConvert.SerializeObject(ObjActualizar, Newtonsoft.Json.Formatting.Indented)}");
            DTO.DetallePlanSepare ObjDetallePedido = new DTO.DetallePlanSepare();
            try
            {

                Comando.Connection = Conexion;
                Comando.CommandText = "SP_DetallePlanSepareActualizar";
                Comando.CommandType = CommandType.StoredProcedure;

                Comando.Parameters.Add(new SqlParameter("@DetallePlanSepareId", ObjActualizar.DetallePlanSepareId));
                Comando.Parameters.Add(new SqlParameter("@PlanSepareId", ObjActualizar.PlanSepareId));
                Comando.Parameters.Add(new SqlParameter("@FechaPago", ObjActualizar.FechaPago));
                Comando.Parameters.Add(new SqlParameter("@ValorPagado", ObjActualizar.ValorPagado));
                Comando.Parameters.Add(new SqlParameter("@MedioPago", ObjActualizar.MedioPago));
                Comando.Parameters.Add(new SqlParameter("@ValorRestante", ObjActualizar.ValorRestante));

                Conexion.Open();
                _Logger.Debug($"Se va a ejecutar el SP {Comando.CommandText}");
                LeerFilas = Comando.ExecuteReader();

                while (LeerFilas.Read())
                {
                    ObjDetallePedido = (new DTO.DetallePlanSepare
                    {
                        DetallePlanSepareId = LeerFilas.GetInt64(0),
                        PlanSepareId = LeerFilas.GetInt64(1),
                        FechaPago = LeerFilas.GetDateTime(2),
                        ValorPagado = (float)LeerFilas.GetSqlDecimal(3).Value,
                        MedioPago = LeerFilas.GetString(4),
                        ValorRestante = (float)LeerFilas.GetSqlDecimal(5).Value,
                        FechaCreacion = LeerFilas.GetDateTime(6),
                        FechaModificacion = LeerFilas.GetDateTime(7)
                    });
                    break;
                }
                Comando.Parameters.Clear();
                LeerFilas.Close();
                Conexion.Close();

                if (ObjDetallePedido.DetallePlanSepareId > 0)
                {
                    _Logger.Debug($"Se actualizaron los siguientes datos:");
                    _Logger.Debug($"\n{Newtonsoft.Json.JsonConvert.SerializeObject(ObjDetallePedido, Newtonsoft.Json.Formatting.Indented)}");
                    _Logger.Debug($"Se ejecuto correctamente el SP {Comando.CommandText}");
                }
                else
                {
                    _Logger.Error($"Ocurrio algo inesperado al ejecutar el SP {Comando.CommandText}");
                }
            }
            catch (Exception ex)
            {
                _Logger.Error($"Se genero una excepción: {ex.Message}");
            }
            _Logger.Debug($"Se finaliza método ActualizarDetallePlanSepare");
            return ObjDetallePedido;
        }

        public bool EliminarDetallePlanSepare(long DetallePlanSepareId)
        {
            _Logger.Debug($"Se ingresa al método EliminarDetallePlanSepare con lo siguientes datos:");
            _Logger.Debug($"\n{Newtonsoft.Json.JsonConvert.SerializeObject(DetallePlanSepareId, Newtonsoft.Json.Formatting.Indented)}");
            DTO.DetallePlanSepare ObjDetallePedido = new DTO.DetallePlanSepare();
            try
            {

                Comando.Connection = Conexion;
                Comando.CommandText = "SP_DetallePlanSepareEliminar";
                Comando.CommandType = CommandType.StoredProcedure;

                Comando.Parameters.Add(new SqlParameter("@DetallePlanSepareId", DetallePlanSepareId));

                SqlParameter StatusParam = new SqlParameter("@Status", SqlDbType.Int)
                {
                    Direction = ParameterDirection.Output
                };
                Comando.Parameters.Add(StatusParam);

                Conexion.Open();
                _Logger.Debug($"Se va a ejecutar el SP {Comando.CommandText}");
                Comando.ExecuteNonQuery();

                int Status = int.Parse(StatusParam.Value.ToString());

                Comando.Parameters.Clear();
                Conexion.Close();
                if (Status == 1)
                {
                    return true;
                }
                _Logger.Error($"Ocurrio algo inesperado al ejecutar el SP {Comando.CommandText}");
            }
            catch (Exception ex)
            {
                _Logger.Error($"Se genero una excepción: {ex.Message}");
            }
            _Logger.Debug($"Se finaliza método EliminarDetallePlanSepare");
            return false;
        }
    }
}