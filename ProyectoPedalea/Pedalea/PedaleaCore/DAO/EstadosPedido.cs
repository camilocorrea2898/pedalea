﻿using Microsoft.Extensions.Configuration;
using NLog;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace PedaleaCore.DAO
{
    public class EstadosPedido : ConexionBD
    {
        SqlDataReader LeerFilas;
        SqlCommand Comando = new SqlCommand();
        public ILogger _Logger = LogManager.GetCurrentClassLogger();

        public EstadosPedido()
        {
            var builder = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json", optional: false);
            IConfiguration config = builder.Build();
            Conexion.ConnectionString = config.GetValue<string>("ConnectionStringPedalea");
        }
        public List<DTO.EstadosPedido> TraerEstadosPedido()
        {
            _Logger.Debug($"Se ingresa al método TraerEstadosPedido");
            List<DTO.EstadosPedido> ObjListEstadosPedido = new List<DTO.EstadosPedido>();
            try
            {

                Comando.Connection = Conexion;
                Comando.CommandText = "SP_EstadosPedidoTraerTodo";
                Comando.CommandType = CommandType.StoredProcedure;
                Conexion.Open();
                _Logger.Debug($"Se va a ejecutar el SP {Comando.CommandText}");
                LeerFilas = Comando.ExecuteReader();

                while (LeerFilas.Read())
                {
                    ObjListEstadosPedido.Add(new DTO.EstadosPedido
                    {
                        EstadoPedidoId = LeerFilas.GetInt64(0),
                        Descripcion = LeerFilas.GetString(1),
                        FechaCreacion = LeerFilas.GetDateTime(2),
                        FechaModificacion = LeerFilas.GetDateTime(3)
                    });
                }
                Comando.Parameters.Clear();
                LeerFilas.Close();
                Conexion.Close();
                _Logger.Debug($"Se va a consulto correctamente el SP {Comando.CommandText}");

            }
            catch (Exception ex)
            {
                _Logger.Error($"Se genero una excepción: {ex.Message}");
            }
            _Logger.Debug($"Se finaliza método TraerEstadosPedido");
            return ObjListEstadosPedido;
        }

        public DTO.EstadosPedido TraerEstadosPedidoPorId(long EstadoPedidoId)
        {
            _Logger.Debug($"Se ingresa al método TraerEstadosPedidoPorId {EstadoPedidoId}");
            DTO.EstadosPedido ObjEstadosPedido = new DTO.EstadosPedido();
            try
            {

                Comando.Connection = Conexion;
                Comando.CommandText = "SP_EstadosPedidoTraerPorDetalleId";
                Comando.CommandType = CommandType.StoredProcedure;

                SqlParameter parametro = new SqlParameter();
                parametro.ParameterName = "@EstadoPedidoId";
                parametro.Value = EstadoPedidoId;

                Comando.Parameters.Add(parametro);

                Conexion.Open();
                _Logger.Debug($"Se va a ejecutar el SP {Comando.CommandText}");
                LeerFilas = Comando.ExecuteReader();

                while (LeerFilas.Read())
                {
                    ObjEstadosPedido = (new DTO.EstadosPedido
                    {
                        EstadoPedidoId = LeerFilas.GetInt64(0),
                        Descripcion = LeerFilas.GetString(1),
                        FechaCreacion = LeerFilas.GetDateTime(2),
                        FechaModificacion = LeerFilas.GetDateTime(3)
                    });
                    break;
                }
                Comando.Parameters.Clear();
                LeerFilas.Close();
                Conexion.Close();
                _Logger.Debug($"Se consulto correctamente el SP {Comando.CommandText}");

            }
            catch (Exception ex)
            {
                _Logger.Error($"Se genero una excepción: {ex.Message}");
            }
            _Logger.Debug($"Se finaliza método TraerEstadosPedidoPorId");
            return ObjEstadosPedido;
        }

        public DTO.EstadosPedido InsertarEstadosPedido(DTO.EstadosPedido ObjInsertar)
        {
            _Logger.Debug($"Se ingresa al método InsertarEstadosPedido con lo siguientes datos:");
            _Logger.Debug($"\n{Newtonsoft.Json.JsonConvert.SerializeObject(ObjInsertar, Newtonsoft.Json.Formatting.Indented)}");
            DTO.EstadosPedido ObjEstadosPedido = new DTO.EstadosPedido();
            try
            {

                Comando.Connection = Conexion;
                Comando.CommandText = "SP_EstadosPedidoInsertar";
                Comando.CommandType = CommandType.StoredProcedure;

                Comando.Parameters.Add(new SqlParameter("@Descripcion", ObjInsertar.Descripcion));

                Conexion.Open();
                _Logger.Debug($"Se va a ejecutar el SP {Comando.CommandText}");
                LeerFilas = Comando.ExecuteReader();

                while (LeerFilas.Read())
                {
                    ObjEstadosPedido = (new DTO.EstadosPedido
                    {
                        EstadoPedidoId = LeerFilas.GetInt64(0),
                        Descripcion = LeerFilas.GetString(1),
                        FechaCreacion = LeerFilas.GetDateTime(2),
                        FechaModificacion = LeerFilas.GetDateTime(3)
                    });
                    break;
                }
                Comando.Parameters.Clear();
                LeerFilas.Close();
                Conexion.Close();

                if (ObjEstadosPedido.EstadoPedidoId > 0)
                {
                    _Logger.Debug($"Se insertaron los siguientes datos:");
                    _Logger.Debug($"\n{Newtonsoft.Json.JsonConvert.SerializeObject(ObjEstadosPedido, Newtonsoft.Json.Formatting.Indented)}");
                    _Logger.Debug($"Se ejecuto correctamente el SP {Comando.CommandText}");
                }
                else
                {
                    _Logger.Error($"Ocurrio algo inesperado al ejecutar el SP {Comando.CommandText}");
                }
            }
            catch (Exception ex)
            {
                _Logger.Error($"Se genero una excepción: {ex.Message}");
            }
            _Logger.Debug($"Se finaliza método InsertarEstadosPedido");
            return ObjEstadosPedido;
        }

        public DTO.EstadosPedido ActualizarEstadosPedido(DTO.EstadosPedido ObjActualizar)
        {
            _Logger.Debug($"Se ingresa al método ActualizarEstadosPedido con lo siguientes datos:");
            _Logger.Debug($"\n{Newtonsoft.Json.JsonConvert.SerializeObject(ObjActualizar, Newtonsoft.Json.Formatting.Indented)}");
            DTO.EstadosPedido ObjEstadosPedido = new DTO.EstadosPedido();
            try
            {

                Comando.Connection = Conexion;
                Comando.CommandText = "SP_EstadosPedidoActualizar";
                Comando.CommandType = CommandType.StoredProcedure;

                Comando.Parameters.Add(new SqlParameter("@EstadoPedidoId", ObjActualizar.EstadoPedidoId));
                Comando.Parameters.Add(new SqlParameter("@Descripcion", ObjActualizar.Descripcion));

                Conexion.Open();
                _Logger.Debug($"Se va a ejecutar el SP {Comando.CommandText}");
                LeerFilas = Comando.ExecuteReader();

                while (LeerFilas.Read())
                {
                    ObjEstadosPedido = (new DTO.EstadosPedido
                    {
                        EstadoPedidoId = LeerFilas.GetInt64(0),
                        Descripcion = LeerFilas.GetString(1),
                        FechaCreacion = LeerFilas.GetDateTime(2),
                        FechaModificacion = LeerFilas.GetDateTime(3)
                    });
                    break;
                }
                Comando.Parameters.Clear();
                LeerFilas.Close();
                Conexion.Close();

                if (ObjEstadosPedido.EstadoPedidoId > 0)
                {
                    _Logger.Debug($"Se actualizaron los siguientes datos:");
                    _Logger.Debug($"\n{Newtonsoft.Json.JsonConvert.SerializeObject(ObjEstadosPedido, Newtonsoft.Json.Formatting.Indented)}");
                    _Logger.Debug($"Se ejecuto correctamente el SP {Comando.CommandText}");
                }
                else
                {
                    _Logger.Error($"Ocurrio algo inesperado al ejecutar el SP {Comando.CommandText}");
                }
            }
            catch (Exception ex)
            {
                _Logger.Error($"Se genero una excepción: {ex.Message}");
            }
            _Logger.Debug($"Se finaliza método ActualizarEstadosPedido");
            return ObjEstadosPedido;
        }

        public bool EliminarEstadosPedido(long EstadoPedidoId)
        {
            _Logger.Debug($"Se ingresa al método EliminarEstadosPedido con lo siguientes datos:");
            _Logger.Debug($"\n{Newtonsoft.Json.JsonConvert.SerializeObject(EstadoPedidoId, Newtonsoft.Json.Formatting.Indented)}");
            DTO.EstadosPedido ObjEstadosPedido = new DTO.EstadosPedido();
            try
            {

                Comando.Connection = Conexion;
                Comando.CommandText = "SP_EstadosPedidoEliminar";
                Comando.CommandType = CommandType.StoredProcedure;

                Comando.Parameters.Add(new SqlParameter("@EstadoPedidoId", EstadoPedidoId));

                SqlParameter StatusParam = new SqlParameter("@Status", SqlDbType.Int)
                {
                    Direction = ParameterDirection.Output
                };
                Comando.Parameters.Add(StatusParam);

                Conexion.Open();
                _Logger.Debug($"Se va a ejecutar el SP {Comando.CommandText}");
                Comando.ExecuteNonQuery();

                int Status = int.Parse(StatusParam.Value.ToString());

                Comando.Parameters.Clear();
                Conexion.Close();
                if (Status == 1)
                {
                    return true;
                }
                _Logger.Error($"Ocurrio algo inesperado al ejecutar el SP {Comando.CommandText}");
            }
            catch (Exception ex)
            {
                _Logger.Error($"Se genero una excepción: {ex.Message}");
            }
            _Logger.Debug($"Se finaliza método EliminarEstadosPedido");
            return false;
        }
    }
}