let url = "https://localhost:7233/Pedalea/DepartamentosVenta/TraerTodo"

$.get(url, function(respuesta){
    var $dropdown = $("#dropdown");
    $.each(respuesta, function() {
        $dropdown.append($("<option />").val(this.departamentoVentaId).text(this.descripcion));
    });
},"json")

$("#IrTienda").click(function() {
    if ($("#dropdown option:selected").val() =="0"){
        alert("Selecciona un departamento de venta para poder continuar.");
    }else{
        window.location.replace("ProductosDepartamento.html?departamentoVentaId="+$("#dropdown option:selected").val());
    }
});