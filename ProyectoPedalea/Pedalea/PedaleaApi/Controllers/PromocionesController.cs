﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NLog;

namespace PedaleaApi.Controllers
{
    [Route("Pedalea/[controller]")]
    [ApiController]
    public class PromocionesController : ControllerBase
    {
        public NLog.ILogger _Logger = LogManager.GetCurrentClassLogger();


        [HttpGet("TraerTodo")]
        public List<PedaleaCore.DTO.Promociones> TraerTodo()
        {
            _Logger.Debug($"-------------------------------------------------------------------------------------");
            _Logger.Debug($"Ingreso en el controlador Promociones.TraerTodo");
            List<PedaleaCore.DTO.Promociones> ObjReturn = new List<PedaleaCore.DTO.Promociones>();
            try
            {
                PedaleaCore.DAO.Promociones ObjMethod = new PedaleaCore.DAO.Promociones();
                ObjReturn= ObjMethod.TraerPromociones();
            }
            catch(Exception ex)
            {
                _Logger.Debug($"Excepcion en el metodo Promociones.TraerTodo: {ex.Message}");
            }
            _Logger.Debug($"Finaliza el controlador Promociones.TraerTodo");
            _Logger.Debug($"-------------------------------------------------------------------------------------");
            return ObjReturn;
        }

        [HttpGet("TraerPorId/{PromocionId}")]
        public PedaleaCore.DTO.Promociones TraerPorId(long PromocionId)
        {
            _Logger.Debug($"-------------------------------------------------------------------------------------");
            _Logger.Debug($"Ingreso en el controlador Promociones.TraerPorId: {PromocionId}");
            PedaleaCore.DTO.Promociones ObjReturn = new PedaleaCore.DTO.Promociones();
            try
            {
                PedaleaCore.DAO.Promociones ObjMethod = new PedaleaCore.DAO.Promociones();
                ObjReturn = ObjMethod.TraerPromocionesPorId(PromocionId);
            }
            catch (Exception ex)
            {
                _Logger.Debug($"Excepcion en el controlador Promociones.TraerPorId: {ex.Message}");
            }
            _Logger.Debug($"Finaliza el controlador Promociones.TraerPorId: {PromocionId}");
            _Logger.Debug($"-------------------------------------------------------------------------------------");
            return ObjReturn;
        }

        [HttpPost("ValidarPromociones")]
        public PedaleaCore.DTO.ValidarPromocionesDepartamentoResponse Insertar(PedaleaCore.DTO.ValidarPromocionesDepartamento ObjRequest)
        {
            _Logger.Debug($"-------------------------------------------------------------------------------------");
            _Logger.Debug($"Ingreso en el controlador Promociones.ValidarPromociones: \n{Newtonsoft.Json.JsonConvert.SerializeObject(ObjRequest, Newtonsoft.Json.Formatting.Indented)}");
            PedaleaCore.DTO.ValidarPromocionesDepartamentoResponse ObjReturn = new PedaleaCore.DTO.ValidarPromocionesDepartamentoResponse();
            try
            {
                PedaleaCore.DAO.Promociones ObjMethod = new PedaleaCore.DAO.Promociones();
                ObjReturn = ObjMethod.ValidarPromocionesPorDepartamentoYCompra(ObjRequest);
            }
            catch (Exception ex)
            {
                _Logger.Debug($"Excepcion en el controlador Promociones.ValidarPromociones: {ex.Message}");
            }
            _Logger.Debug($"Finaliza el controlador Promociones.ValidarPromociones:  \n{Newtonsoft.Json.JsonConvert.SerializeObject(ObjReturn, Newtonsoft.Json.Formatting.Indented)}");
            _Logger.Debug($"-------------------------------------------------------------------------------------");
            return ObjReturn;
        }

        [HttpPost("Insertar")]
        public PedaleaCore.DTO.Promociones Insertar(PedaleaCore.DTO.Promociones ObjRequest)
        {
            _Logger.Debug($"-------------------------------------------------------------------------------------");
            _Logger.Debug($"Ingreso en el controlador Promociones.Insertar: \n{Newtonsoft.Json.JsonConvert.SerializeObject(ObjRequest, Newtonsoft.Json.Formatting.Indented)}");
            PedaleaCore.DTO.Promociones ObjReturn = new PedaleaCore.DTO.Promociones();
            try
            {
                PedaleaCore.DAO.Promociones ObjMethod = new PedaleaCore.DAO.Promociones();
                ObjReturn = ObjMethod.InsertarPromociones(ObjRequest);
            }
            catch (Exception ex)
            {
                _Logger.Debug($"Excepcion en el controlador Promociones.Insertar: {ex.Message}");
            }
            _Logger.Debug($"Finaliza el controlador Promociones.Insertar:  \n{Newtonsoft.Json.JsonConvert.SerializeObject(ObjReturn, Newtonsoft.Json.Formatting.Indented)}");
            _Logger.Debug($"-------------------------------------------------------------------------------------");
            return ObjReturn;
        }

        [HttpPut("Actualizar/{PromocionId}")]
        public PedaleaCore.DTO.Promociones Actualizar(long PromocionId, PedaleaCore.DTO.Promociones ObjRequest)
        {
            _Logger.Debug($"-------------------------------------------------------------------------------------");
            ObjRequest.PromocionId = PromocionId;
            _Logger.Debug($"Ingreso en el controlador Promociones.Actualizar: \n{Newtonsoft.Json.JsonConvert.SerializeObject(ObjRequest, Newtonsoft.Json.Formatting.Indented)}");
            PedaleaCore.DTO.Promociones ObjReturn = new PedaleaCore.DTO.Promociones();
            try
            {
                PedaleaCore.DAO.Promociones ObjMethod = new PedaleaCore.DAO.Promociones();
                ObjReturn = ObjMethod.ActualizarPromociones(ObjRequest);
            }
            catch (Exception ex)
            {
                _Logger.Debug($"Excepcion en el controlador Promociones.Actualizar: {ex.Message}");
            }
            _Logger.Debug($"Finaliza el controlador Promociones.Actualizar:  \n{Newtonsoft.Json.JsonConvert.SerializeObject(ObjReturn, Newtonsoft.Json.Formatting.Indented)}");
            _Logger.Debug($"-------------------------------------------------------------------------------------");
            return ObjReturn;
        }

        [HttpDelete("Eliminar/{DetallePromocionId}")]
        public PedaleaCore.DTO.Parametros Eliminar(long DetallePromocionId)
        {
            _Logger.Debug($"-------------------------------------------------------------------------------------");
            _Logger.Debug($"Ingreso en el controlador Promociones.Eliminar: {DetallePromocionId}");
            PedaleaCore.DTO.Parametros ObjReturn = new PedaleaCore.DTO.Parametros();
            try
            {
                PedaleaCore.DAO.Promociones ObjMethod = new PedaleaCore.DAO.Promociones();
                ObjReturn.Eliminado = ObjMethod.EliminarPromociones(DetallePromocionId);
            }
            catch (Exception ex)
            {
                _Logger.Debug($"Excepcion en el controlador Promociones.Eliminar: {ex.Message}");
            }
            _Logger.Debug($"Finaliza el controlador Promociones.Eliminar: {ObjReturn}");
            _Logger.Debug($"-------------------------------------------------------------------------------------");
            return ObjReturn;
        }
    }
}
