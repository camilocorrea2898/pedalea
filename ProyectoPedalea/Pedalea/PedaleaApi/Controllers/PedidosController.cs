﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NLog;

namespace PedaleaApi.Controllers
{
    [Route("Pedalea/[controller]")]
    [ApiController]
    public class PedidosController : ControllerBase
    {
        public NLog.ILogger _Logger = LogManager.GetCurrentClassLogger();


        [HttpGet("TraerTodo")]
        public List<PedaleaCore.DTO.Pedidos> TraerTodo()
        {
            _Logger.Debug($"-------------------------------------------------------------------------------------");
            _Logger.Debug($"Ingreso en el controlador Pedidos.TraerTodo");
            List<PedaleaCore.DTO.Pedidos> ObjReturn = new List<PedaleaCore.DTO.Pedidos>();
            try
            {
                PedaleaCore.DAO.Pedidos ObjMethod = new PedaleaCore.DAO.Pedidos();
                ObjReturn= ObjMethod.TraerPedidos();
            }
            catch(Exception ex)
            {
                _Logger.Debug($"Excepcion en el metodo Pedidos.TraerTodo: {ex.Message}");
            }
            _Logger.Debug($"Finaliza el controlador Pedidos.TraerTodo");
            _Logger.Debug($"-------------------------------------------------------------------------------------");
            return ObjReturn;
        }

        [HttpGet("TraerPorId/{PedidoId}")]
        public PedaleaCore.DTO.Pedidos TraerPorId(long PedidoId)
        {
            _Logger.Debug($"-------------------------------------------------------------------------------------");
            _Logger.Debug($"Ingreso en el controlador Pedidos.TraerPorId: {PedidoId}");
            PedaleaCore.DTO.Pedidos ObjReturn = new PedaleaCore.DTO.Pedidos();
            try
            {
                PedaleaCore.DAO.Pedidos ObjMethod = new PedaleaCore.DAO.Pedidos();
                ObjReturn = ObjMethod.TraerPedidosPorId(PedidoId);
            }
            catch (Exception ex)
            {
                _Logger.Debug($"Excepcion en el controlador Pedidos.TraerPorId: {ex.Message}");
            }
            _Logger.Debug($"Finaliza el controlador Pedidos.TraerPorId: {PedidoId}");
            _Logger.Debug($"-------------------------------------------------------------------------------------");
            return ObjReturn;
        }

        [HttpGet("TraerPorIdentificacionCliente/{IdentificacionCliente}")]
        public PedaleaCore.DTO.Pedidos TraerPorIdentificacionCliente(string IdentificacionCliente)
        {
            _Logger.Debug($"-------------------------------------------------------------------------------------");
            _Logger.Debug($"Ingreso en el controlador Pedidos.TraerPorIdentificacionCliente: {IdentificacionCliente}");
            PedaleaCore.DTO.Pedidos ObjReturn = new PedaleaCore.DTO.Pedidos();
            try
            {
                PedaleaCore.DAO.Pedidos ObjMethod = new PedaleaCore.DAO.Pedidos();
                ObjReturn = ObjMethod.TraerPedidosPorIdentificacionCliente(IdentificacionCliente);
            }
            catch (Exception ex)
            {
                _Logger.Debug($"Excepcion en el controlador Pedidos.TraerPorIdentificacionCliente: {ex.Message}");
            }
            _Logger.Debug($"Finaliza el controlador Pedidos.TraerPorIdentificacionCliente: {IdentificacionCliente}");
            _Logger.Debug($"-------------------------------------------------------------------------------------");
            return ObjReturn;
        }

        [HttpPost("Insertar")]
        public PedaleaCore.DTO.Pedidos Insertar(PedaleaCore.DTO.Pedidos ObjRequest)
        {
            _Logger.Debug($"-------------------------------------------------------------------------------------");
            _Logger.Debug($"Ingreso en el controlador Pedidos.Insertar: \n{Newtonsoft.Json.JsonConvert.SerializeObject(ObjRequest, Newtonsoft.Json.Formatting.Indented)}");
            PedaleaCore.DTO.Pedidos ObjReturn = new PedaleaCore.DTO.Pedidos();
            try
            {
                PedaleaCore.DAO.Pedidos ObjMethod = new PedaleaCore.DAO.Pedidos();
                ObjReturn = ObjMethod.InsertarPedidos(ObjRequest);
            }
            catch (Exception ex)
            {
                _Logger.Debug($"Excepcion en el controlador Pedidos.Insertar: {ex.Message}");
            }
            _Logger.Debug($"Finaliza el controlador Pedidos.Insertar:  \n{Newtonsoft.Json.JsonConvert.SerializeObject(ObjReturn, Newtonsoft.Json.Formatting.Indented)}");
            _Logger.Debug($"-------------------------------------------------------------------------------------");
            return ObjReturn;
        }

        [HttpPut("Actualizar/{PedidoId}")]
        public PedaleaCore.DTO.Pedidos Actualizar(long PedidoId, PedaleaCore.DTO.Pedidos ObjRequest)
        {
            _Logger.Debug($"-------------------------------------------------------------------------------------");
            ObjRequest.PedidoId = PedidoId;
            _Logger.Debug($"Ingreso en el controlador Pedidos.Actualizar: \n{Newtonsoft.Json.JsonConvert.SerializeObject(ObjRequest, Newtonsoft.Json.Formatting.Indented)}");
            PedaleaCore.DTO.Pedidos ObjReturn = new PedaleaCore.DTO.Pedidos();
            try
            {
                PedaleaCore.DAO.Pedidos ObjMethod = new PedaleaCore.DAO.Pedidos();
                ObjReturn = ObjMethod.ActualizarPedidos(ObjRequest);
            }
            catch (Exception ex)
            {
                _Logger.Debug($"Excepcion en el controlador Pedidos.Actualizar: {ex.Message}");
            }
            _Logger.Debug($"Finaliza el controlador Pedidos.Actualizar:  \n{Newtonsoft.Json.JsonConvert.SerializeObject(ObjReturn, Newtonsoft.Json.Formatting.Indented)}");
            _Logger.Debug($"-------------------------------------------------------------------------------------");
            return ObjReturn;
        }

        [HttpDelete("Eliminar/{DetallePedidosId}")]
        public PedaleaCore.DTO.Parametros Eliminar(long DetallePedidosId)
        {
            _Logger.Debug($"-------------------------------------------------------------------------------------");
            _Logger.Debug($"Ingreso en el controlador Pedidos.Eliminar: {DetallePedidosId}");
            PedaleaCore.DTO.Parametros ObjReturn = new PedaleaCore.DTO.Parametros();
            try
            {
                PedaleaCore.DAO.Pedidos ObjMethod = new PedaleaCore.DAO.Pedidos();
                ObjReturn.Eliminado = ObjMethod.EliminarPedidos(DetallePedidosId);
            }
            catch (Exception ex)
            {
                _Logger.Debug($"Excepcion en el controlador Pedidos.Eliminar: {ex.Message}");
            }
            _Logger.Debug($"Finaliza el controlador Pedidos.Eliminar: {ObjReturn}");
            _Logger.Debug($"-------------------------------------------------------------------------------------");
            return ObjReturn;
        }
    }
}
