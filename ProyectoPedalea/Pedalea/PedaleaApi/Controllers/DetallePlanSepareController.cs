﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NLog;

namespace PedaleaApi.Controllers
{
    [Route("Pedalea/[controller]")]
    [ApiController]
    public class DetallePlanSepareController : ControllerBase
    {
        public NLog.ILogger _Logger = LogManager.GetCurrentClassLogger();


        [HttpGet("TraerTodo")]
        public List<PedaleaCore.DTO.DetallePlanSepare> TraerTodo()
        {
            _Logger.Debug($"-------------------------------------------------------------------------------------");
            _Logger.Debug($"Ingreso en el controlador DetallePlanSepare.TraerTodo");
            List<PedaleaCore.DTO.DetallePlanSepare> ObjReturn = new List<PedaleaCore.DTO.DetallePlanSepare>();
            try
            {
                PedaleaCore.DAO.DetallePlanSepare ObjMethod = new PedaleaCore.DAO.DetallePlanSepare();
                ObjReturn= ObjMethod.TraerDetallePlanSepare();
            }
            catch(Exception ex)
            {
                _Logger.Debug($"Excepcion en el metodo DetallePlanSepare.TraerTodo: {ex.Message}");
            }
            _Logger.Debug($"Finaliza el controlador DetallePlanSepare.TraerTodo");
            _Logger.Debug($"-------------------------------------------------------------------------------------");
            return ObjReturn;
        }

        [HttpGet("TraerPorId/{DetalleDetallePlanSepareId}")]
        public PedaleaCore.DTO.DetallePlanSepare TraerPorId(long DetalleDetallePlanSepareId)
        {
            _Logger.Debug($"-------------------------------------------------------------------------------------");
            _Logger.Debug($"Ingreso en el controlador DetallePlanSepare.TraerPorId: {DetalleDetallePlanSepareId}");
            PedaleaCore.DTO.DetallePlanSepare ObjReturn = new PedaleaCore.DTO.DetallePlanSepare();
            try
            {
                PedaleaCore.DAO.DetallePlanSepare ObjMethod = new PedaleaCore.DAO.DetallePlanSepare();
                ObjReturn = ObjMethod.TraerDetallePlanSeparePorId(DetalleDetallePlanSepareId);
            }
            catch (Exception ex)
            {
                _Logger.Debug($"Excepcion en el controlador DetallePlanSepare.TraerPorId: {ex.Message}");
            }
            _Logger.Debug($"Finaliza el controlador DetallePlanSepare.TraerPorId: {DetalleDetallePlanSepareId}");
            _Logger.Debug($"-------------------------------------------------------------------------------------");
            return ObjReturn;
        }

        [HttpGet("TraerPorPlanSepareId/{PlanSepareId}")]
        public List<PedaleaCore.DTO.DetallePlanSepare> TraerPorPlanSepareId(long PlanSepareId)
        {
            _Logger.Debug($"-------------------------------------------------------------------------------------");
            _Logger.Debug($"Ingreso en el controlador DetallePlanSepare.TraerPorPlanSepareId: {PlanSepareId}");
            List<PedaleaCore.DTO.DetallePlanSepare> ObjReturn = new List<PedaleaCore.DTO.DetallePlanSepare>();
            try
            {
                PedaleaCore.DAO.DetallePlanSepare ObjMethod = new PedaleaCore.DAO.DetallePlanSepare();
                ObjReturn = ObjMethod.TraerDetallePlanSeparePorPlanSepareId(PlanSepareId);
            }
            catch (Exception ex)
            {
                _Logger.Debug($"Excepcion en el controlador DetallePlanSepare.TraerPorPlanSepareId: {ex.Message}");
            }
            _Logger.Debug($"Finaliza el controlador DetallePlanSepare.TraerPorPlanSepareId: {PlanSepareId}");
            _Logger.Debug($"-------------------------------------------------------------------------------------");
            return ObjReturn;
        }

        [HttpPost("Insertar")]
        public PedaleaCore.DTO.DetallePlanSepare Insertar(PedaleaCore.DTO.DetallePlanSepare ObjRequest)
        {
            _Logger.Debug($"-------------------------------------------------------------------------------------");
            _Logger.Debug($"Ingreso en el controlador DetallePlanSepare.Insertar: \n{Newtonsoft.Json.JsonConvert.SerializeObject(ObjRequest, Newtonsoft.Json.Formatting.Indented)}");
            PedaleaCore.DTO.DetallePlanSepare ObjReturn = new PedaleaCore.DTO.DetallePlanSepare();
            try
            {
                PedaleaCore.DAO.DetallePlanSepare ObjMethod = new PedaleaCore.DAO.DetallePlanSepare();
                ObjReturn = ObjMethod.InsertarDetallePlanSepare(ObjRequest);
            }
            catch (Exception ex)
            {
                _Logger.Debug($"Excepcion en el controlador DetallePlanSepare.Insertar: {ex.Message}");
            }
            _Logger.Debug($"Finaliza el controlador DetallePlanSepare.Insertar:  \n{Newtonsoft.Json.JsonConvert.SerializeObject(ObjReturn, Newtonsoft.Json.Formatting.Indented)}");
            _Logger.Debug($"-------------------------------------------------------------------------------------");
            return ObjReturn;
        }

        [HttpPut("Actualizar/{DetallePlanSepareId}")]
        public PedaleaCore.DTO.DetallePlanSepare Actualizar(long DetallePlanSepareId,PedaleaCore.DTO.DetallePlanSepare ObjRequest)
        {
            _Logger.Debug($"-------------------------------------------------------------------------------------");
            ObjRequest.DetallePlanSepareId = DetallePlanSepareId;
            _Logger.Debug($"Ingreso en el controlador DetallePlanSepare.Actualizar: \n{Newtonsoft.Json.JsonConvert.SerializeObject(ObjRequest, Newtonsoft.Json.Formatting.Indented)}");
            PedaleaCore.DTO.DetallePlanSepare ObjReturn = new PedaleaCore.DTO.DetallePlanSepare();
            try
            {
                PedaleaCore.DAO.DetallePlanSepare ObjMethod = new PedaleaCore.DAO.DetallePlanSepare();
                ObjReturn = ObjMethod.ActualizarDetallePlanSepare(ObjRequest);
            }
            catch (Exception ex)
            {
                _Logger.Debug($"Excepcion en el controlador DetallePlanSepare.Actualizar: {ex.Message}");
            }
            _Logger.Debug($"Finaliza el controlador DetallePlanSepare.Actualizar:  \n{Newtonsoft.Json.JsonConvert.SerializeObject(ObjReturn, Newtonsoft.Json.Formatting.Indented)}");
            _Logger.Debug($"-------------------------------------------------------------------------------------");
            return ObjReturn;
        }

        [HttpDelete("Eliminar/{DetalleDetallePlanSepareId}")]
        public PedaleaCore.DTO.Parametros Eliminar(long DetalleDetallePlanSepareId)
        {
            _Logger.Debug($"-------------------------------------------------------------------------------------");
            _Logger.Debug($"Ingreso en el controlador DetallePlanSepare.Eliminar: {DetalleDetallePlanSepareId}");
            PedaleaCore.DTO.Parametros ObjReturn = new PedaleaCore.DTO.Parametros();
            try
            {
                PedaleaCore.DAO.DetallePlanSepare ObjMethod = new PedaleaCore.DAO.DetallePlanSepare();
                ObjReturn.Eliminado = ObjMethod.EliminarDetallePlanSepare(DetalleDetallePlanSepareId);
            }
            catch (Exception ex)
            {
                _Logger.Debug($"Excepcion en el controlador DetallePlanSepare.Eliminar: {ex.Message}");
            }
            _Logger.Debug($"Finaliza el controlador DetallePlanSepare.Eliminar: {ObjReturn}");
            _Logger.Debug($"-------------------------------------------------------------------------------------");
            return ObjReturn;
        }
    }
}
