﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NLog;

namespace PedaleaApi.Controllers
{
    [Route("Pedalea/[controller]")]
    [ApiController]
    public class EstadosPedidoController : ControllerBase
    {
        public NLog.ILogger _Logger = LogManager.GetCurrentClassLogger();


        [HttpGet("TraerTodo")]
        public List<PedaleaCore.DTO.EstadosPedido> TraerTodo()
        {
            _Logger.Debug($"-------------------------------------------------------------------------------------");
            _Logger.Debug($"Ingreso en el controlador EstadosPedido.TraerTodo");
            List<PedaleaCore.DTO.EstadosPedido> ObjReturn = new List<PedaleaCore.DTO.EstadosPedido>();
            try
            {
                PedaleaCore.DAO.EstadosPedido ObjMethod = new PedaleaCore.DAO.EstadosPedido();
                ObjReturn= ObjMethod.TraerEstadosPedido();
            }
            catch(Exception ex)
            {
                _Logger.Debug($"Excepcion en el metodo EstadosPedido.TraerTodo: {ex.Message}");
            }
            _Logger.Debug($"Finaliza el controlador EstadosPedido.TraerTodo");
            _Logger.Debug($"-------------------------------------------------------------------------------------");
            return ObjReturn;
        }

        [HttpGet("TraerPorId/{EstadoPedidoId}")]
        public PedaleaCore.DTO.EstadosPedido TraerPorId(long EstadoPedidoId)
        {
            _Logger.Debug($"-------------------------------------------------------------------------------------");
            _Logger.Debug($"Ingreso en el controlador EstadosPedido.TraerPorId: {EstadoPedidoId}");
            PedaleaCore.DTO.EstadosPedido ObjReturn = new PedaleaCore.DTO.EstadosPedido();
            try
            {
                PedaleaCore.DAO.EstadosPedido ObjMethod = new PedaleaCore.DAO.EstadosPedido();
                ObjReturn = ObjMethod.TraerEstadosPedidoPorId(EstadoPedidoId);
            }
            catch (Exception ex)
            {
                _Logger.Debug($"Excepcion en el controlador EstadosPedido.TraerPorId: {ex.Message}");
            }
            _Logger.Debug($"Finaliza el controlador EstadosPedido.TraerPorId: {EstadoPedidoId}");
            _Logger.Debug($"-------------------------------------------------------------------------------------");
            return ObjReturn;
        }

        [HttpPost("Insertar")]
        public PedaleaCore.DTO.EstadosPedido Insertar(PedaleaCore.DTO.EstadosPedido ObjRequest)
        {
            _Logger.Debug($"-------------------------------------------------------------------------------------");
            _Logger.Debug($"Ingreso en el controlador EstadosPedido.Insertar: \n{Newtonsoft.Json.JsonConvert.SerializeObject(ObjRequest, Newtonsoft.Json.Formatting.Indented)}");
            PedaleaCore.DTO.EstadosPedido ObjReturn = new PedaleaCore.DTO.EstadosPedido();
            try
            {
                PedaleaCore.DAO.EstadosPedido ObjMethod = new PedaleaCore.DAO.EstadosPedido();
                ObjReturn = ObjMethod.InsertarEstadosPedido(ObjRequest);
            }
            catch (Exception ex)
            {
                _Logger.Debug($"Excepcion en el controlador EstadosPedido.Insertar: {ex.Message}");
            }
            _Logger.Debug($"Finaliza el controlador EstadosPedido.Insertar:  \n{Newtonsoft.Json.JsonConvert.SerializeObject(ObjReturn, Newtonsoft.Json.Formatting.Indented)}");
            _Logger.Debug($"-------------------------------------------------------------------------------------");
            return ObjReturn;
        }

        [HttpPut("Actualizar/{EstadoPedidoId}")]
        public PedaleaCore.DTO.EstadosPedido Actualizar(long EstadoPedidoId, PedaleaCore.DTO.EstadosPedido ObjRequest)
        {
            _Logger.Debug($"-------------------------------------------------------------------------------------");
            ObjRequest.EstadoPedidoId = EstadoPedidoId;
            _Logger.Debug($"Ingreso en el controlador EstadosPedido.Actualizar: \n{Newtonsoft.Json.JsonConvert.SerializeObject(ObjRequest, Newtonsoft.Json.Formatting.Indented)}");
            PedaleaCore.DTO.EstadosPedido ObjReturn = new PedaleaCore.DTO.EstadosPedido();
            try
            {
                PedaleaCore.DAO.EstadosPedido ObjMethod = new PedaleaCore.DAO.EstadosPedido();
                ObjReturn = ObjMethod.ActualizarEstadosPedido(ObjRequest);
            }
            catch (Exception ex)
            {
                _Logger.Debug($"Excepcion en el controlador EstadosPedido.Actualizar: {ex.Message}");
            }
            _Logger.Debug($"Finaliza el controlador EstadosPedido.Actualizar:  \n{Newtonsoft.Json.JsonConvert.SerializeObject(ObjReturn, Newtonsoft.Json.Formatting.Indented)}");
            _Logger.Debug($"-------------------------------------------------------------------------------------");
            return ObjReturn;
        }

        [HttpDelete("Eliminar/{DetalleEstadosPedidoId}")]
        public PedaleaCore.DTO.Parametros Eliminar(long DetalleEstadosPedidoId)
        {
            _Logger.Debug($"-------------------------------------------------------------------------------------");
            _Logger.Debug($"Ingreso en el controlador EstadosPedido.Eliminar: {DetalleEstadosPedidoId}");
            PedaleaCore.DTO.Parametros ObjReturn = new PedaleaCore.DTO.Parametros();
            try
            {
                PedaleaCore.DAO.EstadosPedido ObjMethod = new PedaleaCore.DAO.EstadosPedido();
                ObjReturn.Eliminado = ObjMethod.EliminarEstadosPedido(DetalleEstadosPedidoId);
            }
            catch (Exception ex)
            {
                _Logger.Debug($"Excepcion en el controlador EstadosPedido.Eliminar: {ex.Message}");
            }
            _Logger.Debug($"Finaliza el controlador EstadosPedido.Eliminar: {ObjReturn}");
            _Logger.Debug($"-------------------------------------------------------------------------------------");
            return ObjReturn;
        }
    }
}
