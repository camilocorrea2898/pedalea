﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NLog;

namespace PedaleaApi.Controllers
{
    [Route("Pedalea/[controller]")]
    [ApiController]
    public class XrefPromocionesDepartamentosController : ControllerBase
    {
        public NLog.ILogger _Logger = LogManager.GetCurrentClassLogger();


        [HttpGet("TraerTodo")]
        public List<PedaleaCore.DTO.XrefPromocionesDepartamentos> TraerTodo()
        {
            _Logger.Debug($"-------------------------------------------------------------------------------------");
            _Logger.Debug($"Ingreso en el controlador XrefPromocionesDepartamentos.TraerTodo");
            List<PedaleaCore.DTO.XrefPromocionesDepartamentos> ObjReturn = new List<PedaleaCore.DTO.XrefPromocionesDepartamentos>();
            try
            {
                PedaleaCore.DAO.XrefPromocionesDepartamentos ObjMethod = new PedaleaCore.DAO.XrefPromocionesDepartamentos();
                ObjReturn= ObjMethod.TraerXrefPromocionesDepartamentos();
            }
            catch(Exception ex)
            {
                _Logger.Debug($"Excepcion en el metodo XrefPromocionesDepartamentos.TraerTodo: {ex.Message}");
            }
            _Logger.Debug($"Finaliza el controlador XrefPromocionesDepartamentos.TraerTodo");
            _Logger.Debug($"-------------------------------------------------------------------------------------");
            return ObjReturn;
        }

        [HttpGet("TraerPorPromocionId/{PromocionId}")]
        public List<PedaleaCore.DTO.XrefPromocionesDepartamentos> TraerPorPromocionId(long PromocionId)
        {
            _Logger.Debug($"-------------------------------------------------------------------------------------");
            _Logger.Debug($"Ingreso en el controlador XrefPromocionesDepartamentos.TraerPorPromocionId: {PromocionId}");
            List<PedaleaCore.DTO.XrefPromocionesDepartamentos> ObjReturn = new List<PedaleaCore.DTO.XrefPromocionesDepartamentos>();
            try
            {
                PedaleaCore.DAO.XrefPromocionesDepartamentos ObjMethod = new PedaleaCore.DAO.XrefPromocionesDepartamentos();
                ObjReturn = ObjMethod.TraerXrefPromocionesDepartamentosPorPromocionId(PromocionId);
            }
            catch (Exception ex)
            {
                _Logger.Debug($"Excepcion en el controlador XrefPromocionesDepartamentos.TraerPorPromocionId: {ex.Message}");
            }
            _Logger.Debug($"Finaliza el controlador XrefPromocionesDepartamentos.TraerPorPromocionId: {PromocionId}");
            _Logger.Debug($"-------------------------------------------------------------------------------------");
            return ObjReturn;
        }

        [HttpGet("TraerPorDepartamentoId/{DepartamentoVentaId}")]
        public List<PedaleaCore.DTO.XrefPromocionesDepartamentos> TraerPorDepartamentoId(long DepartamentoVentaId)
        {
            _Logger.Debug($"-------------------------------------------------------------------------------------");
            _Logger.Debug($"Ingreso en el controlador XrefPromocionesDepartamentos.TraerPorDepartamentoId: {DepartamentoVentaId}");
            List<PedaleaCore.DTO.XrefPromocionesDepartamentos> ObjReturn = new List<PedaleaCore.DTO.XrefPromocionesDepartamentos>();
            try
            {
                PedaleaCore.DAO.XrefPromocionesDepartamentos ObjMethod = new PedaleaCore.DAO.XrefPromocionesDepartamentos();
                ObjReturn = ObjMethod.TraerXrefPromocionesDepartamentosPorDepartamentoId(DepartamentoVentaId);
            }
            catch (Exception ex)
            {
                _Logger.Debug($"Excepcion en el controlador XrefPromocionesDepartamentos.TraerPorDepartamentoId: {ex.Message}");
            }
            _Logger.Debug($"Finaliza el controlador XrefPromocionesDepartamentos.TraerPorDepartamentoId: {DepartamentoVentaId}");
            _Logger.Debug($"-------------------------------------------------------------------------------------");
            return ObjReturn;
        }


        [HttpPost("Insertar")]
        public PedaleaCore.DTO.XrefPromocionesDepartamentos Insertar(PedaleaCore.DTO.XrefPromocionesDepartamentos ObjRequest)
        {
            _Logger.Debug($"-------------------------------------------------------------------------------------");
            _Logger.Debug($"Ingreso en el controlador XrefPromocionesDepartamentos.Insertar: \n{Newtonsoft.Json.JsonConvert.SerializeObject(ObjRequest, Newtonsoft.Json.Formatting.Indented)}");
            PedaleaCore.DTO.XrefPromocionesDepartamentos ObjReturn = new PedaleaCore.DTO.XrefPromocionesDepartamentos();
            try
            {
                PedaleaCore.DAO.XrefPromocionesDepartamentos ObjMethod = new PedaleaCore.DAO.XrefPromocionesDepartamentos();
                ObjReturn = ObjMethod.InsertarXrefPromocionesDepartamentos(ObjRequest);
            }
            catch (Exception ex)
            {
                _Logger.Debug($"Excepcion en el controlador XrefPromocionesDepartamentos.Insertar: {ex.Message}");
            }
            _Logger.Debug($"Finaliza el controlador XrefPromocionesDepartamentos.Insertar:  \n{Newtonsoft.Json.JsonConvert.SerializeObject(ObjReturn, Newtonsoft.Json.Formatting.Indented)}");
            _Logger.Debug($"-------------------------------------------------------------------------------------");
            return ObjReturn;
        }

        [HttpPut("Actualizar/{PromocionId}/{DepartamentoVentaId}")]
        public PedaleaCore.DTO.XrefPromocionesDepartamentos Actualizar(long PromocionId,int DepartamentoVentaId, PedaleaCore.DTO.XrefPromocionesDepartamentos ObjRequestNew)
        {
            _Logger.Debug($"-------------------------------------------------------------------------------------");
            
            PedaleaCore.DTO.XrefPromocionesDepartamentos ObjReturn = new PedaleaCore.DTO.XrefPromocionesDepartamentos();
            try
            {
                PedaleaCore.DTO.XrefPromocionesDepartamentosUpdate ObjRequest = new PedaleaCore.DTO.XrefPromocionesDepartamentosUpdate();
                PedaleaCore.DTO.XrefPromocionesDepartamentos ObjRequestLast = new PedaleaCore.DTO.XrefPromocionesDepartamentos();
                ObjRequestLast.PromocionId = PromocionId;
                ObjRequestLast.DepartamentoVentaId = DepartamentoVentaId;

                ObjRequest.XrefPromocionesDepartamentosLast = ObjRequestLast;
                ObjRequest.XrefPromocionesDepartamentosNew = ObjRequestNew;
                _Logger.Debug($"Ingreso en el controlador XrefPromocionesDepartamentos.Actualizar: \n{Newtonsoft.Json.JsonConvert.SerializeObject(ObjRequest, Newtonsoft.Json.Formatting.Indented)}");

                PedaleaCore.DAO.XrefPromocionesDepartamentos ObjMethod = new PedaleaCore.DAO.XrefPromocionesDepartamentos();
                ObjReturn = ObjMethod.ActualizarXrefPromocionesDepartamentos(ObjRequest);
            }
            catch (Exception ex)
            {
                _Logger.Debug($"Excepcion en el controlador XrefPromocionesDepartamentos.Actualizar: {ex.Message}");
            }
            _Logger.Debug($"Finaliza el controlador XrefPromocionesDepartamentos.Actualizar:  \n{Newtonsoft.Json.JsonConvert.SerializeObject(ObjReturn, Newtonsoft.Json.Formatting.Indented)}");
            _Logger.Debug($"-------------------------------------------------------------------------------------");
            return ObjReturn;
        }

        [HttpDelete("Eliminar/{DetallePromocionId}")]
        public PedaleaCore.DTO.Parametros Eliminar(long DetallePromocionId)
        {
            _Logger.Debug($"-------------------------------------------------------------------------------------");
            _Logger.Debug($"Ingreso en el controlador XrefPromocionesDepartamentos.Eliminar: {DetallePromocionId}");
            PedaleaCore.DTO.Parametros ObjReturn = new PedaleaCore.DTO.Parametros();
            try
            {
                PedaleaCore.DAO.XrefPromocionesDepartamentos ObjMethod = new PedaleaCore.DAO.XrefPromocionesDepartamentos();
                ObjReturn.Eliminado = ObjMethod.EliminarXrefPromocionesDepartamentos(DetallePromocionId);
            }
            catch (Exception ex)
            {
                _Logger.Debug($"Excepcion en el controlador XrefPromocionesDepartamentos.Eliminar: {ex.Message}");
            }
            _Logger.Debug($"Finaliza el controlador XrefPromocionesDepartamentos.Eliminar: {ObjReturn}");
            _Logger.Debug($"-------------------------------------------------------------------------------------");
            return ObjReturn;
        }
    }
}
