﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NLog;

namespace PedaleaApi.Controllers
{
    [Route("Pedalea/[controller]")]
    [ApiController]
    public class PlanSepareController : ControllerBase
    {
        public NLog.ILogger _Logger = LogManager.GetCurrentClassLogger();


        [HttpGet("TraerTodo")]
        public List<PedaleaCore.DTO.PlanSepare> TraerTodo()
        {
            _Logger.Debug($"-------------------------------------------------------------------------------------");
            _Logger.Debug($"Ingreso en el controlador PlanSepare.TraerTodo");
            List<PedaleaCore.DTO.PlanSepare> ObjReturn = new List<PedaleaCore.DTO.PlanSepare>();
            try
            {
                PedaleaCore.DAO.PlanSepare ObjMethod = new PedaleaCore.DAO.PlanSepare();
                ObjReturn= ObjMethod.TraerPlanSepare();
            }
            catch(Exception ex)
            {
                _Logger.Debug($"Excepcion en el metodo PlanSepare.TraerTodo: {ex.Message}");
            }
            _Logger.Debug($"Finaliza el controlador PlanSepare.TraerTodo");
            _Logger.Debug($"-------------------------------------------------------------------------------------");
            return ObjReturn;
        }

        [HttpGet("TraerPorId/{PlanSepareId}")]
        public PedaleaCore.DTO.PlanSepare TraerPorId(long PlanSepareId)
        {
            _Logger.Debug($"-------------------------------------------------------------------------------------");
            _Logger.Debug($"Ingreso en el controlador PlanSepare.TraerPorId: {PlanSepareId}");
            PedaleaCore.DTO.PlanSepare ObjReturn = new PedaleaCore.DTO.PlanSepare();
            try
            {
                PedaleaCore.DAO.PlanSepare ObjMethod = new PedaleaCore.DAO.PlanSepare();
                ObjReturn = ObjMethod.TraerPlanSeparePorId(PlanSepareId);
            }
            catch (Exception ex)
            {
                _Logger.Debug($"Excepcion en el controlador PlanSepare.TraerPorId: {ex.Message}");
            }
            _Logger.Debug($"Finaliza el controlador PlanSepare.TraerPorId: {PlanSepareId}");
            _Logger.Debug($"-------------------------------------------------------------------------------------");
            return ObjReturn;
        }

        [HttpGet("TraerPorPedidoId/{PedidoId}")]
        public PedaleaCore.DTO.PlanSepare TraerPorPedidoId(long PedidoId)
        {
            _Logger.Debug($"-------------------------------------------------------------------------------------");
            _Logger.Debug($"Ingreso en el controlador PlanSepare.TraerPorPedidoId: {PedidoId}");
            PedaleaCore.DTO.PlanSepare ObjReturn = new PedaleaCore.DTO.PlanSepare();
            try
            {
                PedaleaCore.DAO.PlanSepare ObjMethod = new PedaleaCore.DAO.PlanSepare();
                ObjReturn = ObjMethod.TraerPlanSeparePorPedidoId(PedidoId);
            }
            catch (Exception ex)
            {
                _Logger.Debug($"Excepcion en el controlador PlanSepare.TraerPorPedidoId: {ex.Message}");
            }
            _Logger.Debug($"Finaliza el controlador PlanSepare.TraerPorPedidoId: {PedidoId}");
            _Logger.Debug($"-------------------------------------------------------------------------------------");
            return ObjReturn;
        }

        [HttpPost("Insertar")]
        public PedaleaCore.DTO.PlanSepare Insertar(PedaleaCore.DTO.PlanSepare ObjRequest)
        {
            _Logger.Debug($"-------------------------------------------------------------------------------------");
            _Logger.Debug($"Ingreso en el controlador PlanSepare.Insertar: \n{Newtonsoft.Json.JsonConvert.SerializeObject(ObjRequest, Newtonsoft.Json.Formatting.Indented)}");
            PedaleaCore.DTO.PlanSepare ObjReturn = new PedaleaCore.DTO.PlanSepare();
            try
            {
                PedaleaCore.DAO.PlanSepare ObjMethod = new PedaleaCore.DAO.PlanSepare();
                ObjReturn = ObjMethod.InsertarPlanSepare(ObjRequest);
            }
            catch (Exception ex)
            {
                _Logger.Debug($"Excepcion en el controlador PlanSepare.Insertar: {ex.Message}");
            }
            _Logger.Debug($"Finaliza el controlador PlanSepare.Insertar:  \n{Newtonsoft.Json.JsonConvert.SerializeObject(ObjReturn, Newtonsoft.Json.Formatting.Indented)}");
            _Logger.Debug($"-------------------------------------------------------------------------------------");
            return ObjReturn;
        }

        [HttpPut("Actualizar/{PlanSepareId}")]
        public PedaleaCore.DTO.PlanSepare Actualizar(long PlanSepareId, PedaleaCore.DTO.PlanSepare ObjRequest)
        {
            _Logger.Debug($"-------------------------------------------------------------------------------------");
            ObjRequest.PlanSepareId = PlanSepareId;
            _Logger.Debug($"Ingreso en el controlador PlanSepare.Actualizar: \n{Newtonsoft.Json.JsonConvert.SerializeObject(ObjRequest, Newtonsoft.Json.Formatting.Indented)}");
            PedaleaCore.DTO.PlanSepare ObjReturn = new PedaleaCore.DTO.PlanSepare();
            try
            {
                PedaleaCore.DAO.PlanSepare ObjMethod = new PedaleaCore.DAO.PlanSepare();
                ObjReturn = ObjMethod.ActualizarPlanSepare(ObjRequest);
            }
            catch (Exception ex)
            {
                _Logger.Debug($"Excepcion en el controlador PlanSepare.Actualizar: {ex.Message}");
            }
            _Logger.Debug($"Finaliza el controlador PlanSepare.Actualizar:  \n{Newtonsoft.Json.JsonConvert.SerializeObject(ObjReturn, Newtonsoft.Json.Formatting.Indented)}");
            _Logger.Debug($"-------------------------------------------------------------------------------------");
            return ObjReturn;
        }

        [HttpDelete("Eliminar/{DetallePlanSepareId}")]
        public PedaleaCore.DTO.Parametros Eliminar(long DetallePlanSepareId)
        {
            _Logger.Debug($"-------------------------------------------------------------------------------------");
            _Logger.Debug($"Ingreso en el controlador PlanSepare.Eliminar: {DetallePlanSepareId}");
            PedaleaCore.DTO.Parametros ObjReturn = new PedaleaCore.DTO.Parametros();
            try
            {
                PedaleaCore.DAO.PlanSepare ObjMethod = new PedaleaCore.DAO.PlanSepare();
                ObjReturn.Eliminado = ObjMethod.EliminarPlanSepare(DetallePlanSepareId);
            }
            catch (Exception ex)
            {
                _Logger.Debug($"Excepcion en el controlador PlanSepare.Eliminar: {ex.Message}");
            }
            _Logger.Debug($"Finaliza el controlador PlanSepare.Eliminar: {ObjReturn}");
            _Logger.Debug($"-------------------------------------------------------------------------------------");
            return ObjReturn;
        }
    }
}
