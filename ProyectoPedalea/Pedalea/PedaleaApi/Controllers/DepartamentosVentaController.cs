﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NLog;

namespace PedaleaApi.Controllers
{
    [Route("Pedalea/[controller]")]
    [ApiController]
    public class DepartamentosVentaController : ControllerBase
    {
        public NLog.ILogger _Logger = LogManager.GetCurrentClassLogger();


        [HttpGet("TraerTodo")]
        public List<PedaleaCore.DTO.DepartamentosVenta> TraerTodo()
        {
            _Logger.Debug($"-------------------------------------------------------------------------------------");
            _Logger.Debug($"Ingreso en el controlador DepartamentosVenta.TraerTodo");
            List<PedaleaCore.DTO.DepartamentosVenta> ObjReturn = new List<PedaleaCore.DTO.DepartamentosVenta>();
            try
            {
                PedaleaCore.DAO.DepartamentosVenta ObjMethod = new PedaleaCore.DAO.DepartamentosVenta();
                ObjReturn= ObjMethod.TraerDepartamentosVenta();
            }
            catch(Exception ex)
            {
                _Logger.Debug($"Excepcion en el metodo DepartamentosVenta.TraerTodo: {ex.Message}");
            }
            _Logger.Debug($"Finaliza el controlador DepartamentosVenta.TraerTodo");
            _Logger.Debug($"-------------------------------------------------------------------------------------");
            return ObjReturn;
        }

        [HttpGet("TraerPorId/{DepartamentoVentaId}")]
        public PedaleaCore.DTO.DepartamentosVenta TraerPorId(long DepartamentoVentaId)
        {
            _Logger.Debug($"-------------------------------------------------------------------------------------");
            _Logger.Debug($"Ingreso en el controlador DepartamentosVenta.TraerPorId: {DepartamentoVentaId}");
            PedaleaCore.DTO.DepartamentosVenta ObjReturn = new PedaleaCore.DTO.DepartamentosVenta();
            try
            {
                PedaleaCore.DAO.DepartamentosVenta ObjMethod = new PedaleaCore.DAO.DepartamentosVenta();
                ObjReturn = ObjMethod.TraerDepartamentoVentaPorId(DepartamentoVentaId);
            }
            catch (Exception ex)
            {
                _Logger.Debug($"Excepcion en el controlador DepartamentosVenta.TraerPorId: {ex.Message}");
            }
            _Logger.Debug($"Finaliza el controlador DepartamentosVenta.TraerPorId: {DepartamentoVentaId}");
            _Logger.Debug($"-------------------------------------------------------------------------------------");
            return ObjReturn;
        }

        [HttpPost("Insertar")]
        public PedaleaCore.DTO.DepartamentosVenta Insertar(PedaleaCore.DTO.DepartamentosVenta ObjRequest)
        {
            _Logger.Debug($"-------------------------------------------------------------------------------------");
            _Logger.Debug($"Ingreso en el controlador DepartamentosVenta.Insertar: \n{Newtonsoft.Json.JsonConvert.SerializeObject(ObjRequest, Newtonsoft.Json.Formatting.Indented)}");
            PedaleaCore.DTO.DepartamentosVenta ObjReturn = new PedaleaCore.DTO.DepartamentosVenta();
            try
            {
                PedaleaCore.DAO.DepartamentosVenta ObjMethod = new PedaleaCore.DAO.DepartamentosVenta();
                ObjReturn = ObjMethod.InsertarDepartamentoVenta(ObjRequest);
            }
            catch (Exception ex)
            {
                _Logger.Debug($"Excepcion en el controlador DepartamentosVenta.Insertar: {ex.Message}");
            }
            _Logger.Debug($"Finaliza el controlador DepartamentosVenta.Insertar:  \n{Newtonsoft.Json.JsonConvert.SerializeObject(ObjReturn, Newtonsoft.Json.Formatting.Indented)}");
            _Logger.Debug($"-------------------------------------------------------------------------------------");
            return ObjReturn;
        }

        [HttpPut("Actualizar/{DepartamentoVentaId}")]
        public PedaleaCore.DTO.DepartamentosVenta Actualizar(long DepartamentoVentaId,PedaleaCore.DTO.DepartamentosVenta ObjRequest)
        {
            _Logger.Debug($"-------------------------------------------------------------------------------------");
            ObjRequest.DepartamentoVentaId = DepartamentoVentaId;
            _Logger.Debug($"Ingreso en el controlador DepartamentosVenta.Actualizar: \n{Newtonsoft.Json.JsonConvert.SerializeObject(ObjRequest, Newtonsoft.Json.Formatting.Indented)}");
            PedaleaCore.DTO.DepartamentosVenta ObjReturn = new PedaleaCore.DTO.DepartamentosVenta();
            try
            {
                PedaleaCore.DAO.DepartamentosVenta ObjMethod = new PedaleaCore.DAO.DepartamentosVenta();
                ObjReturn = ObjMethod.ActualizarDepartamentoVenta(ObjRequest);
            }
            catch (Exception ex)
            {
                _Logger.Debug($"Excepcion en el controlador DepartamentosVenta.Actualizar: {ex.Message}");
            }
            _Logger.Debug($"Finaliza el controlador DepartamentosVenta.Actualizar:  \n{Newtonsoft.Json.JsonConvert.SerializeObject(ObjReturn, Newtonsoft.Json.Formatting.Indented)}");
            _Logger.Debug($"-------------------------------------------------------------------------------------");
            return ObjReturn;
        }

        [HttpDelete("Eliminar/{DepartamentoVentaId}")]
        public PedaleaCore.DTO.Parametros Eliminar(long DepartamentoVentaId)
        {
            _Logger.Debug($"-------------------------------------------------------------------------------------");
            _Logger.Debug($"Ingreso en el controlador DepartamentosVenta.Eliminar: {DepartamentoVentaId}");
            PedaleaCore.DTO.Parametros ObjReturn = new PedaleaCore.DTO.Parametros();
            try
            {
                PedaleaCore.DAO.DepartamentosVenta ObjMethod = new PedaleaCore.DAO.DepartamentosVenta();
                ObjReturn.Eliminado = ObjMethod.EliminarDepartamentoVenta(DepartamentoVentaId);
            }
            catch (Exception ex)
            {
                _Logger.Debug($"Excepcion en el controlador DepartamentosVenta.Eliminar: {ex.Message}");
            }
            _Logger.Debug($"Finaliza el controlador DepartamentosVenta.Eliminar: {ObjReturn}");
            _Logger.Debug($"-------------------------------------------------------------------------------------");
            return ObjReturn;
        }
    }
}
