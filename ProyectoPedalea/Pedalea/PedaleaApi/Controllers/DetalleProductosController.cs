﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NLog;

namespace PedaleaApi.Controllers
{
    [Route("Pedalea/[controller]")]
    [ApiController]
    public class DetalleProductosController : ControllerBase
    {
        public NLog.ILogger _Logger = LogManager.GetCurrentClassLogger();


        [HttpGet("TraerTodo")]
        public List<PedaleaCore.DTO.DetalleProductos> TraerTodo()
        {
            _Logger.Debug($"-------------------------------------------------------------------------------------");
            _Logger.Debug($"Ingreso en el controlador DetalleProductos.TraerTodo");
            List<PedaleaCore.DTO.DetalleProductos> ObjReturn = new List<PedaleaCore.DTO.DetalleProductos>();
            try
            {
                PedaleaCore.DAO.DetalleProductos ObjMethod = new PedaleaCore.DAO.DetalleProductos();
                ObjReturn= ObjMethod.TraerDetalleProductos();
            }
            catch(Exception ex)
            {
                _Logger.Debug($"Excepcion en el metodo DetalleProductos.TraerTodo: {ex.Message}");
            }
            _Logger.Debug($"Finaliza el controlador DetalleProductos.TraerTodo");
            _Logger.Debug($"-------------------------------------------------------------------------------------");
            return ObjReturn;
        }

        [HttpGet("TraerPorId/{DetalleProductoId}")]
        public PedaleaCore.DTO.DetalleProductos TraerPorId(long DetalleProductoId)
        {
            _Logger.Debug($"-------------------------------------------------------------------------------------");
            _Logger.Debug($"Ingreso en el controlador DetalleProductos.TraerPorId: {DetalleProductoId}");
            PedaleaCore.DTO.DetalleProductos ObjReturn = new PedaleaCore.DTO.DetalleProductos();
            try
            {
                PedaleaCore.DAO.DetalleProductos ObjMethod = new PedaleaCore.DAO.DetalleProductos();
                ObjReturn = ObjMethod.TraerDetalleProductosPorId(DetalleProductoId);
            }
            catch (Exception ex)
            {
                _Logger.Debug($"Excepcion en el controlador DetalleProductos.TraerPorId: {ex.Message}");
            }
            _Logger.Debug($"Finaliza el controlador DetalleProductos.TraerPorId: {DetalleProductoId}");
            _Logger.Debug($"-------------------------------------------------------------------------------------");
            return ObjReturn;
        }

        [HttpGet("TraerPorProductoId/{ProductoId}")]
        public List<PedaleaCore.DTO.DetalleProductos> TraerPorProductoId(long ProductoId)
        {
            _Logger.Debug($"-------------------------------------------------------------------------------------");
            _Logger.Debug($"Ingreso en el controlador DetalleProductos.TraerPorProductoId: {ProductoId}");
            List<PedaleaCore.DTO.DetalleProductos> ObjReturn = new List<PedaleaCore.DTO.DetalleProductos>();
            try
            {
                PedaleaCore.DAO.DetalleProductos ObjMethod = new PedaleaCore.DAO.DetalleProductos();
                ObjReturn = ObjMethod.TraerDetalleProductosPorProductoId(ProductoId);
            }
            catch (Exception ex)
            {
                _Logger.Debug($"Excepcion en el controlador DetalleProductos.TraerPorProductoId: {ex.Message}");
            }
            _Logger.Debug($"Finaliza el controlador DetalleProductos.TraerPorProductoId: {ProductoId}");
            _Logger.Debug($"-------------------------------------------------------------------------------------");
            return ObjReturn;
        }

        [HttpPost("Insertar")]
        public PedaleaCore.DTO.DetalleProductos Insertar(PedaleaCore.DTO.DetalleProductos ObjRequest)
        {
            _Logger.Debug($"-------------------------------------------------------------------------------------");
            _Logger.Debug($"Ingreso en el controlador DetalleProductos.Insertar: \n{Newtonsoft.Json.JsonConvert.SerializeObject(ObjRequest, Newtonsoft.Json.Formatting.Indented)}");
            PedaleaCore.DTO.DetalleProductos ObjReturn = new PedaleaCore.DTO.DetalleProductos();
            try
            {
                PedaleaCore.DAO.DetalleProductos ObjMethod = new PedaleaCore.DAO.DetalleProductos();
                ObjReturn = ObjMethod.InsertarDetalleProducto(ObjRequest);
            }
            catch (Exception ex)
            {
                _Logger.Debug($"Excepcion en el controlador DetalleProductos.Insertar: {ex.Message}");
            }
            _Logger.Debug($"Finaliza el controlador DetalleProductos.Insertar:  \n{Newtonsoft.Json.JsonConvert.SerializeObject(ObjReturn, Newtonsoft.Json.Formatting.Indented)}");
            _Logger.Debug($"-------------------------------------------------------------------------------------");
            return ObjReturn;
        }

        [HttpPut("Actualizar/{DetalleProductoId}")]
        public PedaleaCore.DTO.DetalleProductos Actualizar(long DetalleProductoId, PedaleaCore.DTO.DetalleProductos ObjRequest)
        {
            _Logger.Debug($"-------------------------------------------------------------------------------------");
            ObjRequest.DetalleProductoId = DetalleProductoId;
            _Logger.Debug($"Ingreso en el controlador DetalleProductos.Actualizar: \n{Newtonsoft.Json.JsonConvert.SerializeObject(ObjRequest, Newtonsoft.Json.Formatting.Indented)}");
            PedaleaCore.DTO.DetalleProductos ObjReturn = new PedaleaCore.DTO.DetalleProductos();
            try
            {
                PedaleaCore.DAO.DetalleProductos ObjMethod = new PedaleaCore.DAO.DetalleProductos();
                ObjReturn = ObjMethod.ActualizarDetalleProductos(ObjRequest);
            }
            catch (Exception ex)
            {
                _Logger.Debug($"Excepcion en el controlador DetalleProductos.Actualizar: {ex.Message}");
            }
            _Logger.Debug($"Finaliza el controlador DetalleProductos.Actualizar:  \n{Newtonsoft.Json.JsonConvert.SerializeObject(ObjReturn, Newtonsoft.Json.Formatting.Indented)}");
            _Logger.Debug($"-------------------------------------------------------------------------------------");
            return ObjReturn;
        }

        [HttpDelete("Eliminar/{DetalleDetalleProductosId}")]
        public PedaleaCore.DTO.Parametros Eliminar(long DetalleDetalleProductosId)
        {
            _Logger.Debug($"-------------------------------------------------------------------------------------");
            _Logger.Debug($"Ingreso en el controlador DetalleProductos.Eliminar: {DetalleDetalleProductosId}");
            PedaleaCore.DTO.Parametros ObjReturn = new PedaleaCore.DTO.Parametros();
            try
            {
                PedaleaCore.DAO.DetalleProductos ObjMethod = new PedaleaCore.DAO.DetalleProductos();
                ObjReturn.Eliminado = ObjMethod.EliminarDetalleProductos(DetalleDetalleProductosId);
            }
            catch (Exception ex)
            {
                _Logger.Debug($"Excepcion en el controlador DetalleProductos.Eliminar: {ex.Message}");
            }
            _Logger.Debug($"Finaliza el controlador DetalleProductos.Eliminar: {ObjReturn}");
            _Logger.Debug($"-------------------------------------------------------------------------------------");
            return ObjReturn;
        }
    }
}
