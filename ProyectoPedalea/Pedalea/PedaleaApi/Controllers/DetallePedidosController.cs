﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NLog;

namespace PedaleaApi.Controllers
{
    [Route("Pedalea/[controller]")]
    [ApiController]
    public class DetallePedidosController : ControllerBase
    {
        public NLog.ILogger _Logger = LogManager.GetCurrentClassLogger();


        [HttpGet("TraerTodo")]
        public List<PedaleaCore.DTO.DetallePedidos> TraerTodo()
        {
            _Logger.Debug($"-------------------------------------------------------------------------------------");
            _Logger.Debug($"Ingreso en el controlador DetallePedidos.TraerTodo");
            List<PedaleaCore.DTO.DetallePedidos> ObjReturn = new List<PedaleaCore.DTO.DetallePedidos>();
            try
            {
                PedaleaCore.DAO.DetallePedidos ObjMethod = new PedaleaCore.DAO.DetallePedidos();
                ObjReturn= ObjMethod.TraerDetallePedidos();
            }
            catch(Exception ex)
            {
                _Logger.Debug($"Excepcion en el metodo DetallePedidos.TraerTodo: {ex.Message}");
            }
            _Logger.Debug($"Finaliza el controlador DetallePedidos.TraerTodo");
            _Logger.Debug($"-------------------------------------------------------------------------------------");
            return ObjReturn;
        }

        [HttpGet("TraerPorId/{DetallePedidoId}")]
        public PedaleaCore.DTO.DetallePedidos TraerPorId(long DetallePedidoId)
        {
            _Logger.Debug($"-------------------------------------------------------------------------------------");
            _Logger.Debug($"Ingreso en el controlador DetallePedidos.TraerPorId: {DetallePedidoId}");
            PedaleaCore.DTO.DetallePedidos ObjReturn = new PedaleaCore.DTO.DetallePedidos();
            try
            {
                PedaleaCore.DAO.DetallePedidos ObjMethod = new PedaleaCore.DAO.DetallePedidos();
                ObjReturn = ObjMethod.TraerDetallePedidoPorId(DetallePedidoId);
            }
            catch (Exception ex)
            {
                _Logger.Debug($"Excepcion en el controlador DetallePedidos.TraerPorId: {ex.Message}");
            }
            _Logger.Debug($"Finaliza el controlador DetallePedidos.TraerPorId: {DetallePedidoId}");
            _Logger.Debug($"-------------------------------------------------------------------------------------");
            return ObjReturn;
        }

        [HttpGet("TraerPorPedidoId/{PedidoId}")]
        public List<PedaleaCore.DTO.DetallePedidos> TraerPorPedidoId(long PedidoId)
        {
            _Logger.Debug($"-------------------------------------------------------------------------------------");
            _Logger.Debug($"Ingreso en el controlador DetallePedidos.TraerPorPedidoId: {PedidoId}");
            List<PedaleaCore.DTO.DetallePedidos> ObjReturn = new List<PedaleaCore.DTO.DetallePedidos>();
            try
            {
                PedaleaCore.DAO.DetallePedidos ObjMethod = new PedaleaCore.DAO.DetallePedidos();
                ObjReturn = ObjMethod.TraerDetallePedidoPorPedidoId(PedidoId);
            }
            catch (Exception ex)
            {
                _Logger.Debug($"Excepcion en el controlador DetallePedidos.TraerPorPedidoId: {ex.Message}");
            }
            _Logger.Debug($"Finaliza el controlador DetallePedidos.TraerPorPedidoId: {PedidoId}");
            _Logger.Debug($"-------------------------------------------------------------------------------------");
            return ObjReturn;
        }

        [HttpPost("Insertar")]
        public PedaleaCore.DTO.DetallePedidos Insertar(PedaleaCore.DTO.DetallePedidos ObjRequest)
        {
            _Logger.Debug($"-------------------------------------------------------------------------------------");
            _Logger.Debug($"Ingreso en el controlador DetallePedidos.Insertar: \n{Newtonsoft.Json.JsonConvert.SerializeObject(ObjRequest, Newtonsoft.Json.Formatting.Indented)}");
            PedaleaCore.DTO.DetallePedidos ObjReturn = new PedaleaCore.DTO.DetallePedidos();
            try
            {
                PedaleaCore.DAO.DetallePedidos ObjMethod = new PedaleaCore.DAO.DetallePedidos();
                ObjReturn = ObjMethod.InsertarDetallePedido(ObjRequest);
            }
            catch (Exception ex)
            {
                _Logger.Debug($"Excepcion en el controlador DetallePedidos.Insertar: {ex.Message}");
            }
            _Logger.Debug($"Finaliza el controlador DetallePedidos.Insertar:  \n{Newtonsoft.Json.JsonConvert.SerializeObject(ObjReturn, Newtonsoft.Json.Formatting.Indented)}");
            _Logger.Debug($"-------------------------------------------------------------------------------------");
            return ObjReturn;
        }

        [HttpPut("Actualizar/{DetallePedidoId}")]
        public PedaleaCore.DTO.DetallePedidos Actualizar(long DetallePedidoId,PedaleaCore.DTO.DetallePedidos ObjRequest)
        {
            _Logger.Debug($"-------------------------------------------------------------------------------------");
            ObjRequest.DetallePedidoId = DetallePedidoId;
            _Logger.Debug($"Ingreso en el controlador DetallePedidos.Actualizar: \n{Newtonsoft.Json.JsonConvert.SerializeObject(ObjRequest, Newtonsoft.Json.Formatting.Indented)}");
            PedaleaCore.DTO.DetallePedidos ObjReturn = new PedaleaCore.DTO.DetallePedidos();
            try
            {
                PedaleaCore.DAO.DetallePedidos ObjMethod = new PedaleaCore.DAO.DetallePedidos();
                ObjReturn = ObjMethod.ActualizarDetallePedido(ObjRequest);
            }
            catch (Exception ex)
            {
                _Logger.Debug($"Excepcion en el controlador DetallePedidos.Actualizar: {ex.Message}");
            }
            _Logger.Debug($"Finaliza el controlador DetallePedidos.Actualizar:  \n{Newtonsoft.Json.JsonConvert.SerializeObject(ObjReturn, Newtonsoft.Json.Formatting.Indented)}");
            _Logger.Debug($"-------------------------------------------------------------------------------------");
            return ObjReturn;
        }

        [HttpDelete("Eliminar/{DetallePedidoId}")]
        public PedaleaCore.DTO.Parametros Eliminar(long DetallePedidoId)
        {
            _Logger.Debug($"-------------------------------------------------------------------------------------");
            _Logger.Debug($"Ingreso en el controlador DetallePedidos.Eliminar: {DetallePedidoId}");
            PedaleaCore.DTO.Parametros ObjReturn = new PedaleaCore.DTO.Parametros();
            try
            {
                PedaleaCore.DAO.DetallePedidos ObjMethod = new PedaleaCore.DAO.DetallePedidos();
                ObjReturn.Eliminado = ObjMethod.EliminarDetallePedido(DetallePedidoId);
            }
            catch (Exception ex)
            {
                _Logger.Debug($"Excepcion en el controlador DetallePedidos.Eliminar: {ex.Message}");
            }
            _Logger.Debug($"Finaliza el controlador DetallePedidos.Eliminar: {ObjReturn}");
            _Logger.Debug($"-------------------------------------------------------------------------------------");
            return ObjReturn;
        }
    }
}
