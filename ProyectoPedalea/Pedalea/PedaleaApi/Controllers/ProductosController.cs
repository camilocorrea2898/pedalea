﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NLog;

namespace PedaleaApi.Controllers
{
    [Route("Pedalea/[controller]")]
    [ApiController]
    public class ProductosController : ControllerBase
    {
        public NLog.ILogger _Logger = LogManager.GetCurrentClassLogger();


        [HttpGet("TraerTodo")]
        public List<PedaleaCore.DTO.Productos> TraerTodo()
        {
            _Logger.Debug($"-------------------------------------------------------------------------------------");
            _Logger.Debug($"Ingreso en el controlador Productos.TraerTodo");
            List<PedaleaCore.DTO.Productos> ObjReturn = new List<PedaleaCore.DTO.Productos>();
            try
            {
                PedaleaCore.DAO.Productos ObjMethod = new PedaleaCore.DAO.Productos();
                ObjReturn= ObjMethod.TraerProductos();
            }
            catch(Exception ex)
            {
                _Logger.Debug($"Excepcion en el metodo Productos.TraerTodo: {ex.Message}");
            }
            _Logger.Debug($"Finaliza el controlador Productos.TraerTodo");
            _Logger.Debug($"-------------------------------------------------------------------------------------");
            return ObjReturn;
        }

        [HttpGet("TraerPorId/{ProductoId}")]
        public PedaleaCore.DTO.Productos TraerPorId(long ProductoId)
        {
            _Logger.Debug($"-------------------------------------------------------------------------------------");
            _Logger.Debug($"Ingreso en el controlador Productos.TraerPorId: {ProductoId}");
            PedaleaCore.DTO.Productos ObjReturn = new PedaleaCore.DTO.Productos();
            try
            {
                PedaleaCore.DAO.Productos ObjMethod = new PedaleaCore.DAO.Productos();
                ObjReturn = ObjMethod.TraerProductosPorId(ProductoId);
            }
            catch (Exception ex)
            {
                _Logger.Debug($"Excepcion en el controlador Productos.TraerPorId: {ex.Message}");
            }
            _Logger.Debug($"Finaliza el controlador Productos.TraerPorId: {ProductoId}");
            _Logger.Debug($"-------------------------------------------------------------------------------------");
            return ObjReturn;
        }

        [HttpGet("Traerdisponibles")]
        public PedaleaCore.DTO.ProductosDisponiblesDepartamento Traerdisponibles()
        {
            _Logger.Debug($"-------------------------------------------------------------------------------------");
            _Logger.Debug($"Ingreso en el controlador Productos.Traerdisponibles:");
            PedaleaCore.DTO.ProductosDisponiblesDepartamento ObjReturn = new PedaleaCore.DTO.ProductosDisponiblesDepartamento();
            try
            {
                PedaleaCore.DAO.Productos ObjMethod = new PedaleaCore.DAO.Productos();
                ObjReturn = ObjMethod.TraerProductosDisponibles();
            }
            catch (Exception ex)
            {
                _Logger.Debug($"Excepcion en el controlador Productos.Traerdisponibles: {ex.Message}");
            }
            _Logger.Debug($"Finaliza el controlador Productos.Traerdisponibles: ");
            _Logger.Debug($"-------------------------------------------------------------------------------------");
            return ObjReturn;
        }

        [HttpGet("TraerdisponiblesPorDepartamento/{DepartamentoVentaId}")]
        public PedaleaCore.DTO.ProductosDisponiblesDepartamento TraerdisponiblesPorDepartamento(long DepartamentoVentaId)
        {
            _Logger.Debug($"-------------------------------------------------------------------------------------");
            _Logger.Debug($"Ingreso en el controlador Productos.TraerdisponiblesPorDepartamento: {DepartamentoVentaId}");
            PedaleaCore.DTO.ProductosDisponiblesDepartamento ObjReturn = new PedaleaCore.DTO.ProductosDisponiblesDepartamento();
            try
            {
                PedaleaCore.DAO.Productos ObjMethod = new PedaleaCore.DAO.Productos();
                ObjReturn = ObjMethod.TraerProductosDisponiblesPorDepartamento(DepartamentoVentaId);
            }
            catch (Exception ex)
            {
                _Logger.Debug($"Excepcion en el controlador Productos.TraerdisponiblesPorDepartamento: {ex.Message}");
            }
            _Logger.Debug($"Finaliza el controlador Productos.TraerdisponiblesPorDepartamento: {DepartamentoVentaId}");
            _Logger.Debug($"-------------------------------------------------------------------------------------");
            return ObjReturn;
        }

        [HttpPost("Insertar")]
        public PedaleaCore.DTO.Productos Insertar(PedaleaCore.DTO.Productos ObjRequest)
        {
            _Logger.Debug($"-------------------------------------------------------------------------------------");
            _Logger.Debug($"Ingreso en el controlador Productos.Insertar: \n{Newtonsoft.Json.JsonConvert.SerializeObject(ObjRequest, Newtonsoft.Json.Formatting.Indented)}");
            PedaleaCore.DTO.Productos ObjReturn = new PedaleaCore.DTO.Productos();
            try
            {
                PedaleaCore.DAO.Productos ObjMethod = new PedaleaCore.DAO.Productos();
                ObjReturn = ObjMethod.InsertarProductos(ObjRequest);
            }
            catch (Exception ex)
            {
                _Logger.Debug($"Excepcion en el controlador Productos.Insertar: {ex.Message}");
            }
            _Logger.Debug($"Finaliza el controlador Productos.Insertar:  \n{Newtonsoft.Json.JsonConvert.SerializeObject(ObjReturn, Newtonsoft.Json.Formatting.Indented)}");
            _Logger.Debug($"-------------------------------------------------------------------------------------");
            return ObjReturn;
        }

        [HttpPut("Actualizar/{ProductoId}")]
        public PedaleaCore.DTO.Productos Actualizar(long ProductoId, PedaleaCore.DTO.Productos ObjRequest)
        {
            _Logger.Debug($"-------------------------------------------------------------------------------------");
            ObjRequest.ProductoId = ProductoId;
            _Logger.Debug($"Ingreso en el controlador Productos.Actualizar: \n{Newtonsoft.Json.JsonConvert.SerializeObject(ObjRequest, Newtonsoft.Json.Formatting.Indented)}");
            PedaleaCore.DTO.Productos ObjReturn = new PedaleaCore.DTO.Productos();
            try
            {
                PedaleaCore.DAO.Productos ObjMethod = new PedaleaCore.DAO.Productos();
                ObjReturn = ObjMethod.ActualizarProductos(ObjRequest);
            }
            catch (Exception ex)
            {
                _Logger.Debug($"Excepcion en el controlador Productos.Actualizar: {ex.Message}");
            }
            _Logger.Debug($"Finaliza el controlador Productos.Actualizar:  \n{Newtonsoft.Json.JsonConvert.SerializeObject(ObjReturn, Newtonsoft.Json.Formatting.Indented)}");
            _Logger.Debug($"-------------------------------------------------------------------------------------");
            return ObjReturn;
        }

        [HttpDelete("Eliminar/{DetalleProductoId}")]
        public PedaleaCore.DTO.Parametros Eliminar(long DetalleProductoId)
        {
            _Logger.Debug($"-------------------------------------------------------------------------------------");
            _Logger.Debug($"Ingreso en el controlador Productos.Eliminar: {DetalleProductoId}");
            PedaleaCore.DTO.Parametros ObjReturn = new PedaleaCore.DTO.Parametros();
            try
            {
                PedaleaCore.DAO.Productos ObjMethod = new PedaleaCore.DAO.Productos();
                ObjReturn.Eliminado = ObjMethod.EliminarProductos(DetalleProductoId);
            }
            catch (Exception ex)
            {
                _Logger.Debug($"Excepcion en el controlador Productos.Eliminar: {ex.Message}");
            }
            _Logger.Debug($"Finaliza el controlador Productos.Eliminar: {ObjReturn}");
            _Logger.Debug($"-------------------------------------------------------------------------------------");
            return ObjReturn;
        }
    }
}
