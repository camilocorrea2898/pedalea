-----------INICIO CRUD DepartamentosVenta-----------
create procedure SP_DepartamentosVentaTraerTodo
as
SELECT 
DepartamentoVentaId,Descripcion,Direccion,Ciudad,Activo,FechaCreacion,FechaModificacion  
FROM DepartamentosVenta
go

create procedure SP_DepartamentosVentaTraerPorId
(@DepartamentoVentaId bigint)
as
SELECT 
DepartamentoVentaId,Descripcion,Direccion,Ciudad,Activo,FechaCreacion,FechaModificacion  
FROM DepartamentosVenta where DepartamentoVentaId=@DepartamentoVentaId
go

create procedure SP_DepartamentosVentaInsertar
(@Descripcion varchar(300),@Direccion varchar(max),@Ciudad varchar(300),@Activo bit)
as
declare @Id bigint
set @Id=0
insert into DepartamentosVenta(Descripcion,Direccion,Ciudad,Activo) 
values (@Descripcion,@Direccion,@Ciudad,@Activo)
set @Id=scope_identity()
if(@Id>0)
begin
SELECT 
DepartamentoVentaId,Descripcion,Direccion,Ciudad,Activo,FechaCreacion,FechaModificacion  
FROM DepartamentosVenta where DepartamentoVentaId=@Id
end
go

create procedure SP_DepartamentosVentaActualizar
(@DepartamentoVentaId bigint,@Descripcion varchar(300),@Direccion varchar(max),@Ciudad varchar(300),@Activo bit)
as
update DepartamentosVenta set Descripcion=@Descripcion,Direccion=@Direccion,Ciudad=@Ciudad,Activo=@Activo,FechaModificacion=getdate() 
where DepartamentoVentaId=@DepartamentoVentaId
set @DepartamentoVentaId=scope_identity()
if(@DepartamentoVentaId>0)
begin
SELECT 
DepartamentoVentaId,Descripcion,Direccion,Ciudad,Activo,FechaCreacion,FechaModificacion  
FROM DepartamentosVenta where DepartamentoVentaId=@DepartamentoVentaId
end
go

create procedure SP_DepartamentosVentaEliminar
(@DepartamentoVentaId bigint,@Status int output)
as
declare @contadorEliminado int
set @contadorEliminado=1
set @Status=0

delete from XrefPromocionesDepartamentos
where DepartamentoVentaId=@DepartamentoVentaId
set @DepartamentoVentaId=scope_identity()

SELECT @contadorEliminado=count(*)
FROM XrefPromocionesDepartamentos where DepartamentoVentaId=@DepartamentoVentaId


if(@contadorEliminado<1)
begin
	set @contadorEliminado=1
	delete from DepartamentosVenta
	where DepartamentoVentaId=@DepartamentoVentaId
	set @DepartamentoVentaId=scope_identity()

	SELECT @contadorEliminado=count(*)
	FROM DepartamentosVenta where DepartamentoVentaId=@DepartamentoVentaId

	if(@contadorEliminado<1)
	begin
		set @Status=1
	end
end
return @Status
go
-----------FIN CRUD DepartamentosVenta-----------
-----------INICIO CRUD DetallePedidos-----------
create procedure SP_DetallePedidosTraerTodo
as
SELECT 
[DetallePedidoId],[PedidoId],[ProductoId],[ValorProducto],[Cantidad],[SubTotal],FechaCreacion,FechaModificacion  
FROM DetallePedidos 
go

create procedure SP_DetallePedidosTraerPorDetalleId
(@DetallePedidoId bigint)
as
SELECT 
[DetallePedidoId],[PedidoId],[ProductoId],[ValorProducto],[Cantidad],[SubTotal],FechaCreacion,FechaModificacion  
FROM DetallePedidos where DetallePedidoId=@DetallePedidoId
go

create procedure SP_DetallePedidosTraerPorPedidoId
(@PedidoId bigint)
as
SELECT 
[DetallePedidoId],[PedidoId],[ProductoId],[ValorProducto],[Cantidad],[SubTotal],FechaCreacion,FechaModificacion  
FROM DetallePedidos where PedidoId=@PedidoId
go

create procedure SP_DetallePedidosInsertar
(@PedidoId bigint, @ProductoId bigint,@ValorProducto numeric(18,2),@Cantidad int, @SubTotal numeric(18,2))
as
declare @Id bigint
set @Id=0
insert into DetallePedidos(PedidoId,ProductoId,ValorProducto,Cantidad,SubTotal) 
values (@PedidoId,@ProductoId,@ValorProducto,@Cantidad,@SubTotal)
set @Id=scope_identity()
if(@Id>0)
begin
SELECT 
[DetallePedidoId],[PedidoId],[ProductoId],[ValorProducto],[Cantidad],[SubTotal],FechaCreacion,FechaModificacion  
FROM DetallePedidos where DetallePedidoId=@Id
end
go

create procedure SP_DetallePedidosActualizar
(@DetallePedidoId bigint,@PedidoId bigint, @ProductoId bigint,@ValorProducto numeric(18,2),@Cantidad int, @SubTotal numeric(18,2))
as
update DetallePedidos set PedidoId=@PedidoId,ProductoId=@ProductoId,ValorProducto=@ValorProducto,Cantidad=@Cantidad,SubTotal=@SubTotal,FechaModificacion=getdate() 
where DetallePedidoId=@DetallePedidoId
set @DetallePedidoId=scope_identity()
if(@DetallePedidoId>0)
begin
SELECT 
[DetallePedidoId],[PedidoId],[ProductoId],[ValorProducto],[Cantidad],[SubTotal],FechaCreacion,FechaModificacion  
FROM DetallePedidos where DetallePedidoId=@DetallePedidoId
end
go

create procedure SP_DetallePedidosEliminar
(@DetallePedidoId bigint,@Status int output)
as
declare @contadorEliminado int
set @contadorEliminado=1
set @Status=0
delete from DetallePedidos where DetallePedidoId=@DetallePedidoId

SELECT @contadorEliminado=count(*) 
FROM DetallePedidos where DetallePedidoId=@DetallePedidoId

if(@contadorEliminado<1)
begin
set @Status=1
end
return @Status
go
-----------FIN CRUD DetallePedidos-----------
-----------INICIO CRUD DetallePlanSepare-----------
create procedure SP_DetallePlanSepareTraerTodo
as
SELECT 
[DetallePlanSepareId],[PlanSepareId],[FechaPago],[ValorPagado],[MedioPago],[ValorRestante],[FechaCreacion],[FechaModificacion]
FROM DetallePlanSepare 
go

create procedure SP_DetallePlanSepareTraerPorDetalleId
(@DetallePlanSepareId bigint)
as
SELECT 
[DetallePlanSepareId],[PlanSepareId],[FechaPago],[ValorPagado],[MedioPago],[ValorRestante],[FechaCreacion],[FechaModificacion]
FROM DetallePlanSepare where [DetallePlanSepareId]=@DetallePlanSepareId
go

create procedure SP_DetallePlanSepareTraerPorPlanSepareId
(@PlanSepareId bigint)
as
SELECT 
[DetallePlanSepareId],[PlanSepareId],[FechaPago],[ValorPagado],[MedioPago],[ValorRestante],[FechaCreacion],[FechaModificacion]
FROM DetallePlanSepare where PlanSepareId=@PlanSepareId
go

create procedure SP_DetallePlanSepareInsertar
(@PlanSepareId bigint, @FechaPago datetime, @ValorPagado numeric(18,2),@MedioPago varchar(150),@ValorRestante numeric(18,2))
as
declare @Id bigint
set @Id=0
insert into DetallePlanSepare([PlanSepareId],[FechaPago],[ValorPagado],[MedioPago],[ValorRestante]) 
values (@PlanSepareId,@FechaPago,@ValorPagado,@MedioPago,@ValorRestante)
set @Id=scope_identity()
if(@Id>0)
begin
SELECT 
[DetallePlanSepareId],[PlanSepareId],[FechaPago],[ValorPagado],[MedioPago],[ValorRestante],[FechaCreacion],[FechaModificacion]
FROM DetallePlanSepare where [DetallePlanSepareId]=@Id
end
go

create procedure SP_DetallePlanSepareActualizar
(@DetallePlanSepareId bigint, @PlanSepareId bigint, @FechaPago datetime, @ValorPagado numeric(18,2),@MedioPago varchar(150),@ValorRestante numeric(18,2))
as
update DetallePlanSepare set PlanSepareId=@PlanSepareId,FechaPago=@FechaPago,ValorPagado=@ValorPagado,MedioPago=@MedioPago,ValorRestante=@ValorRestante,FechaModificacion=getdate() 
where DetallePlanSepareId=@DetallePlanSepareId
set @DetallePlanSepareId=scope_identity()
if(@DetallePlanSepareId>0)
begin
SELECT 
[DetallePlanSepareId],[PlanSepareId],[FechaPago],[ValorPagado],[MedioPago],[ValorRestante],[FechaCreacion],[FechaModificacion]
FROM DetallePlanSepare where [DetallePlanSepareId]=@DetallePlanSepareId
end
go

create procedure SP_DetallePlanSepareEliminar
(@DetallePlanSepareId bigint,@Status int output)
as
declare @contadorEliminado int
set @contadorEliminado=1
set @Status=0
delete from DetallePlanSepare where [DetallePlanSepareId]=@DetallePlanSepareId

SELECT @contadorEliminado=count(*) 
FROM DetallePlanSepare where [DetallePlanSepareId]=@DetallePlanSepareId

if(@contadorEliminado<1)
begin
set @Status=1
end
return @Status
go
-----------FIN CRUD DetallePlanSepare-----------
-----------INICIO CRUD DetalleProductos-----------
create procedure SP_DetalleProductosTraerTodo
as
SELECT 
[DetalleProductoId],[ProductoId],[Stock],[Talla],[Color],[FechaCreacion],[FechaModificacion]
FROM DetalleProductos 
go

create procedure SP_DetalleProductosTraerPorDetalleId
(@DetalleProductoId bigint)
as
SELECT 
[DetalleProductoId],[ProductoId],[Stock],[Talla],[Color],[FechaCreacion],[FechaModificacion]
FROM DetalleProductos  where DetalleProductoId=@DetalleProductoId
go

create procedure SP_DetalleProductosTraerPorProductoId
(@ProductoId bigint)
as
SELECT 
[DetalleProductoId],[ProductoId],[Stock],[Talla],[Color],[FechaCreacion],[FechaModificacion]
FROM DetalleProductos  where [ProductoId]=@ProductoId
go

create procedure SP_DetalleProductosInsertar
(@ProductoId bigint, @Stock int, @Talla varchar(300),@Color varchar(300))
as
declare @Id bigint
set @Id=0
insert into DetalleProductos([ProductoId],[Stock],[Talla],[Color]) 
values (@ProductoId,@Stock,@Talla,@Color)
set @Id=scope_identity()
if(@Id>0)
begin
SELECT 
[DetalleProductoId],[ProductoId],[Stock],[Talla],[Color],[FechaCreacion],[FechaModificacion]
FROM DetalleProductos  where DetalleProductoId=@Id
end
go

create procedure SP_DetalleProductosActualizar
(@DetalleProductoId bigint, @ProductoId bigint, @Stock int, @Talla varchar(300),@Color varchar(300))
as
update DetalleProductos set ProductoId=@ProductoId,Stock=@Stock,Talla=@Talla,Color=@Color,FechaModificacion=getdate() 
where DetalleProductoId=@DetalleProductoId
set @DetalleProductoId=scope_identity()
if(@DetalleProductoId>0)
begin
SELECT 
[DetalleProductoId],[ProductoId],[Stock],[Talla],[Color],[FechaCreacion],[FechaModificacion]
FROM DetalleProductos  where DetalleProductoId=@DetalleProductoId
end
go

create procedure SP_DetalleProductosEliminar
(@DetalleProductoId bigint,@Status int output)
as
declare @contadorEliminado int
set @contadorEliminado=1
set @Status=0
delete from DetalleProductos where DetalleProductoId=@DetalleProductoId

SELECT @contadorEliminado=count(*) 
FROM DetalleProductos where DetalleProductoId=@DetalleProductoId

if(@contadorEliminado<1)
begin
set @Status=1
end
return @Status
go
-----------FIN CRUD DetalleProductos-----------
-----------INICIO CRUD EstadosPedido-----------
create procedure SP_EstadosPedidoTraerTodo
as
SELECT 
[EstadoPedidoId],[Descripcion],[FechaCreacion],[FechaModificacion]
FROM EstadosPedido 
go

create procedure SP_EstadosPedidoTraerPorId
(@EstadoPedidoId bigint)
as
SELECT 
[EstadoPedidoId],[Descripcion],[FechaCreacion],[FechaModificacion]
FROM EstadosPedido  where EstadoPedidoId=@EstadoPedidoId
go

create procedure SP_EstadosPedidoInsertar
(@Descripcion varchar(50))
as
declare @Id bigint
set @Id=0
insert into EstadosPedido(Descripcion) 
values (@Descripcion)
set @Id=scope_identity()
if(@Id>0)
begin
SELECT 
[EstadoPedidoId],[Descripcion],[FechaCreacion],[FechaModificacion]
FROM EstadosPedido  where EstadoPedidoId=@Id
end
go

create procedure SP_EstadosPedidoActualizar
(@EstadoPedidoId bigint, @Descripcion varchar(50))
as
update EstadosPedido set Descripcion=@Descripcion,FechaModificacion=getdate() 
where EstadoPedidoId=@EstadoPedidoId
set @EstadoPedidoId=scope_identity()
if(@EstadoPedidoId>0)
begin
SELECT 
[EstadoPedidoId],[Descripcion],[FechaCreacion],[FechaModificacion]
FROM EstadosPedido  where EstadoPedidoId=@EstadoPedidoId
end
go

create procedure SP_EstadosPedidoEliminar
(@EstadoPedidoId bigint,@Status int output)
as
declare @contadorEliminado int
set @contadorEliminado=1
set @Status=0
delete from EstadosPedido where EstadoPedidoId=@EstadoPedidoId

SELECT @contadorEliminado=count(*) 
FROM EstadosPedido where EstadoPedidoId=@EstadoPedidoId

if(@contadorEliminado<1)
begin
set @Status=1
end
return @Status
go
-----------FIN CRUD EstadosPedido-----------
-----------INICIO CRUD Pedidos-----------
create procedure SP_PedidosTraerTodo
as
SELECT 
[PedidoId],[FechaPedido],[EstadoPedidoId],[DireccionEnvio],[FechaEnvio],[IdentificacionCliente],[SubTotal],[ValorTotal],[PromocionId],[FechaCreacion],[FechaModificacion]
FROM Pedidos  where [EstadoPedidoId]<>6
go

create procedure SP_PedidosTraerPorId
(@PedidoId bigint)
as
SELECT 
[PedidoId],[FechaPedido],[EstadoPedidoId],[DireccionEnvio],[FechaEnvio],[IdentificacionCliente],[SubTotal],[ValorTotal],[PromocionId],[FechaCreacion],[FechaModificacion]
FROM Pedidos  where PedidoId=@PedidoId and [EstadoPedidoId]<>6
go

create procedure SP_PedidosInsertar
(@FechaPedido datetime,@EstadoPedidoId int,@DireccionEnvio varchar(Max),@FechaEnvio datetime,@IdentificacionCliente varchar(50),@SubTotal numeric(18,2),@ValorTotal numeric(18,2),@PromocionId bigint)
as
declare @Id bigint
set @Id=0
insert into Pedidos([FechaPedido],[EstadoPedidoId],[DireccionEnvio],[FechaEnvio],[IdentificacionCliente],[SubTotal],[ValorTotal],[PromocionId]) 
values (@FechaPedido,@EstadoPedidoId,@DireccionEnvio,@FechaEnvio,@IdentificacionCliente,@SubTotal,@ValorTotal,@PromocionId)
set @Id=scope_identity()
if(@Id>0)
begin
SELECT 
[PedidoId],[FechaPedido],[EstadoPedidoId],[DireccionEnvio],[FechaEnvio],[IdentificacionCliente],[SubTotal],[ValorTotal],[PromocionId],[FechaCreacion],[FechaModificacion]
FROM Pedidos  where PedidoId=@Id
end
go

create procedure SP_PedidosActualizar
(@PedidoId bigint,@FechaPedido datetime,@EstadoPedidoId int,@DireccionEnvio varchar(Max),@FechaEnvio datetime,@IdentificacionCliente varchar(50),@SubTotal numeric(18,2),@ValorTotal numeric(18,2),@PromocionId bigint)
as
update Pedidos set [FechaPedido]=@FechaPedido,[EstadoPedidoId]=@EstadoPedidoId,[DireccionEnvio]=@DireccionEnvio,[FechaEnvio]=@FechaEnvio,[IdentificacionCliente]=@IdentificacionCliente,[SubTotal]=@SubTotal,[ValorTotal]=@ValorTotal,[PromocionId]=@PromocionId,FechaModificacion=getdate() 
where PedidoId=@PedidoId
set @PedidoId=scope_identity()
if(@PedidoId>0)
begin
SELECT 
[PedidoId],[FechaPedido],[EstadoPedidoId],[DireccionEnvio],[FechaEnvio],[IdentificacionCliente],[SubTotal],[ValorTotal],[PromocionId],[FechaCreacion],[FechaModificacion]
FROM Pedidos  where PedidoId=@PedidoId
end
go

create procedure SP_PedidosEliminar
(@PedidoId bigint,@Status int output)
as
declare @contadorEliminado int
declare @EstadoPedidoId int
set @contadorEliminado=1
set @Status=0

update Pedidos set [EstadoPedidoId]=6,FechaModificacion=getdate() 
where PedidoId=@PedidoId
set @PedidoId=scope_identity()

SELECT @contadorEliminado=count(*)
FROM Pedidos where PedidoId=@PedidoId

SELECT @EstadoPedidoId=EstadoPedidoId
FROM Pedidos where PedidoId=@PedidoId

if(@contadorEliminado>0 and @EstadoPedidoId=6)
begin
set @Status=1
end
return @Status
go
-----------FIN CRUD Pedidos-----------
-----------INICIO CRUD PlanSepare-----------
create procedure SP_PlanSepareTraerTodo
as
SELECT 
[PlanSepareId],[PedidoId],[ValorTotal],[ValorPagado],[CantidadCuotas],[FechaCreacion],[FechaModificacion]
FROM PlanSepare  
go

create procedure SP_PlanSepareTraerPorId
(@PlanSepareId bigint)
as
SELECT 
[PlanSepareId],[PedidoId],[ValorTotal],[ValorPagado],[CantidadCuotas],[FechaCreacion],[FechaModificacion]
FROM PlanSepare  where PlanSepareId=@PlanSepareId
go

create procedure SP_PlanSepareInsertar
(@PedidoId bigint,@ValorTotal numeric(18,2),@ValorPagado numeric(18,2),@CantidadCuotas int)
as
declare @Id bigint
set @Id=0
insert into PlanSepare([PedidoId],[ValorTotal],[ValorPagado],[CantidadCuotas]) 
values (@PedidoId,@ValorTotal,@ValorPagado,@CantidadCuotas)
set @Id=scope_identity()
if(@Id>0)
begin
SELECT 
[PlanSepareId],[PedidoId],[ValorTotal],[ValorPagado],[CantidadCuotas],[FechaCreacion],[FechaModificacion]
FROM PlanSepare  where PlanSepareId=@Id
end
go

create procedure SP_PlanSepareActualizar
(@PlanSepareId bigint,@PedidoId bigint,@ValorTotal numeric(18,2),@ValorPagado numeric(18,2),@CantidadCuotas int)
as
update PlanSepare set [PedidoId]=@PedidoId,[ValorTotal]=@ValorTotal,[ValorPagado]=@ValorPagado,[CantidadCuotas]=@CantidadCuotas,FechaModificacion=getdate() 
where PlanSepareId=@PlanSepareId
set @PlanSepareId=scope_identity()
if(@PlanSepareId>0)
begin
SELECT 
[PlanSepareId],[PedidoId],[ValorTotal],[ValorPagado],[CantidadCuotas],[FechaCreacion],[FechaModificacion]
FROM PlanSepare  where PlanSepareId=@PlanSepareId
end
go

create procedure SP_PlanSepareEliminar
(@PlanSepareId bigint,@Status int output)
as
declare @contadorEliminado int
set @contadorEliminado=1
set @Status=0

delete from DetallePlanSepare
where PlanSepareId=@PlanSepareId
set @PlanSepareId=scope_identity()

SELECT @contadorEliminado=count(*)
FROM DetallePlanSepare where PlanSepareId=@PlanSepareId


if(@contadorEliminado<1)
begin
	set @contadorEliminado=1
	delete from PlanSepare
	where PlanSepareId=@PlanSepareId
	set @PlanSepareId=scope_identity()

	SELECT @contadorEliminado=count(*)
	FROM PlanSepare where PlanSepareId=@PlanSepareId

	if(@contadorEliminado<1)
	begin
		set @Status=1
	end
end
return @Status
go
-----------FIN CRUD PlanSepare-----------
-----------INICIO CRUD Productos-----------
create procedure SP_ProductosTraerTodo
as
SELECT 
[ProductoId],[Descripcion],[Valor],[DepartamentoVentaId],[FechaCreacion],[FechaModificacion]
FROM Productos  
go

create procedure SP_ProductosTraerPorId
(@ProductoId bigint)
as
SELECT 
[ProductoId],[Descripcion],[Valor],[DepartamentoVentaId],[FechaCreacion],[FechaModificacion]
FROM Productos  where ProductoId=@ProductoId
go

create procedure SP_ProductosInsertar
(@Descripcion varchar(300),@Valor numeric(18,2),@DepartamentoVentaId bigint)
as
declare @Id bigint
set @Id=0
insert into Productos([Descripcion],[Valor],[DepartamentoVentaId]) 
values (@Descripcion,@Valor,@DepartamentoVentaId)
set @Id=scope_identity()
if(@Id>0)
begin
SELECT 
[ProductoId],[Descripcion],[Valor],[DepartamentoVentaId],[FechaCreacion],[FechaModificacion]
FROM Productos  where ProductoId=@Id
end
go

create procedure SP_ProductosActualizar
(@ProductoId bigint,@Descripcion varchar(300),@Valor numeric(18,2),@DepartamentoVentaId bigint)
as
update Productos set [Descripcion]=@Descripcion,[Valor]=@Valor,[DepartamentoVentaId]=@DepartamentoVentaId,FechaModificacion=getdate() 
where ProductoId=@ProductoId
set @ProductoId=scope_identity()
if(@ProductoId>0)
begin
SELECT 
[ProductoId],[Descripcion],[Valor],[DepartamentoVentaId],[FechaCreacion],[FechaModificacion]
FROM Productos  where ProductoId=@ProductoId
end
go

create procedure SP_ProductosEliminar
(@ProductoId bigint,@Status int output)
as
declare @contadorEliminado int
set @contadorEliminado=1
set @Status=0

delete from DetalleProductos
where ProductoId=@ProductoId
set @ProductoId=scope_identity()

SELECT @contadorEliminado=count(*)
FROM DetalleProductos where ProductoId=@ProductoId


if(@contadorEliminado<1)
begin
	set @contadorEliminado=1
	delete from Productos
	where ProductoId=@ProductoId
	set @ProductoId=scope_identity()

	SELECT @contadorEliminado=count(*)
	FROM Productos where ProductoId=@ProductoId

	if(@contadorEliminado<1)
	begin
		set @Status=1
	end
end
return @Status
go

create procedure SP_TraerProductosDisponibles
as
	set nocount on;
	select a.productoId
	,a.StockTotal
	,b.Descripcion as 'DescripcionProducto'
	,b.Valor as 'ValorProducto'
	,c.Descripcion as 'PuntoVenta'
	,c.Direccion as 'DireccionPuntoVenta'
	,c.Ciudad as 'CiudadPuntoVenta'
	from [dbo].[View_ProductosDisponibles] a
	inner join Productos b on a.ProductoId = b.ProductoId
	inner join DepartamentosVenta c on b.DepartamentoVentaId = c.DepartamentoVentaId
	where c.Activo=1
go

create procedure [dbo].[SP_TraerProductosDisponiblesPorDepartamento]
(@DepartamentoVentaId bigint)
as
	set nocount on;
	select cast(a.productoId as bigint) as productoId
	,cast(a.StockTotal as int) as StockTotal
	,b.Descripcion as 'DescripcionProducto'
	,cast(b.Valor as numeric(18,2)) as 'ValorProducto'
	,c.Descripcion as 'PuntoVenta'
	,c.Direccion as 'DireccionPuntoVenta'
	,c.Ciudad as 'CiudadPuntoVenta'
	from [dbo].[View_ProductosDisponibles] a
	inner join Productos b on a.ProductoId = b.ProductoId
	inner join DepartamentosVenta c on b.DepartamentoVentaId = c.DepartamentoVentaId
	where c.Activo=1 and b.DepartamentoVentaId=@DepartamentoVentaId
go
-----------FIN CRUD Productos-----------
-----------INICIO CRUD Promociones-----------
create procedure SP_PromocionesTraerTodo
as
SELECT 
[PromocionId],[Descripcion],[MontoMinimo],[MontoMaximo],[CantidadProductosMinimo],[PorcentajePromocion],[FechaCreacion],[FechaModificacion]
FROM Promociones 
go

create procedure SP_PromocionesTraerPorId
(@PromocionId bigint)
as
SELECT 
[PromocionId],[Descripcion],[MontoMinimo],[MontoMaximo],[CantidadProductosMinimo],[PorcentajePromocion],[FechaCreacion],[FechaModificacion]
FROM Promociones  where PromocionId=@PromocionId
go

create procedure SP_PromocionesInsertar
(@Descripcion varchar(200),@MontoMinimo numeric(18,2),@MontoMaximo numeric(18,2),@CantidadProductosMinimo int,@PorcentajePromocion numeric(4,2))
as
declare @Id bigint
set @Id=0
insert into Promociones([Descripcion],[MontoMinimo],[MontoMaximo],[CantidadProductosMinimo],[PorcentajePromocion]) 
values (@Descripcion,@MontoMinimo,@MontoMaximo,@CantidadProductosMinimo,@PorcentajePromocion)
set @Id=scope_identity()
if(@Id>0)
begin
SELECT 
[PromocionId],[Descripcion],[MontoMinimo],[MontoMaximo],[CantidadProductosMinimo],[PorcentajePromocion],[FechaCreacion],[FechaModificacion]
FROM Promociones  where PromocionId=@Id
end
go

create procedure SP_PromocionesActualizar
(@PromocionId bigint, @Descripcion varchar(200),@MontoMinimo numeric(18,2),@MontoMaximo numeric(18,2),@CantidadProductosMinimo int,@PorcentajePromocion numeric(4,2))
as
update Promociones set [Descripcion]=@Descripcion,[MontoMinimo]=@MontoMinimo,[MontoMaximo]=@MontoMaximo,[CantidadProductosMinimo]=@CantidadProductosMinimo,[PorcentajePromocion]=@PorcentajePromocion,FechaModificacion=getdate() 
where PromocionId=@PromocionId
set @PromocionId=scope_identity()
if(@PromocionId>0)
begin
SELECT 
[PromocionId],[Descripcion],[MontoMinimo],[MontoMaximo],[CantidadProductosMinimo],[PorcentajePromocion],[FechaCreacion],[FechaModificacion]
FROM Promociones  where PromocionId=@PromocionId
end
go

create procedure SP_PromocionesEliminar
(@PromocionId bigint,@Status int output)
as
declare @contadorEliminado int
set @contadorEliminado=1
set @Status=0

delete from XrefPromocionesDepartamentos
where PromocionId=@PromocionId
set @PromocionId=scope_identity()

SELECT @contadorEliminado=count(*)
FROM XrefPromocionesDepartamentos where PromocionId=@PromocionId


if(@contadorEliminado<1)
begin
	set @contadorEliminado=1
	delete from Promociones
	where PromocionId=@PromocionId
	set @PromocionId=scope_identity()

	SELECT @contadorEliminado=count(*)
	FROM Promociones where PromocionId=@PromocionId

	if(@contadorEliminado<1)
	begin
		set @Status=1
	end
end
return @Status
go

create procedure SP_ValidarPromociones
(@DepartamentoVentaId bigint,@IdentificacionCliente varchar(50),@ValorCompra numeric(18,2),
@ValorFinal numeric(18,2) output,@AplicaSepare bit output)
as
	set nocount on;
	declare @cantidadPedidos int
	declare @AplicaPromocion bit
	declare @ContadorPromociones int
	declare @ProcentajePromocion numeric(4,2)

	set @ValorFinal=@ValorCompra
	set @AplicaSepare=0
	set @ProcentajePromocion=0

	--Se valida cu�ntos pedidos finalizados con anterioridad
	select @cantidadPedidos=coalesce (sum(PedidoId),0) from Pedidos
	where EstadoPedidoId=3 and IdentificacionCliente=@IdentificacionCliente

	--Se validad si el cliente ha tenido m�s de dos pedidos para aplicar promoci�n 
	if(@cantidadPedidos>=2)
	begin
		--Si aplica para promoci�n se consulta cu�l es el porcentaje de la promoci�n a la que aplica
		select top(1) @ProcentajePromocion=coalesce(a.PorcentajePromocion,0)
		from Promociones a
		inner join XrefPromocionesDepartamentos b on a.PromocionId=b.PromocionId
		inner join DepartamentosVenta c on c.DepartamentoVentaId=b.DepartamentoVentaId
		where c.DepartamentoVentaId=@DepartamentoVentaId and @ValorCompra between a.MontoMinimo and a.MontoMaximo
	end

	--Se valida si se encontraron promociones que aplicaran al cliente
	if(@ProcentajePromocion>0)
	begin
		--Si el cliente aplica para promoci�n se realiza el calculo del valor con promoci�n y se asigna a variable que retorna
		set @ValorFinal=@ValorCompra-((@ValorCompra*@ProcentajePromocion)/100)
	end

	--Se valida si el cliente aplica para el Plan Separe
	if(@ValorFinal>=1000000)
	begin
		set @AplicaSepare=1
	end
go

-----------FIN CRUD Promociones-----------