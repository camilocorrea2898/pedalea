create database Pedalea;
Use Pedalea;

create table EstadosPedido (
EstadoPedidoId int not null IDENTITY(1,1) PRIMARY KEY,
Descripcion varchar(50) NOT NULL,
FechaCreacion datetime NOT NULL DEFAULT(GETDATE()),
FechaModificacion datetime NOT NULL DEFAULT(GETDATE())
);

create table Promociones (
PromocionId bigint not null IDENTITY(1,1) PRIMARY KEY,
Descripcion varchar(200) NOT NULL,
MontoMinimo numeric(18,0) NOT NULL,
MontoMaximo numeric(18,0) NOT NULL,
CantidadProductosMinimo int NOT NULL,
PorcentajePromocion numeric(2,2) NOT NULL,
FechaCreacion datetime NOT NULL DEFAULT(GETDATE()),
FechaModificacion datetime NOT NULL DEFAULT(GETDATE())
);

create table DepartamentosVenta (
DepartamentoVentaId bigint not null IDENTITY(1,1) PRIMARY KEY,
Descripcion varchar(300) NOT NULL,
Direccion varchar(MAX) NOT NULL,
Ciudad varchar(300) NOT NULL,
Activo bit NOT NULL DEFAULT(1),
FechaCreacion datetime NOT NULL DEFAULT(GETDATE()),
FechaModificacion datetime NOT NULL DEFAULT(GETDATE())
);

create table XrefPromocionesDepartamentos (
PromocionId bigint not null,
DepartamentoVentaId bigint not null,
FOREIGN KEY (PromocionId) references Promociones(PromocionId),
FOREIGN KEY (DepartamentoVentaId) references DepartamentosVenta(DepartamentoVentaId),
);

create table Productos (
ProductoId bigint not null IDENTITY(1,1) PRIMARY KEY,
Descripcion varchar(300) NOT NULL,
Valor numeric(18,2) NOT NULL,
DepartamentoVentaId bigint not null,
FechaCreacion datetime NOT NULL DEFAULT(GETDATE()),
FechaModificacion datetime NOT NULL DEFAULT(GETDATE()),
FOREIGN KEY (DepartamentoVentaId) references DepartamentosVenta(DepartamentoVentaId)
);

create table DetalleProductos (
DetalleProductoId bigint not null IDENTITY(1,1) PRIMARY KEY,
ProductoId bigint not null,
Stock int NOT NULL DEFAULT(0),
Talla varchar(300) NULL,
Color varchar(300) NULL,
FechaCreacion datetime NOT NULL DEFAULT(GETDATE()),
FechaModificacion datetime NOT NULL DEFAULT(GETDATE()),
FOREIGN KEY (ProductoId) references Productos(ProductoId)
);

create table Pedidos (
PedidoId bigint not null IDENTITY(1,1) PRIMARY KEY,
FechaPedido datetime NOT NULL DEFAULT(GETDATE()),
EstadoPedidoId int not null,
DireccionEnvio varchar(max) NULL,
FechaEnvio datetime NULL,
IdentificacionCliente varchar(50) NULL,
SubTotal numeric(18,2) NOT NULL,
ValorTotal numeric(18,2) NOT NULL,
PromocionId bigint null,
FechaCreacion datetime NOT NULL DEFAULT(GETDATE()),
FechaModificacion datetime NOT NULL DEFAULT(GETDATE()),
FOREIGN KEY (PromocionId) references Promociones(PromocionId) ON DELETE CASCADE,
FOREIGN KEY (EstadoPedidoId) references EstadosPedido(EstadoPedidoId)
);

create table DetallePedidos (
DetallePedidoId bigint not null IDENTITY(1,1) PRIMARY KEY,
PedidoId bigint not null,
ProductoId bigint not null,
ValorProducto numeric(18,2) NOT NULL,
Cantidad int not null,
SubTotal numeric(18,2) NOT NULL,
FechaCreacion datetime NOT NULL DEFAULT(GETDATE()),
FechaModificacion datetime NOT NULL DEFAULT(GETDATE()),
FOREIGN KEY (PedidoId) references Pedidos(PedidoId),
FOREIGN KEY (ProductoId) references Productos(ProductoId),
);

create table PlanSepare (
PlanSepareId bigint not null IDENTITY(1,1) PRIMARY KEY,
PedidoId bigint not null,
ValorTotal numeric(18,2) NOT NULL,
ValorPagado numeric(18,2) NOT NULL,
CantidadCuotas int NOT NULL,
FechaCreacion datetime NOT NULL DEFAULT(GETDATE()),
FechaModificacion datetime NOT NULL DEFAULT(GETDATE()),
FOREIGN KEY (PedidoId) references Pedidos(PedidoId)
);

create table DetallePlanSepare (
DetallePlanSepareId bigint not null IDENTITY(1,1) PRIMARY KEY,
PlanSepareId bigint not null,
FechaPago datetime NOT NULL,
ValorPagado numeric(18,2) NOT NULL,
MedioPago varchar(150) NULL,
ValorRestante numeric(18,2) NOT NULL,
FechaCreacion datetime NOT NULL DEFAULT(GETDATE()),
FechaModificacion datetime NOT NULL DEFAULT(GETDATE()),
FOREIGN KEY (PlanSepareId) references PlanSepare(PlanSepareId)
);