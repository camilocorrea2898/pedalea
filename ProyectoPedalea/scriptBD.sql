USE [Pedalea]
GO
/****** Object:  User [camilo]    Script Date: 06/04/2022 21:00:15 ******/
CREATE USER [camilo] FOR LOGIN [camilo] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  Table [dbo].[Productos]    Script Date: 06/04/2022 21:00:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Productos](
	[ProductoId] [bigint] IDENTITY(1,1) NOT NULL,
	[Descripcion] [varchar](300) NOT NULL,
	[Valor] [numeric](18, 2) NOT NULL,
	[DepartamentoVentaId] [bigint] NOT NULL,
	[FechaCreacion] [datetime] NOT NULL,
	[FechaModificacion] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ProductoId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DetalleProductos]    Script Date: 06/04/2022 21:00:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DetalleProductos](
	[DetalleProductoId] [bigint] IDENTITY(1,1) NOT NULL,
	[ProductoId] [bigint] NOT NULL,
	[Stock] [int] NOT NULL,
	[Talla] [varchar](300) NULL,
	[Color] [varchar](300) NULL,
	[FechaCreacion] [datetime] NOT NULL,
	[FechaModificacion] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[DetalleProductoId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[View_ProductosDisponibles]    Script Date: 06/04/2022 21:00:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
	Create View [dbo].[View_ProductosDisponibles]
	as
	select b.ProductoId,sum(a.stock) as StockTotal from DetalleProductos a
	inner join Productos b on a.ProductoId = b.ProductoId 
	group by b.ProductoId
	having  sum(a.stock)>=1
GO
/****** Object:  Table [dbo].[DepartamentosVenta]    Script Date: 06/04/2022 21:00:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DepartamentosVenta](
	[DepartamentoVentaId] [bigint] IDENTITY(1,1) NOT NULL,
	[Descripcion] [varchar](300) NOT NULL,
	[Direccion] [varchar](max) NOT NULL,
	[Ciudad] [varchar](300) NOT NULL,
	[Activo] [bit] NOT NULL,
	[FechaCreacion] [datetime] NOT NULL,
	[FechaModificacion] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[DepartamentoVentaId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DetallePedidos]    Script Date: 06/04/2022 21:00:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DetallePedidos](
	[DetallePedidoId] [bigint] IDENTITY(1,1) NOT NULL,
	[PedidoId] [bigint] NOT NULL,
	[ProductoId] [bigint] NOT NULL,
	[ValorProducto] [numeric](18, 2) NOT NULL,
	[Cantidad] [int] NOT NULL,
	[SubTotal] [numeric](18, 2) NOT NULL,
	[FechaCreacion] [datetime] NOT NULL,
	[FechaModificacion] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[DetallePedidoId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DetallePlanSepare]    Script Date: 06/04/2022 21:00:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DetallePlanSepare](
	[DetallePlanSepareId] [bigint] IDENTITY(1,1) NOT NULL,
	[PlanSepareId] [bigint] NOT NULL,
	[FechaPago] [datetime] NOT NULL,
	[ValorPagado] [numeric](18, 2) NOT NULL,
	[MedioPago] [varchar](150) NULL,
	[ValorRestante] [numeric](18, 2) NOT NULL,
	[FechaCreacion] [datetime] NOT NULL,
	[FechaModificacion] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[DetallePlanSepareId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[EstadosPedido]    Script Date: 06/04/2022 21:00:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EstadosPedido](
	[EstadoPedidoId] [int] IDENTITY(1,1) NOT NULL,
	[Descripcion] [varchar](50) NOT NULL,
	[FechaCreacion] [datetime] NOT NULL,
	[FechaModificacion] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[EstadoPedidoId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Pedidos]    Script Date: 06/04/2022 21:00:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Pedidos](
	[PedidoId] [bigint] IDENTITY(1,1) NOT NULL,
	[FechaPedido] [datetime] NOT NULL,
	[EstadoPedidoId] [int] NOT NULL,
	[DireccionEnvio] [varchar](max) NULL,
	[FechaEnvio] [datetime] NULL,
	[IdentificacionCliente] [varchar](50) NULL,
	[SubTotal] [numeric](18, 2) NOT NULL,
	[ValorTotal] [numeric](18, 2) NOT NULL,
	[PromocionId] [bigint] NULL,
	[FechaCreacion] [datetime] NOT NULL,
	[FechaModificacion] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[PedidoId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PlanSepare]    Script Date: 06/04/2022 21:00:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PlanSepare](
	[PlanSepareId] [bigint] IDENTITY(1,1) NOT NULL,
	[PedidoId] [bigint] NOT NULL,
	[ValorTotal] [numeric](18, 2) NOT NULL,
	[ValorPagado] [numeric](18, 2) NOT NULL,
	[CantidadCuotas] [int] NOT NULL,
	[FechaCreacion] [datetime] NOT NULL,
	[FechaModificacion] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[PlanSepareId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Promociones]    Script Date: 06/04/2022 21:00:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Promociones](
	[PromocionId] [bigint] IDENTITY(1,1) NOT NULL,
	[Descripcion] [varchar](200) NOT NULL,
	[MontoMinimo] [numeric](18, 0) NOT NULL,
	[MontoMaximo] [numeric](18, 0) NOT NULL,
	[CantidadProductosMinimo] [int] NOT NULL,
	[PorcentajePromocion] [numeric](4, 2) NULL,
	[FechaCreacion] [datetime] NOT NULL,
	[FechaModificacion] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[PromocionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[XrefPromocionesDepartamentos]    Script Date: 06/04/2022 21:00:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[XrefPromocionesDepartamentos](
	[PromocionId] [bigint] NOT NULL,
	[DepartamentoVentaId] [bigint] NOT NULL
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[DepartamentosVenta] ON 

INSERT [dbo].[DepartamentosVenta] ([DepartamentoVentaId], [Descripcion], [Direccion], [Ciudad], [Activo], [FechaCreacion], [FechaModificacion]) VALUES (1, N'Centro Mayor', N'Calle 1 # 1 1', N'Bogotá', 1, CAST(N'2022-04-04T19:40:20.153' AS DateTime), CAST(N'2022-04-04T19:40:20.153' AS DateTime))
INSERT [dbo].[DepartamentosVenta] ([DepartamentoVentaId], [Descripcion], [Direccion], [Ciudad], [Activo], [FechaCreacion], [FechaModificacion]) VALUES (2, N'Contro comercial Titan Plaza', N'Calle 80 Av Boyacá', N'Bogotá', 1, CAST(N'2022-04-04T19:40:58.310' AS DateTime), CAST(N'2022-04-04T19:40:58.310' AS DateTime))
INSERT [dbo].[DepartamentosVenta] ([DepartamentoVentaId], [Descripcion], [Direccion], [Ciudad], [Activo], [FechaCreacion], [FechaModificacion]) VALUES (3, N'Metrocentro', N'Cale 12 # 45 65', N'Cali', 1, CAST(N'2022-04-04T23:22:30.440' AS DateTime), CAST(N'2022-04-04T23:22:30.440' AS DateTime))
INSERT [dbo].[DepartamentosVenta] ([DepartamentoVentaId], [Descripcion], [Direccion], [Ciudad], [Activo], [FechaCreacion], [FechaModificacion]) VALUES (4, N'Metrocentro', N'Cale 12 # 45 65', N'Cali', 1, CAST(N'2022-04-04T23:25:21.400' AS DateTime), CAST(N'2022-04-04T23:25:21.400' AS DateTime))
INSERT [dbo].[DepartamentosVenta] ([DepartamentoVentaId], [Descripcion], [Direccion], [Ciudad], [Activo], [FechaCreacion], [FechaModificacion]) VALUES (5, N'Metrocentro', N'Cale 12 # 45 65', N'Cali', 1, CAST(N'2022-04-04T23:27:12.400' AS DateTime), CAST(N'2022-04-04T23:27:12.400' AS DateTime))
INSERT [dbo].[DepartamentosVenta] ([DepartamentoVentaId], [Descripcion], [Direccion], [Ciudad], [Activo], [FechaCreacion], [FechaModificacion]) VALUES (6, N'Metrocentro', N'Cale 12 # 45 65', N'Cali', 1, CAST(N'2022-04-04T23:28:41.403' AS DateTime), CAST(N'2022-04-04T23:28:41.403' AS DateTime))
INSERT [dbo].[DepartamentosVenta] ([DepartamentoVentaId], [Descripcion], [Direccion], [Ciudad], [Activo], [FechaCreacion], [FechaModificacion]) VALUES (7, N'Metrocentro', N'Cale 12 # 45 65', N'Cali', 1, CAST(N'2022-04-04T23:42:40.250' AS DateTime), CAST(N'2022-04-04T23:42:40.250' AS DateTime))
SET IDENTITY_INSERT [dbo].[DepartamentosVenta] OFF
GO
SET IDENTITY_INSERT [dbo].[DetalleProductos] ON 

INSERT [dbo].[DetalleProductos] ([DetalleProductoId], [ProductoId], [Stock], [Talla], [Color], [FechaCreacion], [FechaModificacion]) VALUES (1, 2, 5, N'S', N'Amarilla', CAST(N'2022-04-04T19:41:33.940' AS DateTime), CAST(N'2022-04-04T19:41:33.940' AS DateTime))
INSERT [dbo].[DetalleProductos] ([DetalleProductoId], [ProductoId], [Stock], [Talla], [Color], [FechaCreacion], [FechaModificacion]) VALUES (2, 2, 10, N'S', N'Azul', CAST(N'2022-04-04T19:41:42.183' AS DateTime), CAST(N'2022-04-04T19:41:42.183' AS DateTime))
INSERT [dbo].[DetalleProductos] ([DetalleProductoId], [ProductoId], [Stock], [Talla], [Color], [FechaCreacion], [FechaModificacion]) VALUES (3, 2, 3, N'M', N'Amarilla', CAST(N'2022-04-04T19:41:52.230' AS DateTime), CAST(N'2022-04-04T19:41:52.230' AS DateTime))
INSERT [dbo].[DetalleProductos] ([DetalleProductoId], [ProductoId], [Stock], [Talla], [Color], [FechaCreacion], [FechaModificacion]) VALUES (4, 2, 4, N'M', N'Azul', CAST(N'2022-04-04T19:41:58.890' AS DateTime), CAST(N'2022-04-04T19:41:58.890' AS DateTime))
INSERT [dbo].[DetalleProductos] ([DetalleProductoId], [ProductoId], [Stock], [Talla], [Color], [FechaCreacion], [FechaModificacion]) VALUES (5, 3, 5, NULL, NULL, CAST(N'2022-04-04T20:12:06.533' AS DateTime), CAST(N'2022-04-04T20:12:06.533' AS DateTime))
SET IDENTITY_INSERT [dbo].[DetalleProductos] OFF
GO
SET IDENTITY_INSERT [dbo].[EstadosPedido] ON 

INSERT [dbo].[EstadosPedido] ([EstadoPedidoId], [Descripcion], [FechaCreacion], [FechaModificacion]) VALUES (1, N'Pendiente', CAST(N'2022-04-04T20:18:48.953' AS DateTime), CAST(N'2022-04-04T20:18:48.953' AS DateTime))
INSERT [dbo].[EstadosPedido] ([EstadoPedidoId], [Descripcion], [FechaCreacion], [FechaModificacion]) VALUES (2, N'Pendiente envio', CAST(N'2022-04-04T20:19:11.870' AS DateTime), CAST(N'2022-04-04T20:19:11.870' AS DateTime))
INSERT [dbo].[EstadosPedido] ([EstadoPedidoId], [Descripcion], [FechaCreacion], [FechaModificacion]) VALUES (3, N'Finalizado', CAST(N'2022-04-04T20:19:14.873' AS DateTime), CAST(N'2022-04-04T20:19:14.873' AS DateTime))
INSERT [dbo].[EstadosPedido] ([EstadoPedidoId], [Descripcion], [FechaCreacion], [FechaModificacion]) VALUES (4, N'Rechazado', CAST(N'2022-04-04T20:19:19.277' AS DateTime), CAST(N'2022-04-04T20:19:19.277' AS DateTime))
INSERT [dbo].[EstadosPedido] ([EstadoPedidoId], [Descripcion], [FechaCreacion], [FechaModificacion]) VALUES (5, N'Separado', CAST(N'2022-04-04T20:19:30.237' AS DateTime), CAST(N'2022-04-04T20:19:30.237' AS DateTime))
INSERT [dbo].[EstadosPedido] ([EstadoPedidoId], [Descripcion], [FechaCreacion], [FechaModificacion]) VALUES (6, N'Eliminado', CAST(N'2022-04-05T12:22:33.523' AS DateTime), CAST(N'2022-04-05T12:22:33.523' AS DateTime))
SET IDENTITY_INSERT [dbo].[EstadosPedido] OFF
GO
SET IDENTITY_INSERT [dbo].[Productos] ON 

INSERT [dbo].[Productos] ([ProductoId], [Descripcion], [Valor], [DepartamentoVentaId], [FechaCreacion], [FechaModificacion]) VALUES (2, N'Camisa Seleccion Colombia', CAST(220000.00 AS Numeric(18, 2)), 1, CAST(N'2022-04-04T19:41:09.257' AS DateTime), CAST(N'2022-04-04T19:41:09.257' AS DateTime))
INSERT [dbo].[Productos] ([ProductoId], [Descripcion], [Valor], [DepartamentoVentaId], [FechaCreacion], [FechaModificacion]) VALUES (3, N'Balón mundial', CAST(500000.00 AS Numeric(18, 2)), 2, CAST(N'2022-04-04T20:11:55.247' AS DateTime), CAST(N'2022-04-04T20:11:55.247' AS DateTime))
SET IDENTITY_INSERT [dbo].[Productos] OFF
GO
SET IDENTITY_INSERT [dbo].[Promociones] ON 

INSERT [dbo].[Promociones] ([PromocionId], [Descripcion], [MontoMinimo], [MontoMaximo], [CantidadProductosMinimo], [PorcentajePromocion], [FechaCreacion], [FechaModificacion]) VALUES (5, N'prueba', CAST(0 AS Numeric(18, 0)), CAST(100000 AS Numeric(18, 0)), 1, CAST(2.00 AS Numeric(4, 2)), CAST(N'2022-04-04T20:57:09.850' AS DateTime), CAST(N'2022-04-04T20:57:09.850' AS DateTime))
SET IDENTITY_INSERT [dbo].[Promociones] OFF
GO
INSERT [dbo].[XrefPromocionesDepartamentos] ([PromocionId], [DepartamentoVentaId]) VALUES (5, 1)
GO
ALTER TABLE [dbo].[DepartamentosVenta] ADD  DEFAULT ((1)) FOR [Activo]
GO
ALTER TABLE [dbo].[DepartamentosVenta] ADD  DEFAULT (getdate()) FOR [FechaCreacion]
GO
ALTER TABLE [dbo].[DepartamentosVenta] ADD  DEFAULT (getdate()) FOR [FechaModificacion]
GO
ALTER TABLE [dbo].[DetallePedidos] ADD  DEFAULT (getdate()) FOR [FechaCreacion]
GO
ALTER TABLE [dbo].[DetallePedidos] ADD  DEFAULT (getdate()) FOR [FechaModificacion]
GO
ALTER TABLE [dbo].[DetallePlanSepare] ADD  DEFAULT (getdate()) FOR [FechaCreacion]
GO
ALTER TABLE [dbo].[DetallePlanSepare] ADD  DEFAULT (getdate()) FOR [FechaModificacion]
GO
ALTER TABLE [dbo].[DetalleProductos] ADD  DEFAULT ((0)) FOR [Stock]
GO
ALTER TABLE [dbo].[DetalleProductos] ADD  DEFAULT (getdate()) FOR [FechaCreacion]
GO
ALTER TABLE [dbo].[DetalleProductos] ADD  DEFAULT (getdate()) FOR [FechaModificacion]
GO
ALTER TABLE [dbo].[EstadosPedido] ADD  DEFAULT (getdate()) FOR [FechaCreacion]
GO
ALTER TABLE [dbo].[EstadosPedido] ADD  DEFAULT (getdate()) FOR [FechaModificacion]
GO
ALTER TABLE [dbo].[Pedidos] ADD  DEFAULT (getdate()) FOR [FechaPedido]
GO
ALTER TABLE [dbo].[Pedidos] ADD  DEFAULT (getdate()) FOR [FechaCreacion]
GO
ALTER TABLE [dbo].[Pedidos] ADD  DEFAULT (getdate()) FOR [FechaModificacion]
GO
ALTER TABLE [dbo].[PlanSepare] ADD  DEFAULT (getdate()) FOR [FechaCreacion]
GO
ALTER TABLE [dbo].[PlanSepare] ADD  DEFAULT (getdate()) FOR [FechaModificacion]
GO
ALTER TABLE [dbo].[Productos] ADD  DEFAULT (getdate()) FOR [FechaCreacion]
GO
ALTER TABLE [dbo].[Productos] ADD  DEFAULT (getdate()) FOR [FechaModificacion]
GO
ALTER TABLE [dbo].[Promociones] ADD  DEFAULT (getdate()) FOR [FechaCreacion]
GO
ALTER TABLE [dbo].[Promociones] ADD  DEFAULT (getdate()) FOR [FechaModificacion]
GO
ALTER TABLE [dbo].[DetallePedidos]  WITH CHECK ADD FOREIGN KEY([PedidoId])
REFERENCES [dbo].[Pedidos] ([PedidoId])
GO
ALTER TABLE [dbo].[DetallePedidos]  WITH CHECK ADD FOREIGN KEY([ProductoId])
REFERENCES [dbo].[Productos] ([ProductoId])
GO
ALTER TABLE [dbo].[DetallePlanSepare]  WITH CHECK ADD FOREIGN KEY([PlanSepareId])
REFERENCES [dbo].[PlanSepare] ([PlanSepareId])
GO
ALTER TABLE [dbo].[DetalleProductos]  WITH CHECK ADD FOREIGN KEY([ProductoId])
REFERENCES [dbo].[Productos] ([ProductoId])
GO
ALTER TABLE [dbo].[Pedidos]  WITH CHECK ADD FOREIGN KEY([EstadoPedidoId])
REFERENCES [dbo].[EstadosPedido] ([EstadoPedidoId])
GO
ALTER TABLE [dbo].[Pedidos]  WITH CHECK ADD FOREIGN KEY([PromocionId])
REFERENCES [dbo].[Promociones] ([PromocionId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[PlanSepare]  WITH CHECK ADD FOREIGN KEY([PedidoId])
REFERENCES [dbo].[Pedidos] ([PedidoId])
GO
ALTER TABLE [dbo].[Productos]  WITH CHECK ADD FOREIGN KEY([DepartamentoVentaId])
REFERENCES [dbo].[DepartamentosVenta] ([DepartamentoVentaId])
GO
ALTER TABLE [dbo].[XrefPromocionesDepartamentos]  WITH CHECK ADD FOREIGN KEY([DepartamentoVentaId])
REFERENCES [dbo].[DepartamentosVenta] ([DepartamentoVentaId])
GO
ALTER TABLE [dbo].[XrefPromocionesDepartamentos]  WITH CHECK ADD FOREIGN KEY([PromocionId])
REFERENCES [dbo].[Promociones] ([PromocionId])
GO
/****** Object:  StoredProcedure [dbo].[SP_DepartamentosVentaActualizar]    Script Date: 06/04/2022 21:00:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[SP_DepartamentosVentaActualizar]
(@DepartamentoVentaId bigint,@Descripcion varchar(300),@Direccion varchar(max),@Ciudad varchar(300),@Activo bit)
as
update DepartamentosVenta set Descripcion=@Descripcion,Direccion=@Direccion,Ciudad=@Ciudad,Activo=@Activo,FechaModificacion=getdate() 
where DepartamentoVentaId=@DepartamentoVentaId
set @DepartamentoVentaId=scope_identity()
if(@DepartamentoVentaId>0)
begin
SELECT 
DepartamentoVentaId,Descripcion,Direccion,Ciudad,Activo,FechaCreacion,FechaModificacion  
FROM DepartamentosVenta where DepartamentoVentaId=@DepartamentoVentaId
end
GO
/****** Object:  StoredProcedure [dbo].[SP_DepartamentosVentaEliminar]    Script Date: 06/04/2022 21:00:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[SP_DepartamentosVentaEliminar]
(@DepartamentoVentaId bigint,@Status int output)
as
declare @contadorEliminado int
set @contadorEliminado=1
set @Status=0

delete from XrefPromocionesDepartamentos
where DepartamentoVentaId=@DepartamentoVentaId

SELECT @contadorEliminado=count(*)
FROM XrefPromocionesDepartamentos where DepartamentoVentaId=@DepartamentoVentaId


if(@contadorEliminado=0)
begin
	set @contadorEliminado=1
	delete from DepartamentosVenta
	where DepartamentoVentaId=@DepartamentoVentaId

	SELECT @contadorEliminado=count(*)
	FROM DepartamentosVenta where DepartamentoVentaId=@DepartamentoVentaId

	if(@contadorEliminado=0)
	begin
		set @Status=1
	end
end
return @Status
GO
/****** Object:  StoredProcedure [dbo].[SP_DepartamentosVentaInsertar]    Script Date: 06/04/2022 21:00:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[SP_DepartamentosVentaInsertar]
(@Descripcion varchar(300),@Direccion varchar(max),@Ciudad varchar(300),@Activo bit)
as
declare @Id bigint
set @Id=0
insert into DepartamentosVenta(Descripcion,Direccion,Ciudad,Activo) 
values (@Descripcion,@Direccion,@Ciudad,@Activo)
set @Id=scope_identity()
if(@Id>0)
begin
SELECT 
DepartamentoVentaId,Descripcion,Direccion,Ciudad,Activo,FechaCreacion,FechaModificacion  
FROM DepartamentosVenta where DepartamentoVentaId=@Id
end
GO
/****** Object:  StoredProcedure [dbo].[SP_DepartamentosVentaTraerPorDetalleId]    Script Date: 06/04/2022 21:00:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[SP_DepartamentosVentaTraerPorDetalleId]
(@DepartamentoVentaId bigint)
as
SELECT 
DepartamentoVentaId,Descripcion,Direccion,Ciudad,Activo,FechaCreacion,FechaModificacion  
FROM DepartamentosVenta where DepartamentoVentaId=@DepartamentoVentaId
GO
/****** Object:  StoredProcedure [dbo].[SP_DepartamentosVentaTraerTodo]    Script Date: 06/04/2022 21:00:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-----------INICIO CRUD DepartamentosVenta-----------
create procedure [dbo].[SP_DepartamentosVentaTraerTodo]
as
SELECT 
DepartamentoVentaId,Descripcion,Direccion,Ciudad,Activo,FechaCreacion,FechaModificacion  
FROM DepartamentosVenta
GO
/****** Object:  StoredProcedure [dbo].[SP_DetallePedidosActualizar]    Script Date: 06/04/2022 21:00:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[SP_DetallePedidosActualizar]
(@DetallePedidoId bigint,@PedidoId bigint, @ProductoId bigint,@ValorProducto numeric(18,2),@Cantidad int, @SubTotal numeric(18,2))
as
update DetallePedidos set PedidoId=@PedidoId,ProductoId=@ProductoId,ValorProducto=@ValorProducto,Cantidad=@Cantidad,SubTotal=@SubTotal,FechaModificacion=getdate() 
where DetallePedidoId=@DetallePedidoId
set @DetallePedidoId=scope_identity()
if(@DetallePedidoId>0)
begin
SELECT 
[DetallePedidoId],[PedidoId],[ProductoId],[ValorProducto],[Cantidad],[SubTotal],FechaCreacion,FechaModificacion  
FROM DetallePedidos where DetallePedidoId=@DetallePedidoId
end
GO
/****** Object:  StoredProcedure [dbo].[SP_DetallePedidosEliminar]    Script Date: 06/04/2022 21:00:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[SP_DetallePedidosEliminar]
(@DetallePedidoId bigint,@Status int output)
as
declare @contadorEliminado int
set @contadorEliminado=1
set @Status=0
delete from DetallePedidos where DetallePedidoId=@DetallePedidoId

SELECT @contadorEliminado=count(*) 
FROM DetallePedidos where DetallePedidoId=@DetallePedidoId

if(@contadorEliminado<1)
begin
set @Status=1
end
return @Status
GO
/****** Object:  StoredProcedure [dbo].[SP_DetallePedidosInsertar]    Script Date: 06/04/2022 21:00:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[SP_DetallePedidosInsertar]
(@PedidoId bigint, @ProductoId bigint,@ValorProducto numeric(18,2),@Cantidad int, @SubTotal numeric(18,2))
as
declare @Id bigint
set @Id=0
insert into DetallePedidos(PedidoId,ProductoId,ValorProducto,Cantidad,SubTotal) 
values (@PedidoId,@ProductoId,@ValorProducto,@Cantidad,@SubTotal)
set @Id=scope_identity()
if(@Id>0)
begin
SELECT 
[DetallePedidoId],[PedidoId],[ProductoId],[ValorProducto],[Cantidad],[SubTotal],FechaCreacion,FechaModificacion  
FROM DetallePedidos where DetallePedidoId=@Id
end
GO
/****** Object:  StoredProcedure [dbo].[SP_DetallePedidosTraerPorDetalleId]    Script Date: 06/04/2022 21:00:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[SP_DetallePedidosTraerPorDetalleId]
(@DetallePedidoId bigint)
as
SELECT 
[DetallePedidoId],[PedidoId],[ProductoId],[ValorProducto],[Cantidad],[SubTotal],FechaCreacion,FechaModificacion  
FROM DetallePedidos where DetallePedidoId=@DetallePedidoId
GO
/****** Object:  StoredProcedure [dbo].[SP_DetallePedidosTraerPorPedidoId]    Script Date: 06/04/2022 21:00:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[SP_DetallePedidosTraerPorPedidoId]
(@PedidoId bigint)
as
SELECT 
[DetallePedidoId],[PedidoId],[ProductoId],[ValorProducto],[Cantidad],[SubTotal],FechaCreacion,FechaModificacion  
FROM DetallePedidos where PedidoId=@PedidoId
GO
/****** Object:  StoredProcedure [dbo].[SP_DetallePedidosTraerTodo]    Script Date: 06/04/2022 21:00:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-----------FIN CRUD DepartamentosVenta-----------
-----------INICIO CRUD DetallePedidos-----------
create procedure [dbo].[SP_DetallePedidosTraerTodo]
as
SELECT 
[DetallePedidoId],[PedidoId],[ProductoId],[ValorProducto],[Cantidad],[SubTotal],FechaCreacion,FechaModificacion  
FROM DetallePedidos 
GO
/****** Object:  StoredProcedure [dbo].[SP_DetallePlanSepareActualizar]    Script Date: 06/04/2022 21:00:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[SP_DetallePlanSepareActualizar]
(@DetallePlanSepareId bigint, @PlanSepareId bigint, @FechaPago datetime, @ValorPagado numeric(18,2),@MedioPago varchar(150),@ValorRestante numeric(18,2))
as
update DetallePlanSepare set PlanSepareId=@PlanSepareId,FechaPago=@FechaPago,ValorPagado=@ValorPagado,MedioPago=@MedioPago,ValorRestante=@ValorRestante,FechaModificacion=getdate() 
where DetallePlanSepareId=@DetallePlanSepareId
set @DetallePlanSepareId=scope_identity()
if(@DetallePlanSepareId>0)
begin
SELECT 
[DetallePlanSepareId],[PlanSepareId],[FechaPago],[ValorPagado],[MedioPago],[ValorRestante],[FechaCreacion],[FechaModificacion]
FROM DetallePlanSepare where [DetallePlanSepareId]=@DetallePlanSepareId
end
GO
/****** Object:  StoredProcedure [dbo].[SP_DetallePlanSepareEliminar]    Script Date: 06/04/2022 21:00:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[SP_DetallePlanSepareEliminar]
(@DetallePlanSepareId bigint,@Status int output)
as
declare @contadorEliminado int
set @contadorEliminado=1
set @Status=0
delete from DetallePlanSepare where [DetallePlanSepareId]=@DetallePlanSepareId

SELECT @contadorEliminado=count(*) 
FROM DetallePlanSepare where [DetallePlanSepareId]=@DetallePlanSepareId

if(@contadorEliminado<1)
begin
set @Status=1
end
return @Status
GO
/****** Object:  StoredProcedure [dbo].[SP_DetallePlanSepareInsertar]    Script Date: 06/04/2022 21:00:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[SP_DetallePlanSepareInsertar]
(@PlanSepareId bigint, @FechaPago datetime, @ValorPagado numeric(18,2),@MedioPago varchar(150),@ValorRestante numeric(18,2))
as
declare @Id bigint
set @Id=0
insert into DetallePlanSepare([PlanSepareId],[FechaPago],[ValorPagado],[MedioPago],[ValorRestante]) 
values (@PlanSepareId,@FechaPago,@ValorPagado,@MedioPago,@ValorRestante)
set @Id=scope_identity()
if(@Id>0)
begin
SELECT 
[DetallePlanSepareId],[PlanSepareId],[FechaPago],[ValorPagado],[MedioPago],[ValorRestante],[FechaCreacion],[FechaModificacion]
FROM DetallePlanSepare where [DetallePlanSepareId]=@Id
end
GO
/****** Object:  StoredProcedure [dbo].[SP_DetallePlanSepareTraerPorDetalleId]    Script Date: 06/04/2022 21:00:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[SP_DetallePlanSepareTraerPorDetalleId]
(@DetallePlanSepareId bigint)
as
SELECT 
[DetallePlanSepareId],[PlanSepareId],[FechaPago],[ValorPagado],[MedioPago],[ValorRestante],[FechaCreacion],[FechaModificacion]
FROM DetallePlanSepare where [DetallePlanSepareId]=@DetallePlanSepareId
GO
/****** Object:  StoredProcedure [dbo].[SP_DetallePlanSepareTraerPorPlanSepareId]    Script Date: 06/04/2022 21:00:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[SP_DetallePlanSepareTraerPorPlanSepareId]
(@PlanSepareId bigint)
as
SELECT 
[DetallePlanSepareId],[PlanSepareId],[FechaPago],[ValorPagado],[MedioPago],[ValorRestante],[FechaCreacion],[FechaModificacion]
FROM DetallePlanSepare where PlanSepareId=@PlanSepareId
GO
/****** Object:  StoredProcedure [dbo].[SP_DetallePlanSepareTraerTodo]    Script Date: 06/04/2022 21:00:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-----------FIN CRUD DetallePedidos-----------
-----------INICIO CRUD DetallePlanSepare-----------
create procedure [dbo].[SP_DetallePlanSepareTraerTodo]
as
SELECT 
[DetallePlanSepareId],[PlanSepareId],[FechaPago],[ValorPagado],[MedioPago],[ValorRestante],[FechaCreacion],[FechaModificacion]
FROM DetallePlanSepare 
GO
/****** Object:  StoredProcedure [dbo].[SP_DetalleProductosActualizar]    Script Date: 06/04/2022 21:00:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[SP_DetalleProductosActualizar]
(@DetalleProductoId bigint, @ProductoId bigint, @Stock int, @Talla varchar(300),@Color varchar(300))
as
update DetalleProductos set ProductoId=@ProductoId,Stock=@Stock,Talla=@Talla,Color=@Color,FechaModificacion=getdate() 
where DetalleProductoId=@DetalleProductoId
set @DetalleProductoId=scope_identity()
if(@DetalleProductoId>0)
begin
SELECT 
[DetalleProductoId],[ProductoId],[Stock],[Talla],[Color],[FechaCreacion],[FechaModificacion]
FROM DetalleProductos  where DetalleProductoId=@DetalleProductoId
end
GO
/****** Object:  StoredProcedure [dbo].[SP_DetalleProductosEliminar]    Script Date: 06/04/2022 21:00:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[SP_DetalleProductosEliminar]
(@DetalleProductoId bigint,@Status int output)
as
declare @contadorEliminado int
set @contadorEliminado=1
set @Status=0
delete from DetalleProductos where DetalleProductoId=@DetalleProductoId

SELECT @contadorEliminado=count(*) 
FROM DetalleProductos where DetalleProductoId=@DetalleProductoId

if(@contadorEliminado<1)
begin
set @Status=1
end
return @Status
GO
/****** Object:  StoredProcedure [dbo].[SP_DetalleProductosInsertar]    Script Date: 06/04/2022 21:00:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[SP_DetalleProductosInsertar]
(@ProductoId bigint, @Stock int, @Talla varchar(300),@Color varchar(300))
as
declare @Id bigint
set @Id=0
insert into DetalleProductos([ProductoId],[Stock],[Talla],[Color]) 
values (@ProductoId,@Stock,@Talla,@Color)
set @Id=scope_identity()
if(@Id>0)
begin
SELECT 
[DetalleProductoId],[ProductoId],[Stock],[Talla],[Color],[FechaCreacion],[FechaModificacion]
FROM DetalleProductos  where DetalleProductoId=@Id
end
GO
/****** Object:  StoredProcedure [dbo].[SP_DetalleProductosTraerPorDetalleId]    Script Date: 06/04/2022 21:00:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[SP_DetalleProductosTraerPorDetalleId]
(@DetalleProductoId bigint)
as
SELECT 
[DetalleProductoId],[ProductoId],[Stock],[Talla],[Color],[FechaCreacion],[FechaModificacion]
FROM DetalleProductos  where DetalleProductoId=@DetalleProductoId
GO
/****** Object:  StoredProcedure [dbo].[SP_DetalleProductosTraerPorProductoId]    Script Date: 06/04/2022 21:00:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[SP_DetalleProductosTraerPorProductoId]
(@ProductoId bigint)
as
SELECT 
[DetalleProductoId],[ProductoId],[Stock],[Talla],[Color],[FechaCreacion],[FechaModificacion]
FROM DetalleProductos  where [ProductoId]=@ProductoId
GO
/****** Object:  StoredProcedure [dbo].[SP_DetalleProductosTraerTodo]    Script Date: 06/04/2022 21:00:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-----------FIN CRUD DetallePlanSepare-----------
-----------INICIO CRUD DetalleProductos-----------
create procedure [dbo].[SP_DetalleProductosTraerTodo]
as
SELECT 
[DetalleProductoId],[ProductoId],[Stock],[Talla],[Color],[FechaCreacion],[FechaModificacion]
FROM DetalleProductos 
GO
/****** Object:  StoredProcedure [dbo].[SP_EstadosPedidoActualizar]    Script Date: 06/04/2022 21:00:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[SP_EstadosPedidoActualizar]
(@EstadoPedidoId bigint, @Descripcion varchar(50))
as
update EstadosPedido set Descripcion=@Descripcion,FechaModificacion=getdate() 
where EstadoPedidoId=@EstadoPedidoId
set @EstadoPedidoId=scope_identity()
if(@EstadoPedidoId>0)
begin
SELECT 
[EstadoPedidoId],[Descripcion],[FechaCreacion],[FechaModificacion]
FROM EstadosPedido  where EstadoPedidoId=@EstadoPedidoId
end
GO
/****** Object:  StoredProcedure [dbo].[SP_EstadosPedidoEliminar]    Script Date: 06/04/2022 21:00:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[SP_EstadosPedidoEliminar]
(@EstadoPedidoId bigint,@Status int output)
as
declare @contadorEliminado int
set @contadorEliminado=1
set @Status=0
delete from EstadosPedido where EstadoPedidoId=@EstadoPedidoId

SELECT @contadorEliminado=count(*) 
FROM EstadosPedido where EstadoPedidoId=@EstadoPedidoId

if(@contadorEliminado<1)
begin
set @Status=1
end
return @Status
GO
/****** Object:  StoredProcedure [dbo].[SP_EstadosPedidoInsertar]    Script Date: 06/04/2022 21:00:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[SP_EstadosPedidoInsertar]
(@Descripcion varchar(50))
as
declare @Id bigint
set @Id=0
insert into EstadosPedido(Descripcion) 
values (@Descripcion)
set @Id=scope_identity()
if(@Id>0)
begin
SELECT 
[EstadoPedidoId],[Descripcion],[FechaCreacion],[FechaModificacion]
FROM EstadosPedido  where EstadoPedidoId=@Id
end
GO
/****** Object:  StoredProcedure [dbo].[SP_EstadosPedidoTraerPorDetalleId]    Script Date: 06/04/2022 21:00:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[SP_EstadosPedidoTraerPorDetalleId]
(@EstadoPedidoId bigint)
as
SELECT 
[EstadoPedidoId],[Descripcion],[FechaCreacion],[FechaModificacion]
FROM EstadosPedido  where EstadoPedidoId=@EstadoPedidoId
GO
/****** Object:  StoredProcedure [dbo].[SP_EstadosPedidoTraerTodo]    Script Date: 06/04/2022 21:00:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-----------FIN CRUD DetalleProductos-----------
-----------INICIO CRUD EstadosPedido-----------
create procedure [dbo].[SP_EstadosPedidoTraerTodo]
as
SELECT 
[EstadoPedidoId],[Descripcion],[FechaCreacion],[FechaModificacion]
FROM EstadosPedido 
GO
/****** Object:  StoredProcedure [dbo].[SP_PedidosActualizar]    Script Date: 06/04/2022 21:00:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[SP_PedidosActualizar]
(@PedidoId bigint,@FechaPedido datetime,@EstadoPedidoId int,@DireccionEnvio varchar(Max),@FechaEnvio datetime,@IdentificacionCliente varchar(50),@SubTotal numeric(18,2),@ValorTotal numeric(18,2),@PromocionId bigint)
as
update Pedidos set [FechaPedido]=@FechaPedido,[EstadoPedidoId]=@EstadoPedidoId,[DireccionEnvio]=@DireccionEnvio,[FechaEnvio]=@FechaEnvio,[IdentificacionCliente]=@IdentificacionCliente,[SubTotal]=@SubTotal,[ValorTotal]=@ValorTotal,[PromocionId]=@PromocionId,FechaModificacion=getdate() 
where PedidoId=@PedidoId
set @PedidoId=scope_identity()
if(@PedidoId>0)
begin
SELECT 
[PedidoId],[FechaPedido],[EstadoPedidoId],[DireccionEnvio],[FechaEnvio],[IdentificacionCliente],[SubTotal],[ValorTotal],[PromocionId],[FechaCreacion],[FechaModificacion]
FROM Pedidos  where PedidoId=@PedidoId
end
GO
/****** Object:  StoredProcedure [dbo].[SP_PedidosEliminar]    Script Date: 06/04/2022 21:00:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[SP_PedidosEliminar]
(@PedidoId bigint,@Status int output)
as
declare @contadorEliminado int
declare @EstadoPedidoId int
set @contadorEliminado=1
set @Status=0

update Pedidos set [EstadoPedidoId]=6,FechaModificacion=getdate() 
where PedidoId=@PedidoId
set @PedidoId=scope_identity()

SELECT @contadorEliminado=count(*)
FROM Pedidos where PedidoId=@PedidoId

SELECT @EstadoPedidoId=EstadoPedidoId
FROM Pedidos where PedidoId=@PedidoId

if(@contadorEliminado>0 and @EstadoPedidoId=6)
begin
set @Status=1
end
return @Status
GO
/****** Object:  StoredProcedure [dbo].[SP_PedidosInsertar]    Script Date: 06/04/2022 21:00:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[SP_PedidosInsertar]
(@FechaPedido datetime,@EstadoPedidoId int,@DireccionEnvio varchar(Max),@FechaEnvio datetime,@IdentificacionCliente varchar(50),@SubTotal numeric(18,2),@ValorTotal numeric(18,2),@PromocionId bigint)
as
declare @Id bigint
set @Id=0
insert into Pedidos([FechaPedido],[EstadoPedidoId],[DireccionEnvio],[FechaEnvio],[IdentificacionCliente],[SubTotal],[ValorTotal],[PromocionId]) 
values (@FechaPedido,@EstadoPedidoId,@DireccionEnvio,@FechaEnvio,@IdentificacionCliente,@SubTotal,@ValorTotal,@PromocionId)
set @Id=scope_identity()
if(@Id>0)
begin
SELECT 
[PedidoId],[FechaPedido],[EstadoPedidoId],[DireccionEnvio],[FechaEnvio],[IdentificacionCliente],[SubTotal],[ValorTotal],[PromocionId],[FechaCreacion],[FechaModificacion]
FROM Pedidos  where PedidoId=@Id
end
GO
/****** Object:  StoredProcedure [dbo].[SP_PedidosTraerPorDetalleId]    Script Date: 06/04/2022 21:00:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[SP_PedidosTraerPorDetalleId]
(@PedidoId bigint)
as
SELECT 
[PedidoId],[FechaPedido],[EstadoPedidoId],[DireccionEnvio],[FechaEnvio],[IdentificacionCliente],[SubTotal],[ValorTotal],[PromocionId],[FechaCreacion],[FechaModificacion]
FROM Pedidos  where PedidoId=@PedidoId and [EstadoPedidoId]<>6
GO
/****** Object:  StoredProcedure [dbo].[SP_PedidosTraerPorIdentificacionCliente]    Script Date: 06/04/2022 21:00:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[SP_PedidosTraerPorIdentificacionCliente]
(@IdentificacionCliente varchar(50))
as
SELECT 
[PedidoId],[FechaPedido],[EstadoPedidoId],[DireccionEnvio],[FechaEnvio],[IdentificacionCliente],[SubTotal],[ValorTotal],[PromocionId],[FechaCreacion],[FechaModificacion]
FROM Pedidos  where IdentificacionCliente=@IdentificacionCliente

GO
/****** Object:  StoredProcedure [dbo].[SP_PedidosTraerTodo]    Script Date: 06/04/2022 21:00:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-----------FIN CRUD EstadosPedido-----------
-----------INICIO CRUD Pedidos-----------
create procedure [dbo].[SP_PedidosTraerTodo]
as
SELECT 
[PedidoId],[FechaPedido],[EstadoPedidoId],[DireccionEnvio],[FechaEnvio],[IdentificacionCliente],[SubTotal],[ValorTotal],[PromocionId],[FechaCreacion],[FechaModificacion]
FROM Pedidos  where [EstadoPedidoId]<>6
GO
/****** Object:  StoredProcedure [dbo].[SP_PlanSepareActualizar]    Script Date: 06/04/2022 21:00:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[SP_PlanSepareActualizar]
(@PlanSepareId bigint,@PedidoId bigint,@ValorTotal numeric(18,2),@ValorPagado numeric(18,2),@CantidadCuotas int)
as
update PlanSepare set [PedidoId]=@PedidoId,[ValorTotal]=@ValorTotal,[ValorPagado]=@ValorPagado,[CantidadCuotas]=@CantidadCuotas,FechaModificacion=getdate() 
where PlanSepareId=@PlanSepareId
set @PlanSepareId=scope_identity()
if(@PlanSepareId>0)
begin
SELECT 
[PlanSepareId],[PedidoId],[ValorTotal],[ValorPagado],[CantidadCuotas],[FechaCreacion],[FechaModificacion]
FROM PlanSepare  where PlanSepareId=@PlanSepareId
end
GO
/****** Object:  StoredProcedure [dbo].[SP_PlanSepareEliminar]    Script Date: 06/04/2022 21:00:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[SP_PlanSepareEliminar]
(@PlanSepareId bigint,@Status int output)
as
declare @contadorEliminado int
set @contadorEliminado=1
set @Status=0

delete from DetallePlanSepare
where PlanSepareId=@PlanSepareId

SELECT @contadorEliminado=count(*)
FROM DetallePlanSepare where PlanSepareId=@PlanSepareId


if(@contadorEliminado<1)
begin
	set @contadorEliminado=1
	delete from PlanSepare
	where PlanSepareId=@PlanSepareId

	SELECT @contadorEliminado=count(*)
	FROM PlanSepare where PlanSepareId=@PlanSepareId

	if(@contadorEliminado<1)
	begin
		set @Status=1
	end
end
return @Status
GO
/****** Object:  StoredProcedure [dbo].[SP_PlanSepareInsertar]    Script Date: 06/04/2022 21:00:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[SP_PlanSepareInsertar]
(@PedidoId bigint,@ValorTotal numeric(18,2),@ValorPagado numeric(18,2),@CantidadCuotas int)
as
declare @Id bigint
set @Id=0
insert into PlanSepare([PedidoId],[ValorTotal],[ValorPagado],[CantidadCuotas]) 
values (@PedidoId,@ValorTotal,@ValorPagado,@CantidadCuotas)
set @Id=scope_identity()
if(@Id>0)
begin
SELECT 
[PlanSepareId],[PedidoId],[ValorTotal],[ValorPagado],[CantidadCuotas],[FechaCreacion],[FechaModificacion]
FROM PlanSepare  where PlanSepareId=@Id
end
GO
/****** Object:  StoredProcedure [dbo].[SP_PlanSepareTraerPorId]    Script Date: 06/04/2022 21:00:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[SP_PlanSepareTraerPorId]
(@PlanSepareId bigint)
as
SELECT 
[PlanSepareId],[PedidoId],[ValorTotal],[ValorPagado],[CantidadCuotas],[FechaCreacion],[FechaModificacion]
FROM PlanSepare  where PlanSepareId=@PlanSepareId
GO
/****** Object:  StoredProcedure [dbo].[SP_PlanSepareTraerPorPedidoId]    Script Date: 06/04/2022 21:00:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


create procedure [dbo].[SP_PlanSepareTraerPorPedidoId]
(@PedidoId bigint)
as
SELECT 
[PlanSepareId],[PedidoId],[ValorTotal],[ValorPagado],[CantidadCuotas],[FechaCreacion],[FechaModificacion]
FROM PlanSepare  where [PedidoId]=@PedidoId
GO
/****** Object:  StoredProcedure [dbo].[SP_PlanSepareTraerTodo]    Script Date: 06/04/2022 21:00:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-----------FIN CRUD Pedidos-----------
-----------INICIO CRUD PlanSepare-----------
create procedure [dbo].[SP_PlanSepareTraerTodo]
as
SELECT 
[PlanSepareId],[PedidoId],[ValorTotal],[ValorPagado],[CantidadCuotas],[FechaCreacion],[FechaModificacion]
FROM PlanSepare  
GO
/****** Object:  StoredProcedure [dbo].[SP_ProductosActualizar]    Script Date: 06/04/2022 21:00:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[SP_ProductosActualizar]
(@ProductoId bigint,@Descripcion varchar(300),@Valor numeric(18,2),@DepartamentoVentaId bigint)
as
update Productos set [Descripcion]=@Descripcion,[Valor]=@Valor,[DepartamentoVentaId]=@DepartamentoVentaId,FechaModificacion=getdate() 
where ProductoId=@ProductoId
set @ProductoId=scope_identity()
if(@ProductoId>0)
begin
SELECT 
[ProductoId],[Descripcion],[Valor],[DepartamentoVentaId],[FechaCreacion],[FechaModificacion]
FROM Productos  where ProductoId=@ProductoId
end
GO
/****** Object:  StoredProcedure [dbo].[SP_ProductosEliminar]    Script Date: 06/04/2022 21:00:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[SP_ProductosEliminar]
(@ProductoId bigint,@Status int output)
as
declare @contadorEliminado int
set @contadorEliminado=1
set @Status=0

delete from DetalleProductos
where ProductoId=@ProductoId

SELECT @contadorEliminado=count(*)
FROM DetalleProductos where ProductoId=@ProductoId


if(@contadorEliminado<1)
begin
	set @contadorEliminado=1
	delete from Productos
	where ProductoId=@ProductoId

	SELECT @contadorEliminado=count(*)
	FROM Productos where ProductoId=@ProductoId

	if(@contadorEliminado<1)
	begin
		set @Status=1
	end
end
return @Status
GO
/****** Object:  StoredProcedure [dbo].[SP_ProductosInsertar]    Script Date: 06/04/2022 21:00:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[SP_ProductosInsertar]
(@Descripcion varchar(300),@Valor numeric(18,2),@DepartamentoVentaId bigint)
as
declare @Id bigint
set @Id=0
insert into Productos([Descripcion],[Valor],[DepartamentoVentaId]) 
values (@Descripcion,@Valor,@DepartamentoVentaId)
set @Id=scope_identity()
if(@Id>0)
begin
SELECT 
[ProductoId],[Descripcion],[Valor],[DepartamentoVentaId],[FechaCreacion],[FechaModificacion]
FROM Productos  where ProductoId=@Id
end
GO
/****** Object:  StoredProcedure [dbo].[SP_ProductosTraerPorDetalleId]    Script Date: 06/04/2022 21:00:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[SP_ProductosTraerPorDetalleId]
(@ProductoId bigint)
as
SELECT 
[ProductoId],[Descripcion],[Valor],[DepartamentoVentaId],[FechaCreacion],[FechaModificacion]
FROM Productos  where ProductoId=@ProductoId
GO
/****** Object:  StoredProcedure [dbo].[SP_ProductosTraerTodo]    Script Date: 06/04/2022 21:00:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-----------FIN CRUD PlanSepare-----------
-----------INICIO CRUD Productos-----------
create procedure [dbo].[SP_ProductosTraerTodo]
as
SELECT 
[ProductoId],[Descripcion],[Valor],[DepartamentoVentaId],[FechaCreacion],[FechaModificacion]
FROM Productos  
GO
/****** Object:  StoredProcedure [dbo].[SP_PromocionesActualizar]    Script Date: 06/04/2022 21:00:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[SP_PromocionesActualizar]
(@PromocionId bigint, @Descripcion varchar(200),@MontoMinimo numeric(18,2),@MontoMaximo numeric(18,2),@CantidadProductosMinimo int,@PorcentajePromocion numeric(4,2))
as
update Promociones set [Descripcion]=@Descripcion,[MontoMinimo]=@MontoMinimo,[MontoMaximo]=@MontoMaximo,[CantidadProductosMinimo]=@CantidadProductosMinimo,[PorcentajePromocion]=@PorcentajePromocion,FechaModificacion=getdate() 
where PromocionId=@PromocionId
set @PromocionId=scope_identity()
if(@PromocionId>0)
begin
SELECT 
[PromocionId],[Descripcion],[MontoMinimo],[MontoMaximo],[CantidadProductosMinimo],[PorcentajePromocion],[FechaCreacion],[FechaModificacion]
FROM Promociones  where PromocionId=@PromocionId
end
GO
/****** Object:  StoredProcedure [dbo].[SP_PromocionesEliminar]    Script Date: 06/04/2022 21:00:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[SP_PromocionesEliminar]
(@PromocionId bigint,@Status int output)
as
declare @contadorEliminado int
set @contadorEliminado=1
set @Status=0

delete from XrefPromocionesDepartamentos
where PromocionId=@PromocionId

SELECT @contadorEliminado=count(*)
FROM XrefPromocionesDepartamentos where PromocionId=@PromocionId


if(@contadorEliminado<1)
begin
	set @contadorEliminado=1
	delete from Promociones
	where PromocionId=@PromocionId

	SELECT @contadorEliminado=count(*)
	FROM Promociones where PromocionId=@PromocionId

	if(@contadorEliminado<1)
	begin
		set @Status=1
	end
end
return @Status
GO
/****** Object:  StoredProcedure [dbo].[SP_PromocionesInsertar]    Script Date: 06/04/2022 21:00:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[SP_PromocionesInsertar]
(@Descripcion varchar(200),@MontoMinimo numeric(18,2),@MontoMaximo numeric(18,2),@CantidadProductosMinimo int,@PorcentajePromocion numeric(4,2))
as
declare @Id bigint
set @Id=0
insert into Promociones([Descripcion],[MontoMinimo],[MontoMaximo],[CantidadProductosMinimo],[PorcentajePromocion]) 
values (@Descripcion,@MontoMinimo,@MontoMaximo,@CantidadProductosMinimo,@PorcentajePromocion)
set @Id=scope_identity()
if(@Id>0)
begin
SELECT 
[PromocionId],[Descripcion],[MontoMinimo],[MontoMaximo],[CantidadProductosMinimo],[PorcentajePromocion],[FechaCreacion],[FechaModificacion]
FROM Promociones  where PromocionId=@Id
end
GO
/****** Object:  StoredProcedure [dbo].[SP_PromocionesTraerPorDetalleId]    Script Date: 06/04/2022 21:00:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[SP_PromocionesTraerPorDetalleId]
(@PromocionId bigint)
as
SELECT 
[PromocionId],[Descripcion],[MontoMinimo],[MontoMaximo],[CantidadProductosMinimo],[PorcentajePromocion],[FechaCreacion],[FechaModificacion]
FROM Promociones  where PromocionId=@PromocionId
GO
/****** Object:  StoredProcedure [dbo].[SP_PromocionesTraerTodo]    Script Date: 06/04/2022 21:00:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-----------FIN CRUD Productos-----------
-----------INICIO CRUD Promociones-----------
create procedure [dbo].[SP_PromocionesTraerTodo]
as
SELECT 
[PromocionId],[Descripcion],[MontoMinimo],[MontoMaximo],[CantidadProductosMinimo],[PorcentajePromocion],[FechaCreacion],[FechaModificacion]
FROM Promociones 
GO
/****** Object:  StoredProcedure [dbo].[SP_TraerProductosDisponibles]    Script Date: 06/04/2022 21:00:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[SP_TraerProductosDisponibles]
as
	set nocount on;
	select a.productoId
	,a.StockTotal
	,b.Descripcion as 'DescripcionProducto'
	,b.Valor as 'ValorProducto'
	,c.Descripcion as 'PuntoVenta'
	,c.Direccion as 'DireccionPuntoVenta'
	,c.Ciudad as 'CiudadPuntoVenta'
	from [dbo].[View_ProductosDisponibles] a
	inner join Productos b on a.ProductoId = b.ProductoId
	inner join DepartamentosVenta c on b.DepartamentoVentaId = c.DepartamentoVentaId
	where c.Activo=1
GO
/****** Object:  StoredProcedure [dbo].[SP_TraerProductosDisponiblesPorDepartamento]    Script Date: 06/04/2022 21:00:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[SP_TraerProductosDisponiblesPorDepartamento]
(@DepartamentoVentaId bigint)
as
	set nocount on;
	select cast(a.productoId as bigint) as productoId
	,cast(a.StockTotal as int) as StockTotal
	,b.Descripcion as 'DescripcionProducto'
	,cast(b.Valor as numeric(18,2)) as 'ValorProducto'
	,c.Descripcion as 'PuntoVenta'
	,c.Direccion as 'DireccionPuntoVenta'
	,c.Ciudad as 'CiudadPuntoVenta'
	from [dbo].[View_ProductosDisponibles] a
	inner join Productos b on a.ProductoId = b.ProductoId
	inner join DepartamentosVenta c on b.DepartamentoVentaId = c.DepartamentoVentaId
	where c.Activo=1 and b.DepartamentoVentaId=@DepartamentoVentaId
GO
/****** Object:  StoredProcedure [dbo].[SP_ValidarPromociones]    Script Date: 06/04/2022 21:00:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[SP_ValidarPromociones]
(@DepartamentoVentaId bigint,@IdentificacionCliente varchar(50),@ValorCompra numeric(18,2),
@ValorFinal numeric(18,2) output,@AplicaSepare bit output)
as
	set nocount on;
	declare @cantidadPedidos int
	declare @AplicaPromocion bit
	declare @ContadorPromociones int
	declare @ProcentajePromocion numeric(4,2)

	set @ValorFinal=@ValorCompra
	set @AplicaSepare=0
	set @ProcentajePromocion=0

	--Se valida cuántos pedidos finalizados con anterioridad
	select @cantidadPedidos=coalesce (sum(PedidoId),0) from Pedidos
	where EstadoPedidoId=3 and IdentificacionCliente=@IdentificacionCliente

	--Se validad si el cliente ha tenido más de dos pedidos para aplicar promoción 
	if(@cantidadPedidos>=2)
	begin
		--Si aplica para promoción se consulta cuál es el porcentaje de la promoción a la que aplica
		select top(1) @ProcentajePromocion=coalesce(a.PorcentajePromocion,0)
		from Promociones a
		inner join XrefPromocionesDepartamentos b on a.PromocionId=b.PromocionId
		inner join DepartamentosVenta c on c.DepartamentoVentaId=b.DepartamentoVentaId
		where c.DepartamentoVentaId=@DepartamentoVentaId and @ValorCompra between a.MontoMinimo and a.MontoMaximo
	end

	--Se valida si se encontraron promociones que aplicaran al cliente
	if(@ProcentajePromocion>0)
	begin
		--Si el cliente aplica para promoción se realiza el calculo del valor con promoción y se asigna a variable que retorna
		set @ValorFinal=@ValorCompra-((@ValorCompra*@ProcentajePromocion)/100)
	end

	--Se valida si el cliente aplica para el Plan Separe
	if(@ValorFinal>=1000000)
	begin
		set @AplicaSepare=1
	end
GO
/****** Object:  StoredProcedure [dbo].[SP_XrefPromocionesDepartamentosActualizar]    Script Date: 06/04/2022 21:00:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[SP_XrefPromocionesDepartamentosActualizar]
(@PromocionIdLast bigint,@PromocionIdNew bigint,@DepartamentoVentaIdLast bigint,@DepartamentoVentaIdNew bigint)
as
declare @contadorEliminado int
set @contadorEliminado=1

delete from XrefPromocionesDepartamentos
where PromocionId=@PromocionIdLast and DepartamentoVentaId=@DepartamentoVentaIdNew

SELECT @contadorEliminado=count(*)
FROM XrefPromocionesDepartamentos where PromocionId=@PromocionIdLast and DepartamentoVentaId=@DepartamentoVentaIdNew

if(@contadorEliminado<1)
begin
	set @contadorEliminado=1

	insert into XrefPromocionesDepartamentos(PromocionId,DepartamentoVentaId) 
	values (@PromocionIdNew,@DepartamentoVentaIdNew)
end

SELECT 
[PromocionId],DepartamentoVentaId
FROM XrefPromocionesDepartamentos  where PromocionId=@PromocionIdNew and DepartamentoVentaId=@DepartamentoVentaIdNew


GO
/****** Object:  StoredProcedure [dbo].[SP_XrefPromocionesDepartamentosEliminar]    Script Date: 06/04/2022 21:00:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[SP_XrefPromocionesDepartamentosEliminar]
(@PromocionId bigint,@DepartamentoVentaId bigint,@Status int output)
as
declare @contadorEliminado int
set @contadorEliminado=1
set @Status=0

delete from XrefPromocionesDepartamentos
where PromocionId=@PromocionId and DepartamentoVentaId=@DepartamentoVentaId

SELECT @contadorEliminado=count(*)
FROM XrefPromocionesDepartamentos where PromocionId=@PromocionId and DepartamentoVentaId=@DepartamentoVentaId


if(@contadorEliminado<1)
begin
	set @Status=1
end
return @Status

GO
/****** Object:  StoredProcedure [dbo].[SP_XrefPromocionesDepartamentosInsertar]    Script Date: 06/04/2022 21:00:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[SP_XrefPromocionesDepartamentosInsertar]
(@PromocionId bigint, @DepartamentoVentaId bigint)
as

insert into XrefPromocionesDepartamentos(PromocionId,DepartamentoVentaId) 
values (@PromocionId,@DepartamentoVentaId)

SELECT 
[PromocionId],DepartamentoVentaId
FROM XrefPromocionesDepartamentos  where PromocionId=@PromocionId and DepartamentoVentaId=@DepartamentoVentaId


GO
/****** Object:  StoredProcedure [dbo].[SP_XrefPromocionesDepartamentosTraerPorDepartamentoId]    Script Date: 06/04/2022 21:00:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[SP_XrefPromocionesDepartamentosTraerPorDepartamentoId]
(@DepartamentoVentaId bigint)
as
SELECT 
[PromocionId],DepartamentoVentaId
FROM XrefPromocionesDepartamentos  where DepartamentoVentaId=@DepartamentoVentaId

GO
/****** Object:  StoredProcedure [dbo].[SP_XrefPromocionesDepartamentosTraerPorPromocionId]    Script Date: 06/04/2022 21:00:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[SP_XrefPromocionesDepartamentosTraerPorPromocionId]
(@PromocionId bigint)
as
SELECT 
[PromocionId],DepartamentoVentaId
FROM XrefPromocionesDepartamentos  where PromocionId=@PromocionId

GO
/****** Object:  StoredProcedure [dbo].[SP_XrefPromocionesDepartamentosTraerTodo]    Script Date: 06/04/2022 21:00:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[SP_XrefPromocionesDepartamentosTraerTodo]
as
SELECT 
[PromocionId],DepartamentoVentaId
FROM XrefPromocionesDepartamentos 

GO
