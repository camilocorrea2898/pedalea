Create View [dbo].[View_ProductosDisponibles]
as
select b.ProductoId,sum(a.stock) as StockTotal from DetalleProductos a
inner join Productos b on a.ProductoId = b.ProductoId 
group by b.ProductoId
having  sum(a.stock)>=1
